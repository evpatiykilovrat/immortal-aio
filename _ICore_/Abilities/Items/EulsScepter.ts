import { TargetManager } from "../../Manager/Target";
import { AIODisable } from "../Disable";

export class AIOEulsScepter extends AIODisable {
	private readonly Buffs = [
		"modifier_lina_laguna_blade",
		"modifier_lion_finger_of_death",
	]

	public ShouldCast() {

		const target = TargetManager.Target!
		if (!target.IsInNormalState && !target.IsTeleporting && !target.IsChanneling)
			return false

		if (target.BaseOwner.ModifiersBook.HasAnyBuffByNames(this.Buffs))
			return false

		return this.ShouldForceCast()
	}

	public ShouldForceCast() {
		const target = TargetManager.Target!

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (this.AIODisable.UnitTargetCast && target.IsBlockingAbilities)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable)
			return false

		return true
	}
}
