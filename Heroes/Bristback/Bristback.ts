import { AbilityX, AbyssalBlade, BlinkDagger, Bloodthorn, EventsX, Gleipnir, Hairball, MedallionOfCourage, OrchidMalevolence, QuillSpray, RodOfAtos, ScytheOfVyse, ShivasGuard, SolarCrest, UnitX, ViscousNasalGoo } from "immortal-core/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIONuke, AIOShield, AIOShivasGuard, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";

const data = {
	spray: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	horn: undefined as Nullable<AIOBloodthorn>,
	orchid: undefined as Nullable<AIODisable>,
	hair: undefined as Nullable<AIODebuff>,
	atos: undefined as Nullable<AIODisable>,
	medal: undefined as Nullable<AIODebuff>,
	viscous: undefined as Nullable<AIODebuff>,
	abyssal: undefined as Nullable<AIODisable>,
	bladeMail: undefined as Nullable<AIOShield>,
	blink: undefined as Nullable<AIOBlink>,
	shiva: undefined as Nullable<AIOShivasGuard>,
}

export const BAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.horn = new AIOBloodthorn(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIOShivasGuard(abil)
	if (abil instanceof QuillSpray)
		data.spray = new AIONuke(abil)
	if (abil instanceof Hairball)
		data.hair = new AIODebuff(abil)
	if (abil instanceof ViscousNasalGoo)
		data.viscous = new AIODebuff(abil)
	if (abil instanceof MedallionOfCourage || abil instanceof SolarCrest)
		data.medal = new AIODebuff(abil)
}

export const BristbackMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 450, 100))
		return true

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.horn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bladeMail, 400))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.medal))
		return true

	if (helper.UseAbility(data.shiva))
		return true

	if (helper.UseAbility(data.viscous))
		return true

	if (helper.UseAbility(data.spray))
		return true

	if (helper.UseAbility(data.hair))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
