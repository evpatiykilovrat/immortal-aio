import { PathX } from "immortal-core/Imports";
import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const FacelessVoidMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_faceless_void",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_mask_of_madness",
		"item_mjollnir",
		"item_manta",
		"item_diffusal_blade",
		"item_black_king_bar",
		"item_silver_edge",
		"item_orchid",
		"item_bloodthorn",
	],
	Abilities: [
		"faceless_void_time_walk",
		"faceless_void_time_dilation",
		"faceless_void_chronosphere",
	],
	IgnoreInvisDefault: false,
	HitMenu: { default: 1, name: "Chronosphere", imageName: PathX.DOTAAbilities("faceless_void_chronosphere")},
})
