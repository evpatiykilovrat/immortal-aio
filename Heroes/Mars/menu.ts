import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const MarsMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_mars",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_orchid",
		"item_abyssal_blade",
		"item_nullifier",
		"item_bloodthorn",
		"item_blade_mail",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_lotus_orb",
		"item_armlet",
		"item_cyclone",
		"item_solar_crest",
		"item_medallion_of_courage",
		"item_sheepstick",
	],
	Abilities: [
		"mars_spear",
		"mars_gods_rebuke",
		"mars_arena_of_blood",
	],
})
