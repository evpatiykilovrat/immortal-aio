import { EventsX, GameSleeperX, HeroX, TickSleeperX } from "immortal-core/Imports"
import { ArrayExtensions, dotaunitorder_t, Entity, EventsSDK, ExecuteOrder, GameState, Input, Tower } from "wrapper/Imports"
import { BaseOrbwalk } from "../Service/Orbwalk"
import { TargetManager } from "./Target"

export const OrbWallker = new Map<number, BaseOrbwalk>()

export class OrbWallkerManager {

	public static issuedAction = new TickSleeperX()
	public static unitIssuedAction = new GameSleeperX()
	public static issuedActionTimings = new Map<number, number>()

	public static Update(mainHandle?: number) {
		if (this.issuedAction.Sleeping)
			return

		const arrays = [...OrbWallker.entries()].map(x => x[1])
		const allUnits = ArrayExtensions.orderBy(arrays.filter(x => x.Owner.IsValid), x => this.IssuedActionTime(x.Owner.Handle))

		const noOrbwalkUnits: BaseOrbwalk[] = []

		for (const controllable of allUnits) {
			const menuMode = controllable.Menu.ComboKey.is_pressed
				? controllable.Menu.ComboMode
				: controllable.Menu.HarrasMode

			if (!menuMode.BaseState.value || (!menuMode.Attack.value && !menuMode.Move.value))
				continue

			if (!controllable.Owner.IsAlive) {
				this.unitIssuedAction.ResetKey(controllable.Owner.Handle)
				this.issuedActionTimings.delete(controllable.Owner.Handle)
				continue
			}

			if (this.unitIssuedAction.Sleeping(controllable.Owner.Handle))
				continue

			if (!controllable.Orbwalk(TargetManager.Target!) || controllable.Owner.Handle === mainHandle)
				continue

			this.issuedActionTimings.set(controllable.Owner.Handle, GameState.RawGameTime)
			this.unitIssuedAction.Sleep(0.2, controllable.Owner.Handle)
			this.issuedAction.Sleep(0.05)
			return
		}

		if (noOrbwalkUnits.length > 0 && !this.unitIssuedAction.Sleeping(Number.MAX_SAFE_INTEGER))
			this.ControlAllUnits(noOrbwalkUnits)
	}

	public static Dispose() {
		this.issuedAction.ResetTimer()
		this.issuedActionTimings.clear()
		this.unitIssuedAction.FullReset()
	}

	protected static IssuedActionTime(handle: number) {
		const time = this.issuedActionTimings.get(handle)
		if (time === undefined)
			return 0
		return time
	}

	protected static ControlAllUnits(noOrbwalk: BaseOrbwalk[]) {

		if (TargetManager.HasValidTarget) {
			const units = noOrbwalk.filter(x => !(x.Owner.IsInAbilityPhase || x.Owner.IsChanneling) && x.Owner.CanAttack(TargetManager.Target!, x.CanMove() ? 999999 : 0))
				.map(x => x.Owner.BaseOwner)
			if (units.length > 0) {
				ExecuteOrder.PrepareOrder({
					orderType: dotaunitorder_t.DOTA_UNIT_ORDER_ATTACK_TARGET,
					issuers: units,
					target: TargetManager.Target!.BaseOwner,
					queue: false,
					showEffects: false,
				})
			}
		} else {
			const units = noOrbwalk.filter(x => !(x.Owner.IsInAbilityPhase || x.Owner.IsChanneling) && x.Owner.CanMove())
				.map(x => x.Owner.BaseOwner)
			if (units.length > 0) {
				ExecuteOrder.PrepareOrder({
					orderType: dotaunitorder_t.DOTA_UNIT_ORDER_MOVE_TO_POSITION,
					issuers: units,
					position: Input.CursorOnWorld,
					queue: false,
					showEffects: false,
				})
			}
		}
		this.unitIssuedAction.Sleep(0.25, Number.MAX_SAFE_INTEGER)
		this.issuedAction.Sleep(0.08)
	}
}

EventsX.on("AttackStarted", owner => {
	if (!(owner instanceof HeroX) || owner.IsEnemy() || !owner.IsControllable)
		return
	const hero = OrbWallker.get(owner.Handle)
	if (hero === undefined)
		return
	hero.OnAttackStart()
})

EventsSDK.on("TrackingProjectileCreated", proj => {
	const source = proj.Source
	if (!(source instanceof Entity) || source instanceof Tower)
		return
	const find = OrbWallker.get(source.Index)
	if (find === undefined)
		return
	find.MoveSleeper.ResetTimer()
})

EventsX.on("GameEnded", () => {
	OrbWallker.clear()
	OrbWallkerManager.Dispose()
})
