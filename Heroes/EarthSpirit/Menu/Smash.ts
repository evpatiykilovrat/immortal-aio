import { EventsX, GameX, HeroX } from "immortal-core/Imports"
import { LocalPlayer, Menu } from "wrapper/Imports"
import { EarthSpiritMenu } from "./Base"

export interface SmashMenu {
	Key: Menu.KeyBind;
	Heroes: Menu.ImageSelector;
}

const SmashTree = EarthSpiritMenu.menuBase.AddNode("Roll + Smash combo")
export const AllySmashKey = SmashTree.AddKeybind("Key")
export const AllySmashSelector = SmashTree.AddImageSelector("SMASH_TO_ALLY", [], new Map(), "Smash to ally")

export const SmashMenu: SmashMenu = {
	Key: AllySmashKey,
	Heroes: AllySmashSelector,
}

EventsX.on("UnitCreated", async hero => {
	if (!(hero instanceof HeroX) || hero.IsEnemy() || !GameX.IsInGame || LocalPlayer?.Hero?.Index === hero.Handle)
		return
	if (!AllySmashSelector.values.includes(hero.Name)) {
		await AllySmashSelector.values.push(hero.Name)
		await AllySmashSelector.Update()
	}
})

EventsX.on("GameEnded", () => {
	AllySmashSelector.values = []
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["SMASH_TO_ALLY", ""],
	["Smash to ally", "Использовать пинок к союзнику"],
	["Roll + Smash combo", "Ролл + Смэш комбо"],
]))

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["SMASH_TO_ALLY", ""],
]))
