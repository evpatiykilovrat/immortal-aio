import { OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIOTargetable extends AIOUsable {

	public ShouldCast(): boolean {
		const target = TargetManager.Target!
		if (!target.IsVisible || target.IsInvulnerable || target.IsReflectingDamage
			|| (this.Ability.BreaksLinkens && target.IsBlockingAbilities))
			return false

		return true
	}

	public ForceUseAbility(): boolean {
		return this.UseAbility(true)
	}

	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		if (!this.Ability.UseAbility(TargetManager.Target!))
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
