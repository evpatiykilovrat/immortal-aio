import { HitChanceX, OrbWalker } from "immortal-core/Imports";
import { Hero, ProjectileManager } from "wrapper/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class SkywrathMysticFlare extends AIONuke {
	private get ConcussiveShotProjectile() {
		return ProjectileManager.AllTrackingProjectiles.some(proj => {
			if (!proj.ParticlePath.includes("concussive_shot.vpcf") || !(proj.Target instanceof Hero)
				|| proj.Target.IsInvulnerable || !proj.Target.IsAlive
				|| proj.Target.Index !== TargetManager.Target!.Handle)
				return false
			return TargetManager.Target!.Distance(proj.Position) <= 200
		})
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!

		if (target.IdealSpeed <= 260 || this.ConcussiveShotProjectile)
			return true

		const immobile = target.GetImmobilityDuration
		if (immobile < 0.3) {
			const time = (this.Ability.Radius / target.IdealSpeed) + 0.3
			const damage = this.Ability.GetDamage(target)
			if ((damage * time) < target.Health)
				return false
		}
		return true
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		if (this.Owner.HasAghanimsScepter
			&& (target.GetImmobilityDuration > 0 || !target.IsMoving)
			&& !TargetManager.IsValidEnemyHeroes.some(x => !x.Equals(target) && x.Distance(target) < 900)
		) {
			const castPosition = target.InFront(this.Ability.Radius + (target.IsMoving ? 30 : 10))
			if (this.Owner.Distance(castPosition) > this.Ability.CastRange)
				return false
			if (!this.Owner.UseAbility(this.Ability, { intent: castPosition }))
				return false

		} else {
			const input = this.Ability.GetPredictionInput(target)
			input.Delay += 0.3
			const output = this.Ability.GetPredictionOutput(input)
			if (output.HitChance < HitChanceX.Low)
				return false
			if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
				return false
		}

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
