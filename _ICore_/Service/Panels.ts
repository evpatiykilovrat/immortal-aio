import { PathX, UnitX } from "immortal-core/Imports";
import { Color, GUIInfo, Input, LocalPlayer, Menu, RendererSDK, Vector2 } from "wrapper/Imports";
import { ModeCombo } from "../../Menu/Base";

export class SwitchPanel {

	public static Draw(menu: ModeCombo, unit: UnitX) {
		if (menu.Panel === undefined)
			return
		if (!menu.Panel.PanelState.value || unit.Handle !== LocalPlayer!.Hero!.Index)
			return
		const ItemSize = this.ItemSize(menu)
		const Position = this.GetItemPanelPos(menu)
		this.Menu(menu).forEach(selector => {
			for (const name of selector.values) {
				if (!menu.Panel!.PanelSelector.IsEnabled(name))
					continue
				const Image = name.includes("item_")
					? PathX.DOTAItems(name)
					: PathX.DOTAAbilities(name)
				const opacity = menu.Panel!.PanelOpacityState.value * 2.55
				RendererSDK.Image(Image, Position, -1, ItemSize, Color.White.SetA(opacity))
				RendererSDK.OutlinedRect(Position, ItemSize, 1, selector.IsEnabled(name) ? Color.Green.SetA(opacity) : Color.Red.SetA(opacity))

				Position.AddForThis(new Vector2(
					menu.Panel!.PanelIconsPosition.selected_id === 0 ? ItemSize.y + 1 : 0,
					menu.Panel!.PanelIconsPosition.selected_id === 1 ? ItemSize.x + 1 : 0,
				))
			}
		})
	}

	public static MouseLeftDown(menu: ModeCombo) {
		if (menu.Panel === undefined || !menu.Panel.PanelState.value)
			return
		const cursor = Input.CursorOnScreen
		const ItemSize = this.ItemSize(menu)
		const Position = this.GetItemPanelPos(menu)
		SwitchPanel.Menu(menu).forEach(selector => {
			for (const name of selector.values) {
				if (!menu.Panel!.PanelSelector.IsEnabled(name))
					continue
				if (cursor.IsUnderRectangle(Position.x, Position.y, ItemSize.x, ItemSize.y + 1)) {
					selector.enabled_values.set(name, !selector.IsEnabled(name))
					Menu.Base.SaveConfigASAP = true
				}
				Position.AddForThis(new Vector2(
					menu.Panel!.PanelIconsPosition.selected_id === 0 ? ItemSize.y + 1 : 0,
					menu.Panel!.PanelIconsPosition.selected_id === 1 ? ItemSize.x + 1 : 0,
				))
			}
		})
	}

	private static Menu(menu: ModeCombo) {
		return [menu.Abilities, menu.Items]
	}

	private static GetItemPanelPos(menu: ModeCombo) {
		if (menu.Panel === undefined)
			return new Vector2()
		const screen_size = RendererSDK.WindowSize
		return new Vector2(
			GUIInfo.ScaleWidth(menu.Panel.PanelPosistionX.value, screen_size),
			GUIInfo.ScaleHeight(menu.Panel.PanelPosistionY.value, screen_size),
		)
	}

	private static ItemSize(menu: ModeCombo) {
		if (menu.Panel === undefined)
			return new Vector2()
		const screen_size = RendererSDK.WindowSize
		return new Vector2(
			GUIInfo.ScaleWidth(menu.Panel.PanelSize.value, screen_size),
			GUIInfo.ScaleHeight(menu.Panel.PanelSize.value, screen_size),
		)
	}
}
