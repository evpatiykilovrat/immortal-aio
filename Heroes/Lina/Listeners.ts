import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { LAbilitiesCreated, LinaMode, LinaOrbWalk } from "./Lina";
import { LinaMenu } from "./menu";

HeroService.RegisterHeroModule(LinaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(LinaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(LinaMenu)

function onTick(unit: UnitX) {
	if (!LinaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = LinaMenu.TargetMenuTree
	TargetManager.TargetLocked = LinaMenu.ComboKey.is_pressed || LinaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		LinaMode,
		LinaMenu.ComboMode,
		LinaMenu,
		LinaMenu.ComboKey,
	)

	ComboMode(
		unit,
		LinaMode,
		LinaMenu.HarrasMode,
		LinaMenu,
		LinaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!LinaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		LinaMenu.TargetMenuTree.DrawTargetParticleColor,
		LinaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(LinaMenu.ComboMode, owner)
	SwitchPanel.Draw(LinaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!LinaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (LinaMenu.ComboKey.is_pressed || LinaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!LinaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((LinaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!LinaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(LinaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(LinaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (LinaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (LinaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, LinaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new LinaOrbWalk(owner, LinaMenu))
}

function onDestoyed(owner: UnitX) {
	if (LinaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== LinaMenu.npc_name)
		return
	LAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (LinaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
