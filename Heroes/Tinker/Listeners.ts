import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { TinkerMenu } from "./menu";
import { SAbilitiesCreated,TinkerNarkoman} from "./Tinker";

HeroService.RegisterHeroModule(TinkerMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(TinkerMenu, oldState)
TargetManager.DestroyParticleDrawTarget(TinkerMenu)

function onTick(unit: UnitX) {
	if (!TinkerMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = TinkerMenu.TargetMenuTree
	TargetManager.TargetLocked = TinkerMenu.ComboKey.is_pressed || TinkerMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		TinkerNarkoman,
		TinkerMenu.ComboMode,
		TinkerMenu,
		TinkerMenu.ComboKey,
	)

	ComboMode(
		unit,
		TinkerNarkoman,
		TinkerMenu.HarrasMode,
		TinkerMenu,
		TinkerMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!TinkerMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		TinkerMenu.TargetMenuTree.DrawTargetParticleColor,
		TinkerMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(TinkerMenu.ComboMode, owner)
	SwitchPanel.Draw(TinkerMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!TinkerMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (TinkerMenu.ComboKey.is_pressed || TinkerMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!TinkerMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(TinkerMenu.ComboMode)
		SwitchPanel.MouseLeftDown(TinkerMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((TinkerMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (TinkerMenu.HarassKey.assigned_key as VMouseKeys) === key))
	return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!TinkerMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((TinkerMenu.ComboKey.assigned_key as VKeys) === key || (TinkerMenu.HarassKey.assigned_key as VKeys) === key)
}

function onCreated(owner: UnitX) {
	if (TinkerMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, TinkerMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, TinkerMenu))
}

function onDestoyed(owner: UnitX) {
	if (TinkerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== TinkerMenu.npc_name)
		return
	SAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (TinkerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
