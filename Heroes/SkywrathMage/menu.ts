import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const SkywrathMage = BaseHeroMenu({
	NpcName: "npc_dota_hero_skywrath_mage",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_gungir",
		"item_rod_of_atos",
		"item_veil_of_discord",
		"item_force_staff",
		"item_blink",
		"item_orchid",
		"item_bloodthorn",
		"item_sheepstick",
		"item_ethereal_blade",
		"item_nullifier",
		"item_dagon_5",
	],
	Abilities: [
		"skywrath_mage_arcane_bolt",
		"skywrath_mage_concussive_shot",
		"skywrath_mage_ancient_seal",
		"skywrath_mage_mystic_flare",
	],
	LinkenBreak: ["skywrath_mage_arcane_bolt", "skywrath_mage_ancient_seal"],
})
