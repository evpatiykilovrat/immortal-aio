import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const LinaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_lina",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_blink",
		"item_orchid",
		"item_force_staff",
		"item_hurricane_pike",
		"item_ethereal_blade",
		"item_dagon_5",
		"item_cyclone",
		"item_sheepstick",
		"item_bloodthorn",
		"item_veil_of_discord",
		"item_rod_of_atos",
	],
	Abilities: [
		"lina_dragon_slave",
		"lina_light_strike_array",
		"lina_laguna_blade",
	],
})
