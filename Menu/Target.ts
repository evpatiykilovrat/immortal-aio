import { Color, Menu, Menu as MenuSDK } from "wrapper/Imports"

export interface TargetMenu {
	BaseState: MenuSDK.Toggle;
	LockTarget: MenuSDK.Toggle;
	FocusTarget: MenuSDK.Dropdown;
	DeathSwitch: MenuSDK.Toggle;
	DrawTargetTree: MenuSDK.Node;
	DrawTargetParticle: MenuSDK.Toggle;
	DrawTargetParticleColor: MenuSDK.ColorPicker;
	DrawTargetParticleColor2: MenuSDK.ColorPicker;
}

export const TargetMenu = (menuSettings: MenuSDK.Node, BaseState: MenuSDK.Toggle): TargetMenu => {
	const FocusTarget = menuSettings.AddDropdown("Focus target", [
		"Near mouse",
		"Near hero",
		"Lowest health",
	])
	const LockTarget = menuSettings.AddToggle("Lock target", true, "Lock target while combo is active")
	const DeathSwitch = menuSettings.AddToggle("Death switch", true, "Auto switch target if previous died")
	const DrawTargetTree = menuSettings.AddNode("Draw target")
	const DrawTargetParticle = DrawTargetTree.AddToggle("Draw target particle", true)
	const DrawTargetParticleColor = DrawTargetTree.AddColorPicker("Color 1", new Color(142, 167, 0), "Color actiated combo/harass")
	const DrawTargetParticleColor2 = DrawTargetTree.AddColorPicker("Color 2", Color.Red, "Color deactivated combo/harass")

	return {
		BaseState,
		LockTarget,
		FocusTarget,
		DeathSwitch,
		DrawTargetTree,
		DrawTargetParticle,
		DrawTargetParticleColor,
		DrawTargetParticleColor2,
	}
}

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Color 1", "Цвет 1"],
	["Color 2", "Цвет 2"],
	["Focus target", "Фокусировка на"],
	["Lock target", "Фиксация цели"],
	["Near mouse", "Ближайший к мышке"],
	["Near hero", "Ближайший к герою"],
	["Draw target", "Рисовать на цели"],
	["Lowest health", "Наименьшее здоровье"],
	["Death switch", "Смена цели при смерти"],
	["Draw target particle", "Рисовать линию к цели"],
	["Lock target while combo is active", "Не изменять цель в комбо"],
	["Color actiated combo/harass", "Цвет линии при активном комбо/харасе"],
	["Color deactivated combo/harass", "Цвет линии при выключеном комбо/харасе"],
	["Auto switch target if previous died", "Автоматическая смена цели, если текущая умерла"],
]))
