import { AbilityX, AbyssalBlade, BlinkDagger, Bloodthorn, DiffusalBlade, EventsX, Gleipnir, GodsStrength, MaskOfMadness, OrchidMalevolence, RodOfAtos, ScytheOfVyse, StormHammer, UnitX, Warcry } from "immortal-core/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIOSpeedBuff, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";

const data = {
	bolt: undefined as Nullable<AIODisable>,
	mom: undefined as Nullable<AIOBuff>,
	gods: undefined as Nullable<AIOBuff>,
	hex: undefined as Nullable<AIODisable>,
	atos: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	diffusal: undefined as Nullable<AIODebuff>,
	abyssal: undefined as Nullable<AIODisable>,
	warcry: undefined as Nullable<AIOSpeedBuff>,
	blink: undefined as Nullable<AIOBlink>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}

export const SAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof MaskOfMadness)
		data.mom = new AIOBuff(abil)
	if (abil instanceof StormHammer)
		data.bolt = new AIODisable(abil)
	if (abil instanceof Warcry)
		data.warcry = new AIOSpeedBuff(abil)
	if (abil instanceof GodsStrength)
		data.gods = new AIOBuff(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
}

export const SvenMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.gods))
		return true

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.warcry))
		return true

	if (helper.UseAbility(data.bolt))
		return true

	if (!helper.CanBeCasted(data.gods, false, false))
		if (helper.UseAbilityIfNone(data.mom, data.bolt))
			return true

	if (helper.UseAbility(data.blink, 450, 100))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
