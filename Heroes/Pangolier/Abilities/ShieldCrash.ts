import { ActiveAbility } from "immortal-core/Imports";
import { AIONuke, TargetManager } from "_ICore_/Index";

export class PangoShieldCrash extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public CanBeCasted(channelingCheck: boolean) {
		if (!super.CanBeCasted(channelingCheck))
			return false

		const target = TargetManager.Target!

		if (this.Owner.HasBuffByName("modifier_pangolier_gyroshell")
			&& (this.Owner.GetAngle(target.Position) < 0.75) && (this.Owner.Distance(target) < this.Ability.CastRange))
			return false

		return true
	}
}
