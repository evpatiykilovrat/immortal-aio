import { ActiveAbility } from "immortal-core/Imports";
import { AIODisable, TargetManager } from "_ICore_/Index";
import { BaseMenu } from "../../../Menu/Base";

export class AIOBlackHole extends AIODisable {
	constructor(ability: ActiveAbility, private menu: BaseMenu) {
		super(ability)
	}

	public CanHit() {
		if (!super.CanHit())
			return false

		const counts = this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1)

		if (!this.Ability.CanHit(TargetManager.Target!, TargetManager.IsValidEnemyHeroes, counts))
			return false

		return true
	}
}
