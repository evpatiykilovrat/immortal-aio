import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { LAbilitiesCreated, LegionMode } from "./Legion";
import { LegionMenu } from "./menu";

HeroService.RegisterHeroModule(LegionMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(LegionMenu, oldState)
TargetManager.DestroyParticleDrawTarget(LegionMenu)

function onTick(unit: UnitX) {
	if (!LegionMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = LegionMenu.TargetMenuTree
	TargetManager.TargetLocked = LegionMenu.ComboKey.is_pressed || LegionMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		LegionMode,
		LegionMenu.ComboMode,
		LegionMenu,
		LegionMenu.ComboKey,
	)

	ComboMode(
		unit,
		LegionMode,
		LegionMenu.HarrasMode,
		LegionMenu,
		LegionMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!LegionMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		LegionMenu.TargetMenuTree.DrawTargetParticleColor,
		LegionMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(LegionMenu.ComboMode, owner)
	SwitchPanel.Draw(LegionMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!LegionMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (LegionMenu.ComboKey.is_pressed || LegionMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!LegionMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((LegionMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!LegionMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(LegionMenu.ComboMode)
		SwitchPanel.MouseLeftDown(LegionMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (LegionMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (LegionMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, LegionMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, LegionMenu))
}

function onDestoyed(owner: UnitX) {
	if (LegionMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== LegionMenu.npc_name)
		return
	LAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (LegionMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
