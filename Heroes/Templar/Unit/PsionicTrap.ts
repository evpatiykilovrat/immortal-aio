import { EventsX, SelfTrap } from "immortal-core/Imports"
import { ModeCombo } from "Menu/Base"
import { ArrayExtensions } from "wrapper/Imports"
import { AbilityHelper } from "_ICore_/Index"
import { TrapExplode } from "../Abilities/TrapExplode"

let trapExplode: TrapExplode[] = []

EventsX.on("AbilityCreated", abil => {
	if (abil instanceof SelfTrap) {
		const find = trapExplode.find(x => x.Ability.Handle === abil.Handle)
		if (find === undefined)
			trapExplode.push(new TrapExplode(abil))
	}
})

EventsX.on("AbilityDestroyed", abil => {
	if (abil instanceof SelfTrap) {
		const find = trapExplode.find(x => x.Ability.Handle === abil.Handle)
		if (find !== undefined)
			ArrayExtensions.arrayRemove(trapExplode, find)
	}
})

export const TrapExplodeCombo = (menu: ModeCombo) => {
	return trapExplode.some(trap => {
		const abilityHelper = new AbilityHelper(trap.Owner, menu, true)
		return abilityHelper.UseAbility(trap)
	})
}

EventsX.on("GameEnded", () => {
	trapExplode = []
})
