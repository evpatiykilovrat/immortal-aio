import { AbilityX, BladeMail, BlinkDagger, BoulderSmash, Dagon, EtherealBlade, EventsX, ForceStaff, GeomagneticGrip, HeavensHalberd, Magnetize, OrbWalker, RollingBoulder, ScytheOfVyse, ShivasGuard, SpiritVessel, StoneRemnant, UnitX, UrnOfShadows, Vector3Extend, VeilOfDiscord } from "immortal-core/Imports";
import { ArrayExtensions, Flow_t } from "wrapper/Imports";
import { AbilityHelper, AIOAoe, AIOBlink, AIODebuff, AIODisable, AIOEtherealBlade, AIOForceStaff, AIONuke, AIOShield, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { SpiritBoulderSmash } from "./Abilities/BoulderSmash";
import { SpiritGeomagneticGrip } from "./Abilities/GeomagneticGrip";
import { SpiritRollingBoulder } from "./Abilities/RollingBoulder";
import { SpiritStoneRemnant } from "./Abilities/StoneRemnant";
import { SmashMenu } from "./Menu/Index";

const data = {
	mag: undefined as Nullable<AIOAoe>,
	veil: undefined as Nullable<AIODebuff>,
	blink: undefined as Nullable<AIOBlink>,
	smash: undefined as Nullable<SpiritBoulderSmash>,
	stone: undefined as Nullable<SpiritStoneRemnant>,
	rolling: undefined as Nullable<SpiritRollingBoulder>,
	grip: undefined as Nullable<SpiritGeomagneticGrip>,
	force: undefined as Nullable<AIOForceStaff>,
	vessel: undefined as Nullable<AIODebuff>,
	urn: undefined as Nullable<AIODebuff>,
	bladeMail: undefined as Nullable<AIOShield>,
	shiva: undefined as Nullable<AIODebuff>,
	halberd: undefined as Nullable<AIODisable>,
	hex: undefined as Nullable<AIODisable>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
	dagon: undefined as Nullable<AIONuke>,
}

export const EAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof SpiritVessel)
		data.vessel = new AIODebuff(abil)
	if (abil instanceof UrnOfShadows)
		data.urn = new AIODebuff(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof HeavensHalberd)
		data.halberd = new AIODisable(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
	if (abil instanceof Magnetize)
		data.mag = new AIOAoe(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof StoneRemnant)
		data.stone = new SpiritStoneRemnant(abil)
	if (abil instanceof BoulderSmash)
		data.smash = new SpiritBoulderSmash(abil)
	if (abil instanceof RollingBoulder)
		data.rolling = new SpiritRollingBoulder(abil)
	if (abil instanceof GeomagneticGrip)
		data.grip = new SpiritGeomagneticGrip(abil)
}

export const EarthSpiritMode = (unit: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 400, 200)) {
		data.rolling?.Ability.ActionSleeper.Sleep(0.8)
		HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
		return true
	}

	if (helper.UseAbility(data.force, 400, 200)) {
		data.rolling?.Ability.ActionSleeper.Sleep(0.5)
		HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
		return true
	}

	if (helper.UseAbility(data.ethereal))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.CanBeCasted(data.rolling)) {
		if (helper.UseAbility(data.rolling))
			return true
		if (helper.CanBeCasted(data.stone, false)) {
			if (helper.ForceUseAbility(data.stone, true)) {
				data.smash?.Ability.ActionSleeper.Sleep(1)
				return true
			}
		} else {
			if (helper.ForceUseAbility(data.rolling, true))
				return true
		}
	}

	if (helper.CanBeCasted(data.smash)) {

		if (helper.UseAbility(data.smash))
			return true

		if (helper.CanBeCasted(data.stone, false)) {
			if (data.stone?.UseAbility(true, data.smash))
				return true
		} else {
			if (helper.ForceUseAbility(data.smash, true))
				return true
		}
	}

	if (helper.CanBeCasted(data.grip)) {
		if (helper.UseAbility(data.grip))
			return true
		if (helper.CanBeCasted(data.stone, false)) {
			if (data.stone?.UseAbility(true, data.grip))
				return true
		} else {
			if (helper.ForceUseAbility(data.grip, true))
				return true
		}
	}

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.halberd))
		return true

	if (helper.UseAbility(data.mag))
		return true

	if (helper.UseAbility(data.vessel))
		return true

	if (helper.UseAbility(data.urn))
		return true

	if (helper.UseAbility(data.shiva))
		return true

	if (helper.UseAbility(data.bladeMail))
		return true

	if (helper.UseAbility(data.stone))
		return true

	if (helper.UseAbilityIfNone(data.dagon, data.ethereal))
		return true

	return false
}

export const RollSmashCombo = (unit: UnitX, menu: SmashMenu) => {
	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return

	const target = TargetManager.Target!
	const distance = unit.Distance(target.Position)
	const abilityHelper = new AbilityHelper(target)

	if (unit.HasBuffByName("modifier_earth_spirit_rolling_boulder_caster")) {
		if (abilityHelper.ForceUseAbility(data.stone, true)) {
			HERO_DATA.ComboSleeper.Sleep(3, unit.Handle)
			return
		}
	}

	const ally = ArrayExtensions.orderBy(TargetManager.AllyHeroes.filter(x => !x.Equals(unit)
		&& menu.Heroes.IsEnabled(x.Name)
		&& x.Distance(target) > 300
		&& x.Distance(target) < 1500), x => x.Distance(target))[0]

	if (ally === undefined)
		return

	if (target.HasBuffByName("modifier_earth_spirit_boulder_smash")) {
		if (abilityHelper.CanBeCasted(data.rolling, false, false) && data.rolling?.SimpleUseAbility(target.Position)) {
			HERO_DATA.ComboSleeper.Sleep(0.5, unit.Handle)
			return
		}
		return
	}

	if (data.smash?.Ability.CanBeCasted() !== true)
		return

	if (distance < data.smash.Ability.CastRange + 100) {
		if (Vector3Extend.AngleBetween(unit.Position, target.Position, ally.Position) < 30) {
			data.smash.Ability.UseAbility(target)
			HERO_DATA.ComboSleeper.Sleep(0.3, unit.Handle)
			return;
		}

		if (target.GetImmobilityDuration > 0.5) {
			unit.Move(target.Position.Extend2D(ally.Position, -100))
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			HERO_DATA.ComboSleeper.Sleep(0.1, unit.Handle)
			return
		}
	}
	if (abilityHelper.CanBeCasted(data.blink)) {
		const predicted = target.PredictedPosition(unit.GetTurnTime(target.Position) + (GetAvgLatency(Flow_t.IN) / 2) + 0.2)
		const position = predicted.Extend2D(ally.Position, -125)
		if (abilityHelper.UseAbility(data.blink, position)) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
			return
		}
		return
	}

	if (abilityHelper.CanBeCasted(data.rolling)) {
		if (abilityHelper.UseAbility(data.rolling))
			return
		if (abilityHelper.CanBeCasted(data.stone, false)) {
			if (abilityHelper.ForceUseAbility(data.stone, true))
				return
		} else {
			if (abilityHelper.ForceUseAbility(data.rolling, true))
				return
		}
	}
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
