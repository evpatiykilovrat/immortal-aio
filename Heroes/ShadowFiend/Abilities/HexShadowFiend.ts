import { ActiveAbility, isIDisable, isIShield, MantaStyle } from "immortal-core/Imports";
import { AIODisable, TargetManager } from "_ICore_/Index";

export class HexShadowFiend extends AIODisable {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false
		const target = TargetManager.Target!
		return target.Abilities.some(x => (isIShield(x) || isIDisable(x) || x instanceof MantaStyle) && (x.CanBeCasted(false) || x.Cooldown <= 2))
	}
}
