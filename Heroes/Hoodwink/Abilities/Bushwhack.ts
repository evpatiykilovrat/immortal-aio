// import { ActiveAbility, EntityX, ParticlesX, PRectangle } from "immortal-core/Imports";
// import { Vector3 } from "wrapper/Imports";
// import { AIODisable, TargetManager } from "_ICore_/Index";

// const sdk = new ParticlesX()
// export class AIOBushwhack extends AIODisable {
// 	private readonly CPosition = new Vector3()

// 	constructor(ability: ActiveAbility) {
// 		super(ability)
// 	}

// 	public CanHit() {
// 		const target = TargetManager.Target!
// 		const input = this.Ability.GetPredictionInput(target)
// 		input.Delay += this.Owner.Distance(target) / this.Ability.Speed
// 		const output = this.Ability.GetPredictionOutput(input)
// 		this.CPosition.CopyFrom(output.CastPosition)

// 		let polygon = new PRectangle(target.Position, this.CPosition, this.Ability.Radius)

// 		const availableTrees = EntityX.Tree.filter(x => x.IsAlive && x.Distance2D(target.Position) < this.Ability.Radius)

// 		for (const tree of availableTrees) {
// 			if (polygon.IsInside(tree.Position))
// 				return false
// 		}

// 		const chainEndPosition = target.Position.Extend2D(this.CPosition, this.Ability.Radius)
// 		polygon = new PRectangle(this.CPosition, chainEndPosition, this.Ability.Radius)

// 		for (const tree of availableTrees) {
// 			if (polygon.IsInside(tree.Position)) {
// 				this.CPosition.CopyFrom(tree.Position)
// 				return true
// 			}
// 		}

// 		sdk.DrawCircle(this.Owner.Handle, this.Owner.BaseOwner, this.Ability.Radius, {
// 			Position: this.CPosition,
// 		})

// 		return false
// 	}

// 	public UseAbility(aoe: boolean) {
// 		return super.UseAbility(aoe)
// 	}
// }
