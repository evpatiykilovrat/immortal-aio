import { ActiveAbility, HitChanceX, SkillShotTypeX, TimberChain, UnitX, WhirlingDeath } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, AIOUsable, TargetManager } from "_ICore_/Index";

export class BlinkDaggerTimbersaw extends AIOBlink {

	private readonly blinkPosition = new Vector3()

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (this.Owner.HasBuffByName("modifier_shredder_timber_chain"))
			return false

		return true
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {

		if (this.Owner.Distance(target) < 600)
			return false

		const chain = args.find(x => x?.Ability instanceof TimberChain)
		if (chain === undefined)
			return true

		this.blinkPosition.CopyFrom(target.Position)
		const whirlingDeath = args.find(x => x?.Ability instanceof WhirlingDeath)
		if (whirlingDeath !== undefined) {
			const input = whirlingDeath.Ability.GetPredictionInput(target, TargetManager.EnemyHeroes)
			input.Range += this.Ability.CastRange
			input.CastRange = this.Ability.CastRange
			input.SkillShotType = SkillShotTypeX.Circle
			const output = whirlingDeath.Ability.GetPredictionOutput(input)
			if (output.HitChance < HitChanceX.Low)
				return false
			this.blinkPosition.CopyFrom(output.CastPosition)
		}

		if (this.Owner.Distance(this.blinkPosition) < this.Ability.CastRange)
			return true

		return false
	}

	public UseAbility(aoe: boolean) {
		if (!this.Ability.UseAbility(TargetManager.Target!, this.blinkPosition))
			return false
		this.Ability.ActionSleeper.Sleep(this.Ability.GetCastDelay(TargetManager.Target!))
		return true
	}
}
