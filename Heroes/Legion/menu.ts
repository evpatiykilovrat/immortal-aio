import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const LegionMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_legion_commander",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_lotus_orb",
		"item_black_king_bar",
		"item_solar_crest",
		"item_medallion_of_courage",
		"item_mjollnir",
		"item_armlet",
		"item_abyssal_blade",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_blade_mail",
		"item_shivas_guard",
	],
	Abilities: [
		"legion_commander_overwhelming_odds",
		"legion_commander_press_the_attack",
		"legion_commander_duel",
	],
	IgnoreInvisDefault: false,
})
