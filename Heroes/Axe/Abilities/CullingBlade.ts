import { ActiveAbility } from "immortal-core/Imports";
import { Courier } from "wrapper/Imports";
import { AIONuke, TargetManager } from "_ICore_/Index";

export class AxeCullingBlade extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldCast() {
		const target = TargetManager.Target!
		if (target.BaseOwner instanceof Courier && target.IsVisible && !target.IsInvulnerable)
			return true

		if (!super.ShouldCast())
			return false

		const targetHp = target.Health + (target.HealthRegeneration * this.Ability.CastPoint) + 15

		const damage = this.Ability.GetDamage(target)
		if (damage < targetHp)
			return false

		return true
	}
}
