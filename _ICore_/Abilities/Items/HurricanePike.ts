import { HERO_DATA } from "../../data";
import { TargetManager } from "../../Manager/Target";
import { AIOForceStaff } from "../ForceStaff";

export class AIOHurricanePike extends AIOForceStaff {

	public UseAbilityOnTarget() {
		const target = TargetManager.Target!

		if (target.IsRanged)
			return false

		if (target.IsLinkensProtected || target.IsInvulnerable || target.IsUntargetable)
			return false

		if (target.IsStunned || target.IsHexed || target.IsRooted || target.IsDisarmed)
			return false

		if (!this.Ability.CanHit(target))
			return false;

		if (this.Owner.Distance(target) > 350)
			return false

		if (!this.Ability.UseAbility(target))
			return false

		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		return true
	}
}
