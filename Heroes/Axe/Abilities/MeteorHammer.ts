import { ActiveAbility, HitChanceX, MeteorHammer, OrbWalker, UnitX } from "immortal-core/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class AxeMeteorHammer extends AIODisable  {

	constructor(ability: ActiveAbility & MeteorHammer) {
		super(ability)
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target, TargetManager.EnemyHeroes)
		input.Delay -= (this.Ability as MeteorHammer).ChannelTime
		const output = this.Ability.GetPredictionOutput(input)

		if (output.HitChance < HitChanceX.Low)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false

		const hitTime = this.Ability.GetHitTime(output.CastPosition) + 0.5
		const delay = this.Ability.GetCastDelay(output.CastPosition)
		target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	protected ChainStun(target: UnitX, invulnerability: boolean) {
		return true
	}
}
