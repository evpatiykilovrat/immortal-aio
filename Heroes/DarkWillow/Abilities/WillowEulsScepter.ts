import { ActiveAbility, CursedCrown, UnitX } from "immortal-core/Imports"
import { AIOEulsScepter, AIOUsable } from "_ICore_/Index"

export class WillowEulsScepter extends AIOEulsScepter {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const crown = args.find(x => x.Ability instanceof CursedCrown)
		if (crown !== undefined)
			return true
		const crownDebuff = target.GetBuffByName("modifier_dark_willow_cursed_crown")
		if (crownDebuff === undefined || crownDebuff.RemainingTime < this.Ability.Duration + 0.1)
			return false
		return true
	}
}
