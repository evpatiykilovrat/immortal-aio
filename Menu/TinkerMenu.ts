import { PathX } from "immortal-core/Imports"
import { Attributes, Menu as MenuSDK } from "wrapper/Imports"
import { OrbwalkMenu } from "./Orbwalk"
import { CreateSwitchPanel, SwitchPanel } from "./Panel"
import { TargetMenu } from "./Target"

export let Base = MenuSDK.AddEntryDeep(["Heroes"])

export interface ComboSlider {
	name?: string
	min?: number
	max?: number
	default?: number
	tooltip?: string
	imageName?: string
}

export interface ModeCombo {
	Panel?: SwitchPanel;
	Items: MenuSDK.ImageSelector;
	Abilities: MenuSDK.ImageSelector;
	Move: MenuSDK.Toggle;
	Attack: MenuSDK.Toggle;
	IgnoreInvis: MenuSDK.Toggle;
	BaseState: MenuSDK.Toggle;
}

export interface MenuComboOption {
	NpcName: string
	Items: string[],
	Abilities: string[],
	KillstealItems: string[],
	KillstealAbillities: string[],
	DeffendSpell: string[],
	NodeAttribute: Attributes,
	HarassItems?: string[],
	HarassAbilities?: string[],
	LinkenBreak?: string[],
	HitMenu?: ComboSlider
	FailSafeDefault?: boolean
	IgnoreInvisDefault?: boolean
}

export interface ShieldsMenu {
	Sphere: MenuSDK.ImageSelector;
	Shields: MenuSDK.ImageSelector;
}

export interface BaseMenu {
	npc_name: string;
	menuBase: MenuSDK.Node;
	BaseState: MenuSDK.Toggle;
	SetFailsafe: MenuSDK.Toggle;
	SettingsTree: MenuSDK.Node;
	TargetMenuTree: TargetMenu;
	/** Combo */
	ComboKey: MenuSDK.KeyBind;
	ComboSettings: MenuSDK.Node;
	ComboItemMenu: MenuSDK.ImageSelector;
	ComboAbilityMenu: MenuSDK.ImageSelector;
	ComboSettingsMove: MenuSDK.Toggle;
	ComboSettingsAttack: MenuSDK.Toggle;
	/** Harass */
	HarassKey: MenuSDK.KeyBind;
	HarassSettings: MenuSDK.Node;
	HarassItemMenu: MenuSDK.ImageSelector;
	HarassAbilityMenu: MenuSDK.ImageSelector;
	HarassSettingsMove: MenuSDK.Toggle;
	HarassSettingsAttack: MenuSDK.Toggle;
	OrbWallkerMenuTree: OrbwalkMenu,
	Shields: ShieldsMenu;
	ComboMode: ModeCombo;
	HarrasMode: ModeCombo;
	ComboHitCountMenu?: Nullable<MenuSDK.Slider>
	HarassHitCountMenu?: Nullable<MenuSDK.Slider>
}

export const LinkenItemsX: string[] = [
	"item_abyssal_blade",
	"item_book_of_shadows",
	"item_force_boots",
	"item_psychic_headband",
	"item_heavens_halberd",
	"item_diffusal_blade",
	"item_hurricane_pike",
	"item_dagon_5",
	"item_force_staff",
	"item_cyclone",
	"item_wind_waker",
	"item_orchid",
	"item_bloodthorn",
	"item_rod_of_atos",
	"item_nullifier",
	"item_ethereal_blade",
	"item_sheepstick",
]

export const BaseHeroMenu = (option: MenuComboOption) => {
	let BaseTreeAll: MenuSDK.Node
	const npc_name = option.NpcName
	switch (option.NodeAttribute) {
		case Attributes.DOTA_ATTRIBUTE_AGILITY:
			BaseTreeAll = AgilityMenu.AddNode(npc_name, PathX.Heroes(npc_name))
			break;
		case Attributes.DOTA_ATTRIBUTE_INTELLECT:
			BaseTreeAll = IntellectMenu.AddNode(npc_name, PathX.Heroes(npc_name))
			break;
		default:
			BaseTreeAll = StrengthMenu.AddNode(npc_name, PathX.Heroes(npc_name))
			break;
	}
	return CreateHeroMenu(BaseTreeAll, option)
}

export const CreateHeroMenu = (menuBase: MenuSDK.Node, option: MenuComboOption): BaseMenu => {

	const npc_name = option.NpcName
	const BaseState = menuBase.AddToggle("State", false)

	const ComboTree = menuBase.AddNode("Combo")
	const ComboPanel = CreateSwitchPanel(ComboTree, [...option.Abilities, ...option.Items])

	const ComboSettings = ComboTree.AddNode("Settings")
	const ComboSettingsMove = ComboSettings.AddToggle("COMBO_MOVE", true)
	const ComboSettingsAttack = ComboSettings.AddToggle("COMBO_ATTACK", true)
	const ComboSettingsIgnoreInvis = ComboSettings.AddToggle("COMBO_IGNORE_INVIS", option?.IgnoreInvisDefault ?? true, "Use abilities when hero is invisible")
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	const ComboSettingsRearm = ComboSettings.AddNode("Настройка Rearm")
	const ComboSettingsCheckRearm = ComboSettingsRearm.AddToggle("Проверка на фулл прокаст", option?.IgnoreInvisDefault ?? true, "Не жмет реарм если остались способности или предметы")
	const ComboAbilityRearm = ComboSettingsRearm.AddImageSelector("ABILITIES_X_COMBO", option.Abilities, new Map(option.Abilities.map(name => [name, true])))
	const ComboItemsRearm = ComboSettingsRearm.AddImageSelector("ITEMS_X_COMBO", option.Items, new Map(option.Items.map(name => [name, true])))



	const ComboSettings2 = ComboSettings.AddNode("Настройка Soul Ring")
	const AutoUseSoulring = ComboSettings2.AddToggle("Соулринг перед реармом", true)
	const AutocheckSoulring = ComboSettings2.AddToggle("Следить за ХП при комбо", true)

	let SoulringSetting
		SoulringSetting = ComboSettings2.AddSlider("До %HP",
			option.HitMenu?.default ?? 1,
			option.HitMenu?.min ?? 1,
			option.HitMenu?.max ?? 100, 0,
			option.HitMenu?.tooltip ?? "Если ХП% ниже чем указано - прекращает использовать Soul Ring")
	

	const ComboSettings3 = ComboSettings.AddNode("Настройка Bottle")
	const AutoUseBottleFriend = ComboSettings3.AddToggle("Мазать ботлом союзников", true)
	const AutoUseSBottleBase = ComboSettings3.AddToggle("Авто хил ботлом на базе", true)


	const ComboSettings4 = ComboSettings.AddNode("Настройка Blink Dagger")
	const BlinkKursor = ComboSettings4.AddToggle("Блинк на позицию курсора", true)
	const BlinkEnemy = ComboSettings4.AddToggle("Блинк к врагу", true)
	
	let BlinkSetting
		BlinkSetting = ComboSettings4.AddSlider("Сохранять дистанцию к врагу",
			option.HitMenu?.default ?? 1,
			option.HitMenu?.min ?? 1,
			option.HitMenu?.max ?? 1200, 0,
			option.HitMenu?.tooltip ?? "Скрипт будет держать указанную дистанцию к врагу")

	const SafeBlinkEnemy = ComboSettings4.AddToggle("Не блинкаться в малом радиусе", true)


	const TinkerKillsteal = menuBase.AddNode("Килл Стил")
	const BlinkKillsteal = TinkerKillsteal.AddToggle("Использовать Blink Dagger", option?.IgnoreInvisDefault ?? true, "Использует Blink к врагу если не хватает радиуса")
	const KillStealAbillity = TinkerKillsteal.AddImageSelector("ABILITIES_X_COMBO", option.KillstealAbillities, new Map(option.KillstealAbillities.map(name => [name, true])))
	const KillStealItems = TinkerKillsteal.AddImageSelector("ITEMS_X_COMBO", option.KillstealItems, new Map(option.KillstealItems.map(name => [name, true])))


	const TinkerSpam = menuBase.AddNode("Спам Ракетами")
	const RocketKey = TinkerSpam.AddKeybind("Key", "3")
	const RocketBlink = TinkerSpam.AddToggle("Blink Dagger к курсору", option?.IgnoreInvisDefault ?? true, "Использует блинк к вашему курсору")

	const TinkerDefendSpam = menuBase.AddNode("Защитный Режим")
	const DefendKey = TinkerDefendSpam.AddKeybind("Key", "4")
	const DefendAbillity = TinkerDefendSpam.AddImageSelector("ABILITIES_X_COMBO", option.DeffendSpell, new Map(option.DeffendSpell.map(name => [name, true])))


////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

	const ComboKey = ComboTree.AddKeybind("Key", "F")
	const ComboAbilityMenu = ComboTree.AddImageSelector("ABILITIES_X_COMBO", option.Abilities, new Map(option.Abilities.map(name => [name, true])))
	const ComboItemMenu = ComboTree.AddImageSelector("ITEMS_X_COMBO", option.Items, new Map(option.Items.map(name => [name, true])))

	let ComboHitCountMenu
	if (option.HitMenu !== undefined && option.HitMenu.imageName !== undefined) {
		const nodeName = ComboSettings.AddNode(option.HitMenu?.name ?? "", option.HitMenu?.imageName)
		ComboHitCountMenu = nodeName.AddSlider("Enemy count",
			option.HitMenu?.default ?? 1,
			option.HitMenu?.min ?? 1,
			option.HitMenu?.max ?? 5, 0,
			option.HitMenu?.tooltip ?? "Use ability only if it will hit equals/more enemies")
	}


	const HarassTree = menuBase.AddNode("Harass")
	const harassItems = (option.HarassItems === undefined ? option.Items : option.HarassItems)
	const harassAbilities = (option.HarassAbilities === undefined ? option.Abilities : option.HarassAbilities)
	const HarassPanel = CreateSwitchPanel(HarassTree, [...harassAbilities, ...harassItems])

	const HarassSettings = HarassTree.AddNode("Settings")
	const HarassSettingsMove = HarassSettings.AddToggle("COMBO_MOVE", true)
	const HarassSettingsAttack = HarassSettings.AddToggle("COMBO_ATTACK", true)
	const HarassSettingsIgnoreInvis = HarassSettings.AddToggle("COMBO_IGNORE_INVIS", option?.IgnoreInvisDefault ?? true, "Use abilities when hero is invisible")

	const HarassKey = HarassTree.AddKeybind("Key")
	const HarassAbilityMenu = HarassTree.AddImageSelector("ABILITIES_X_HARASS", harassAbilities, new Map(harassAbilities.map(name => [name, true])))
	const HarassItemMenu = HarassTree.AddImageSelector("ITEMS_X_HARASS", harassItems, new Map(harassItems.map(name => [name, true])))

	let HarassHitCountMenu
	if (option.HitMenu !== undefined && option.HitMenu.imageName !== undefined) {
		const nodeName = HarassSettings.AddNode(option.HitMenu?.name ?? "", option.HitMenu?.imageName)
		HarassHitCountMenu = nodeName.AddSlider("Enemy count",
			option.HitMenu?.default ?? 1,
			option.HitMenu?.min ?? 1,
			option.HitMenu?.max ?? 5, 0,
			option.HitMenu?.tooltip ?? "Use ability only if it will hit equals/more enemies")
	}

	

	const SphereTree = menuBase.AddNode("Linken's breaker")
	const SphereAbilities = SphereTree.AddImageSelector("Linken's Sphere", [...option?.LinkenBreak ?? [], ...LinkenItemsX])
	const SpellShiedAbilities = SphereTree.AddImageSelector("Spells Shield", [...option?.LinkenBreak ?? [], ...LinkenItemsX])

	/** General settings */
	const SettingsTree = menuBase.AddNode("Target's settings")
	const OrbWallkerMenuTree = OrbwalkMenu(menuBase)
	const TargetMenuTree = TargetMenu(SettingsTree, BaseState)
	const SetFailsafe = SettingsTree.AddToggle("Fail safe", option?.FailSafeDefault ?? true, "Cancel ability if it won't hit the target")

	const Shields: ShieldsMenu = {
		Sphere: SphereAbilities,
		Shields: SpellShiedAbilities,
	}

	const ComboMode: ModeCombo = {
		BaseState,
		Items: ComboItemMenu,
		Panel: ComboPanel,
		Abilities: ComboAbilityMenu,
		Move: ComboSettingsMove,
		Attack: ComboSettingsAttack,
		IgnoreInvis: ComboSettingsIgnoreInvis,
	}

	const HarrasMode: ModeCombo = {
		BaseState,
		Items: HarassItemMenu,
		Panel: HarassPanel,
		Abilities: HarassAbilityMenu,
		Move: HarassSettingsMove,
		Attack: HarassSettingsAttack,
		IgnoreInvis: HarassSettingsIgnoreInvis,
	}

	return {
		/** Base data */
		npc_name,
		menuBase,
		BaseState,
		SetFailsafe,
		SettingsTree,
		TargetMenuTree,
		/** Combo */
		ComboKey,
		ComboItemMenu,
		ComboAbilityMenu,
		ComboSettings,
		ComboSettingsMove,
		ComboSettingsAttack,
		/** Harass */
		HarassKey,
		HarassSettings,
		HarassItemMenu,
		HarassAbilityMenu,
		HarassSettingsMove,
		HarassSettingsAttack,
		OrbWallkerMenuTree,
		/** Mode */
		Shields,
		ComboMode,
		HarrasMode,
		ComboHitCountMenu,
		HarassHitCountMenu,
	}
}

export const StrengthMenu = Base.AddNode("Strength", PathX.Images.primary_attribute_strength)
export const AgilityMenu = Base.AddNode("Agility", PathX.Images.primary_attribute_agility)
export const IntellectMenu = Base.AddNode("Intelligence", PathX.Images.primary_attribute_intelligence)
