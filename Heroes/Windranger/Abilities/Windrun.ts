import { ActiveAbility } from "immortal-core/Imports";
import { AIOShield, TargetManager } from "_ICore_/Index";

export class WindrangerWindrun extends AIOShield {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!
		if (target.IsStunned || target.IsHexed || target.IsDisarmed)
			return false

		const distance = this.Owner.Distance(target)
		const attackRange = this.Owner.GetAttackRange(target)
		if (distance > attackRange + 100 && distance < attackRange + 500)
			return true

		if (distance < this.Ability.Radius + 100)
			return true

		if (target.HealthPercentage < 50)
			return true

		return false
	}
}
