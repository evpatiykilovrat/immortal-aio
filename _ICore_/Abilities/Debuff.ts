import { ActiveAbility, GameSleeperX, HitChanceX, IDebuff, isIDisable, OrbWalker } from "immortal-core/Imports";
import { item_diffusal_blade } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIODebuff extends AIOUsable {

	protected AIODebuff: ActiveAbility & IDebuff
	private readonly debuffSleeper = new GameSleeperX<number>();
	private readonly visibleSleeper = new GameSleeperX<number>();

	constructor(ability: ActiveAbility) {
		super(ability)
		this.AIODebuff = ability as ActiveAbility & IDebuff
	}

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Target!))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)

		if (isIDisable(this.Ability))
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(TargetManager.Target!))

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast(): boolean {
		const target = TargetManager.Target!
		const isVisible = target.IsVisible;
		if (this.Ability.UnitTargetCast && !isVisible)
			return false

		if (this.Ability.BaseAbility instanceof item_diffusal_blade && target.GetImmobilityDuration > 0)
			return false

		if (isVisible) {
			if (this.visibleSleeper.Sleeping(target.Handle))
				return false
			const modifier = target.GetBuffByName(this.AIODebuff.DebuffModifierName)
			if (modifier !== undefined) {
				const remainingTime = modifier.RemainingTime
				if (remainingTime === 0)
					return false
				const time = remainingTime - this.Ability.GetHitTime(target)
				if (time > 0) {
					this.debuffSleeper.Sleep(time, target.Handle)
					return false
				}
			}
		} else {
			this.visibleSleeper.Sleep(0.1, target.Handle);
			if (this.debuffSleeper.Sleeping(target.Handle))
				return false
		}

		if (this.Ability.BreaksLinkens && target.IsBlockingAbilities)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable) {
			if (this.AIODebuff.UnitTargetCast)
				return false
			if (!this.ChainStun(target, true))
				return false
		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {

		if (!this.Ability.UseAbility(TargetManager.Target!, TargetManager.EnemyHeroes, HitChanceX.Low))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)

		if (isIDisable(this.Ability)) {
			const hitTime = this.Ability.GetHitTime(TargetManager.Target!)
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, hitTime)
		}

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
