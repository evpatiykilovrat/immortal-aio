import { AbilityX, AbyssalBlade, BlackKingBar, BlinkDagger, Bloodthorn, EtherealBlade, EulsScepterOfDivinity, EventsX, HitChanceX, MaskOfMadness, MinotaurHorn, Nullifier, OrbWalker, OrchidMalevolence, RefresherOrb, RefresherShard, RequiemOfSouls, ScytheOfVyse, Shadowraze, SpiderLegs, UnitX, VeilOfDiscord, WindWaker } from "immortal-core/Imports";
import { ArrayExtensions, GameState } from "wrapper/Imports";
import { AbilityHelper, AIOAbyssalBlade, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIOEtherealBlade, AIOEulsScepter, AIONuke, AIONullifier, AIOShield, AIOSpeedBuff, AIOUntargetable, AIOUsable, BaseOrbwalk, HERO_DATA, OrbWallker, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { HexShadowFiend } from "./Abilities/HexShadowFiend";
import { APCHeroes, APHHeroes } from "./menu";

let lastMove = false
let razeOrbwalk = false
let helper: Nullable<AbilityHelper>

const data = {
	legs: undefined as Nullable<AIOSpeedBuff>,
	requiem: undefined as Nullable<AIONuke>,
	veil: undefined as Nullable<AIODebuff>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	nullifier: undefined as Nullable<AIONullifier>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
	hex: undefined as Nullable<AIODisable>,
	blink: undefined as Nullable<AIOBlink>,
	abyssal: undefined as Nullable<AIOAbyssalBlade>,
	euls: undefined as Nullable<AIOEulsScepter>,
	bkb: undefined as Nullable<AIOShield>,
	horn: undefined as Nullable<AIOShield>,
	mom: undefined as Nullable<AIOBuff>,
	refresher: undefined as Nullable<AIOUntargetable>,
	refresher1: undefined as Nullable<AIOUntargetable>,
	razes: [] as AIONuke[],
}

const RazeCanWaitAttack = (raze: AIOUsable, owner: UnitX, target: UnitX): boolean => {
	const damage = raze.Ability.GetDamage(target)
	if (damage > target.Health)
		return false
	const orbWalker = OrbWallker.get(owner.Handle)
	const input = raze.Ability.GetPredictionInput(target)
	if (orbWalker?.MoveSleeper.Sleeping) {
		input.Delay += (orbWalker?.MoveSleeper.RemainingSleepTime ?? 0)
	} else if (!target.IsMoving || !owner.CanAttack(target, -50)) {
		return false
	} else input.Delay += owner.GetAttackPoint(target) + owner.GetTurnTime(target.Position)
	const output = raze.Ability.GetPredictionOutput(input)
	if (output.HitChance < HitChanceX.Low)
		return false
	return true
}

const CycloneCombo = (owner: UnitX, ahelper: AbilityHelper, menu: ModeCombo): boolean => {
	if (ahelper === undefined)
		return false

	const target = TargetManager.Target!

	if (!ahelper.CanBeCasted(data.requiem, false, false))
		return false

	const timeFreak = 0.3
	const position = target.Position
	const distance = owner.Distance(position)
	const requiredTime = (data.requiem?.Ability.CastPoint ?? 0) + 0.06

	if (target.IsInvulnerable) {
		const time = target.GetInvulnerabilityDuration //- ping
		if (time <= 0)
			return true
		const remainingTime = time - requiredTime
		if (remainingTime <= -timeFreak)
			return false
		if (distance < 100) {
			if (ahelper.UseAbility(data.bkb))
				return true
			if (ahelper.UseAbility(data.horn))
				return true
			if (remainingTime <= 0 && ahelper.ForceUseAbility(data.requiem)) {
				lastMove = false
				return true
			}
			if (!lastMove && !OrbWalker.Sleeper.Sleeping(owner.Handle)) {
				OrbWalker.Sleeper.Sleep(0.1, owner.Handle)
				if (ahelper.UseAbility(data.legs)) {
					owner.Move(position)
					lastMove = true
				} else {
					owner.Move(position)
					lastMove = true
				}
			}
			return true
		}

		if ((distance / owner.IdealSpeed) < (remainingTime + timeFreak)) {
			if (ahelper.UseAbility(data.bkb))
				return true
			if (ahelper.UseAbility(data.horn))
				return true
			OrbWalker.Sleeper.Sleep(0.1, owner.Handle)
			HERO_DATA.ComboSleeper.Sleep(0.1, owner.Handle)
			return owner.Move(position)
		}

		if (ahelper.CanBeCasted(data.blink)) {
			const blinkRange = (data.blink?.Ability.CastRange ?? 0) + (owner.IdealSpeed * remainingTime)
			if (blinkRange > distance) {
				if (ahelper.UseAbility(data.blink, position)) {
					OrbWalker.Sleeper.Sleep(0.1, owner.Handle)
					lastMove = false
					return true
				}
			}
		}
	}

	if (!ahelper.CanBeCasted(data.euls, false, false) || !data.euls?.ShouldForceCast() || target.IsMagicImmune)
		return false

	const eulsTime = data.euls.Ability.Duration - requiredTime

	if (ahelper.CanBeCasted(data.blink)) {
		const blinkRange = (data.blink?.Ability.CastRange ?? 0) + (owner.IdealSpeed * eulsTime)
		if (blinkRange > distance) {
			if (ahelper.UseAbility(data.blink, position)) {

				if (owner.CanBeUsePause
					&& (APCHeroes.IsEnabled(target.Name) || APHHeroes.IsEnabled(target.Name))
					&& ahelper.CanBeCasted(data.requiem, false, false))
					GameState.ExecuteCommand("dota_pause")

				OrbWalker.Sleeper.Sleep(0.1, owner.Handle)
				HERO_DATA.ComboSleeper.ExtendSleep(0.1, owner.Handle)
				return true
			}
		}
	}

	if (!menu.Move.value && owner.CanMove() && !target.IsInvulnerable)
		owner.Move(position)

	if ((distance / owner.IdealSpeed) < eulsTime) {
		if (ahelper.UseAbility(data.hex)) {
			HERO_DATA.ComboSleeper.ExtendSleep(0.1, owner.Handle)
			OrbWalker.Sleeper.ExtendSleep(0.1, owner.Handle)
			return true
		}

		if (ahelper.UseAbility(data.veil))
			return true

		if (ahelper.UseAbility(data.ethereal)) {
			OrbWalker.Sleeper.Sleep(0.5, owner.Handle)
			return true
		}

		if (ahelper.ForceUseAbility(data.euls)) {
			if (owner.CanBeUsePause && (APCHeroes.IsEnabled(target.Name) || APHHeroes.IsEnabled(target.Name))
				&& ahelper.CanBeCasted(data.requiem, false, false))
				GameState.ExecuteCommand("dota_pause")
			return true
		}
	}

	return false
}

export const SFAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof MaskOfMadness)
		data.mom = new AIOBuff(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIOAbyssalBlade(abil)
	if (abil instanceof SpiderLegs)
		data.legs = new AIOSpeedBuff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof RequiemOfSouls)
		data.requiem = new AIONuke(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new HexShadowFiend(abil)
	if (abil instanceof EulsScepterOfDivinity || abil instanceof WindWaker)
		data.euls = new AIOEulsScepter(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof MinotaurHorn)
		data.horn = new AIOShield(abil)
	if (abil instanceof RefresherOrb)
		data.refresher = new AIOUntargetable(abil)
	if (abil instanceof RefresherShard)
		data.refresher1 = new AIOUntargetable(abil)
	if (abil instanceof Shadowraze && !data.razes.some(x => x.Ability.Handle === abil.Handle))
		data.razes.push(new AIONuke(abil))
}

export const SFMode = (unit: UnitX, menu: ModeCombo): boolean => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	if (helper === undefined)
		helper = new AbilityHelper(unit, menu)

	const target = TargetManager.Target!
	razeOrbwalk = false

	if (CycloneCombo(unit, helper, menu))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.ethereal))
		return true

	if (!helper.CanBeCasted(data.requiem, false, false)) {
		if (helper.UseAbility(data.refresher1))
			return true
		if (data.refresher1 === undefined || data.refresher1.Ability.RemainingCooldown > 0)
			if (!helper.CanBeCasted(data.requiem, false, false) && helper.UseAbility(data.refresher))
				return true
	}

	if (!helper.CanBeCasted(data.euls, false, false)) {
		if (helper.UseAbility(data.abyssal))
			return true
		if (helper.UseAbility(data.mom))
			return true
	}

	if (helper.CanBeCasted(data.requiem, false, false)
		&& helper.CanBeCasted(data.euls, false, false))
		return false

	const orderedRazes = target.GetAngle(unit.Position) > 1 || !target.IsMoving
		? ArrayExtensions.orderBy(data.razes, x => x.Ability.BaseAbility.ID)
		: ArrayExtensions.orderByRevert(data.razes, x => x.Ability.BaseAbility.ID)

	for (const raze of orderedRazes) {
		if (!helper.CanBeCasted(raze))
			continue
		if (RazeCanWaitAttack(raze, unit, target))
			continue
		if (helper.UseAbility(raze))
			return true
	}

	razeOrbwalk = true
	return false
}

export class NevermoreOrbWalker extends BaseOrbwalk {

	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (target !== undefined && target.HasBuffByName("modifier_nevermore_requiem_fear"))
			move = false

		if (razeOrbwalk
			&& helper !== undefined
			&& target !== undefined
			&& this.Owner.IdealSpeed >= 305
			&& (target.IsRanged || target.GetImmobilityDuration > 1)
		) {
			const distance = this.Owner.Distance(target)
			const razeFilter = ArrayExtensions.orderBy(data.razes, x => Math.abs(x.Ability.CastRange - distance))
			for (const raze of razeFilter) {
				if (!helper.CanBeCasted(raze, false))
					continue
				const position = target.Position.Extend2D(this.Owner.Position, raze.Ability.CastRange - (raze.Ability.Radius - 100))
				const distance2 = this.Owner.Distance(position)
				if (target.GetAttackRange(this.Owner) >= distance2 || target.GetImmobilityDuration > 1) {
					if (distance2 > 50)
						return this.Move(position)
				}
			}
		}
		return super.OrbwalkSet(target, attack, move)
	}
}

function DataUnload() {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			if (key === "razes") {
				// @ts-ignore
				data[key] = []
				continue
			}
			// @ts-ignore
			data[key] = undefined
		}
	}
	lastMove = false
	helper = undefined
	razeOrbwalk = false
}

EventsX.on("AbilityDestroyed", abil => {
	if (abil instanceof RefresherShard)
		data.refresher1 = undefined
})

EventsX.on("GameEnded", () => DataUnload())
