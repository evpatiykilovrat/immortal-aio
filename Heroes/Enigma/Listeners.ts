import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { EAbilitiesCreated, EnigmaMode } from "./Enigma";
import { EnigmaMenu } from "./menu";

HeroService.RegisterHeroModule(EnigmaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(EnigmaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(EnigmaMenu)

function onTick(unit: UnitX) {
	if (!EnigmaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = EnigmaMenu.TargetMenuTree
	TargetManager.TargetLocked = EnigmaMenu.ComboKey.is_pressed || EnigmaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		EnigmaMode,
		EnigmaMenu.ComboMode,
		EnigmaMenu,
		EnigmaMenu.ComboKey,
	)

	ComboMode(
		unit,
		EnigmaMode,
		EnigmaMenu.HarrasMode,
		EnigmaMenu,
		EnigmaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!EnigmaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		EnigmaMenu.TargetMenuTree.DrawTargetParticleColor,
		EnigmaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(EnigmaMenu.ComboMode, owner)
	SwitchPanel.Draw(EnigmaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!EnigmaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (EnigmaMenu.ComboKey.is_pressed || EnigmaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!EnigmaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((EnigmaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!EnigmaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(EnigmaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(EnigmaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (EnigmaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (EnigmaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, EnigmaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, EnigmaMenu))
}

function onDestoyed(owner: UnitX) {
	if (EnigmaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== EnigmaMenu.npc_name)
		return
	EAbilitiesCreated(abil, EnigmaMenu)
}

EventsX.on("removeControllable", owner => {
	if (EnigmaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
