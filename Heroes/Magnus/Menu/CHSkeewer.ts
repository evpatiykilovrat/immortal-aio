import { EventsX, GameX, HeroX, PathX } from "immortal-core/Imports"
import { LocalPlayer, Menu } from "wrapper/Imports"
import { MagnusMenu } from "./Base"

export interface SkewerMode {
	TowerState: boolean;
	Heroes: Menu.ImageSelector
}

const SkeewerComboTree = MagnusMenu.ComboSettings.AddNode("Skewer", PathX.DOTAAbilities("magnataur_skewer"))
const SkewerStateCombo = SkeewerComboTree.AddToggle("SKEWER_TOWER", true, "Skewer to tower if no allies")
const SkewerSelectorCombo = SkeewerComboTree.AddImageSelector("SKEWER_TO_ALLY", [])

const SkeewerHarassTree = MagnusMenu.HarassSettings.AddNode("Skewer", PathX.DOTAAbilities("magnataur_skewer"))
const SkewerHarassState = SkeewerHarassTree.AddToggle("SKEWER_TOWER", true, "Skewer to tower if no allies")
const SkewerSelectorHarass = SkeewerHarassTree.AddImageSelector("SKEWER_TO_ALLY", [])

export const SkewerComboMode: SkewerMode = {
	TowerState: SkewerStateCombo.value,
	Heroes: SkewerSelectorCombo,
}

export const SkewerHarassMode: SkewerMode = {
	TowerState: SkewerHarassState.value,
	Heroes: SkewerSelectorHarass,
}

EventsX.on("UnitCreated", async hero => {
	if (!(hero instanceof HeroX) || hero.IsEnemy() || !GameX.IsInGame || LocalPlayer?.Hero?.Index === hero.Handle)
		return

	if (!SkewerSelectorHarass.values.includes(hero.Name)) {
		SkewerSelectorHarass.values.push(hero.Name)
		await SkewerSelectorHarass.Update()
	}
	if (!SkewerSelectorCombo.values.includes(hero.Name)) {
		SkewerSelectorCombo.values.push(hero.Name)
		await SkewerSelectorCombo.Update()
	}
})

EventsX.on("GameEnded", () => {
	SkewerSelectorCombo.values = []
	SkewerSelectorHarass.values = []
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["SKEWER_TO_ALLY", ""],
	["SKEWER_TOWER", "Использовать к вышке"],
	["Skewer to tower if no allies", "Использовать к вышке, если нет союзников"],
]))

Menu.Localization.AddLocalizationUnit("english", new Map([
	["SKEWER_TO_ALLY", "Use to tower"],
	["SKEWER_TOWER", "Tower"],
]))
