import { ActiveAbility } from "immortal-core/Imports";
import { AIODebuff, TargetManager } from "_ICore_/Index";

export class VoidTimeDilation extends AIODebuff {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (TargetManager.Target!.Abilities.filter(x => x.RemainingCooldown > 0).length >= 2)
			return true

		const allAbilities = TargetManager.EnemyHeroes
			.map(x => x.Abilities)
			.reduce((prev, curr) => [...prev, ...curr], [])
			.filter(x => x.RemainingCooldown > 0).length

		if (allAbilities >= 5)
			return true

		return false
	}
}
