import { ActiveAbility, Shackleshot, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, AIOUsable, TargetManager } from "_ICore_/Index";
import { WindrangerShackleshot } from "./Shackleshot";

export class BlinkDaggerWindranger extends AIOBlink {

	private readonly blinkPosition = new Vector3()

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const abil = args.find(x => x.Ability instanceof Shackleshot) as WindrangerShackleshot
		if (abil === undefined)
			return false
		this.blinkPosition.CopyFrom(abil.GetBlinkPosition(target, this.Ability.CastRange))
		return !this.blinkPosition.IsZero()
	}

	public UseAbility(aoe: boolean) {

		if (this.Owner.Distance(this.blinkPosition) < 400)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: this.blinkPosition }))
			return false

		this.Ability.ActionSleeper.Sleep(this.Ability.GetCastDelay(TargetManager.Target!))
		return true
	}
}
