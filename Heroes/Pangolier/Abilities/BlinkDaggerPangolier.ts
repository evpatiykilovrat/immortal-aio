import { ActiveAbility, OrbWalker } from "immortal-core/Imports";
import { AIOBlink, HERO_DATA, TargetManager } from "_ICore_/Index";

export class BlinkDaggerPangolier extends AIOBlink {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public UseAbility(aoe: boolean) {
		if (!this.Owner.HasBuffByName("modifier_pangolier_gyroshell"))
			return false

		const target = TargetManager.Target!
		const distance = this.Owner.Distance(target)
		if (distance < 300)
			return false

		if (target.HasBuffByName("modifier_pangolier_gyroshell_timeout")
			|| (this.Owner.GetAngle(target.Position) < 1.25 && distance < 800))
			return false

		const position = target.PredictedPosition(0.1)
		if (this.Ability.CastRange < this.Owner.Distance(position))
			return false

		if (!this.Ability.UseAbility(target, position))
			return false

		const delay = this.Ability.GetCastDelay(position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
