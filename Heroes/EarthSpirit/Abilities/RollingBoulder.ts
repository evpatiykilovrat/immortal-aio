import { ActiveAbility, EarthSpiritStone, HitChanceX, OrbWalker, UnitsX, Vector3Extend } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIONuke, TargetManager } from "_ICore_/Index";

export class SpiritRollingBoulder extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public CanHit() {
		return true
	}

	public SimpleUseAbility(position: Vector3) {
		if (!this.Owner.UseAbility(this.Ability, { intent: position }))
			return false
		const delay = this.Ability.GetCastDelay(position)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ForceUseAbility() {
		const target = TargetManager.Target!
		const distance = this.Owner.Distance(target)
		const input = this.Ability.GetPredictionInput(target)
		if (UnitsX.All.some(x => x instanceof EarthSpiritStone
			&& x.Distance(this.Owner) < distance && x.Distance(this.Owner) < 350
			&& x.IsAlive
			&& Vector3Extend.AngleBetween(this.Owner.Position, x.Position, target.Position) < 75)
		) {
			input.Speed *= 2
			input.CastRange *= 2
			input.Range *= 2
		}

		const output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const distance = this.Owner.Distance(target)
		const input = this.Ability.GetPredictionInput(target)

		if (UnitsX.All.some(x => x instanceof EarthSpiritStone
			&& x.Distance(this.Owner) < distance
			&& x.Distance(this.Owner) < 350
			&& x.IsAlive && Vector3Extend.AngleBetween(this.Owner.Position, x.Position, target.Position) < 75)
		) {
			input.Speed *= 2
			input.CastRange *= 2
			input.Range *= 2
		} else if (distance > 350) {
			return false
		}

		const output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
