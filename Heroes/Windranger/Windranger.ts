import { AbilityX, BlinkDagger, Bloodthorn, DiffusalBlade, EventsX, FocusFire, Nullifier, OrbWalker, OrchidMalevolence, Powershot, ScytheOfVyse, Shackleshot, UnitX, Windranger, Windrun } from "immortal-core/Imports";
import { Ability, dotaunitorder_t, EventsSDK, Input } from "wrapper/Imports";
import { AbilityHelper, AIOBloodthorn, AIODebuff, AIODisable, AIONullifier, BaseOrbwalk, HERO_DATA, OrbWallker, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { BlinkDaggerWindranger } from "./Abilities/BlinkDaggerWindranger";
import { WindrangerFocusFire } from "./Abilities/FocusFire";
import { WindrangerPowershot } from "./Abilities/Powershot";
import { WindrangerShackleshot } from "./Abilities/Shackleshot";
import { WindrangerWindrun } from "./Abilities/Windrun";
import { WindrangerMenu } from "./Menu/Base";
import { ShackleMove } from "./Menu/Move";

const data = {
	diffusal: undefined as Nullable<AIODebuff>,
	hex: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	nullifier: undefined as Nullable<AIONullifier>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	blink: undefined as Nullable<BlinkDaggerWindranger>,
	windrun: undefined as Nullable<WindrangerWindrun>,
	focusFire: undefined as Nullable<WindrangerFocusFire>,
	shackleshot: undefined as Nullable<WindrangerShackleshot>,
	powershot: undefined as Nullable<WindrangerPowershot>,
}

export const WAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerWindranger(abil)
	if (abil instanceof Shackleshot)
		data.shackleshot = new WindrangerShackleshot(abil, WindrangerMenu, ShackleMove)
	if (abil instanceof Windrun)
		data.windrun = new WindrangerWindrun(abil)
	if (abil instanceof FocusFire)
		data.focusFire = new WindrangerFocusFire(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof Powershot) {
		data.powershot = new WindrangerPowershot(abil)
		data.powershot.AddShake(data.shackleshot)
	}
}

export const WindrangerMode = (unit: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const target = TargetManager.Target!
	const helper = new AbilityHelper(unit, menu)

	if (data.powershot?.CancelChanneling(target)) {
		HERO_DATA.ComboSleeper.Sleep(0.1, unit.Handle)
		return true
	}
	if (helper.UseAbilityIfCondition(data.blink, data.shackleshot)) {
		helper.ForceUseAbility(data.shackleshot)
		OrbWalker.Sleeper.Sleep(0.5, unit.Handle)
		return true
	}
	if (!helper.CanBeCasted(data.shackleshot, false, false))
		if (helper.UseAbility(data.blink, unit.GetAttackRange(target) + 100, 400))
			return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.shackleshot))
		return true

	if (helper.CanBeCasted(data.shackleshot, false)) {
		const castWindrun = helper.CanBeCasted(data.windrun, false, false)
		const movePosition = data.shackleshot?.GetMovePosition(castWindrun)!
		if (!movePosition.IsZero() && OrbWallker.get(unit.Handle)?.Move(movePosition)) {
			if ((target.Distance(movePosition) > 100 || target.IsMoving) && castWindrun)
				helper.ForceUseAbility(data.windrun)
			OrbWalker.Sleeper.Sleep(0.1, unit.Handle)
			HERO_DATA.ComboSleeper.Sleep(0.1, unit.Handle)
			return true
		}
	}

	if (helper.UseAbilityIfCondition(data.powershot, data.shackleshot, data.blink)) {
		HERO_DATA.ComboSleeper.ExtendSleep(0.2, unit.Handle)
		OrbWalker.Sleeper.ExtendSleep(0.2, unit.Handle)
		return true
	}

	if (helper.UseAbilityIfCondition(data.focusFire, data.powershot))
		return true

	if (helper.UseAbility(data.windrun))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	return false
}

export class WindrangerOrbWalk extends BaseOrbwalk  {
	private get DirectionalMove() {
		if (OrbWalker.Sleeper.Sleeping(this.Owner.Handle))
			return false
		if (this.CanMove()) {
			OrbWalker.Sleeper.Sleep(0.13, this.Owner.Handle)
			return this.Owner.MoveToDirection(Input.CursorOnWorld)
		}
		return false
	}
	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (!(this.Owner instanceof Windranger))
			return super.OrbwalkSet(target, attack, move)
		let focusFireTarget = this.Owner.FocusFireActive === true
			&& this.Owner.FocusFireTarget?.Equals(target) === true
		if (focusFireTarget && target.IsReflectingDamage)
			return this.DirectionalMove
		if (focusFireTarget && this.Owner.HasBuffByName("modifier_windrunner_windrun_invis"))
			focusFireTarget = false
		return super.OrbwalkSet(target, !focusFireTarget, move)
	}
}

EventsX.on("OnExecuteOrder", order => {
	if (order.orderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION || order.position === undefined)
		return
	if (order.ability?.Handle === data.powershot?.Ability.Handle)
		data.powershot?.castPosition.CopyFrom(order.position)
})

EventsSDK.on("PrepareUnitOrders", order => {
	if (order.Queue)
		return
	const abil = order.Ability
	if (!(abil instanceof Ability) || order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION)
		return
	if (abil.Index === data.powershot?.Ability.Handle)
		data.powershot?.castPosition.CopyFrom(order.Position)
})

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
