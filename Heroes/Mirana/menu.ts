import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const MiranaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_mirana",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_diffusal_blade",
		"item_rod_of_atos",
		"item_gungir",
		"item_diffusal_blade",
		"item_urn_of_shadows",
		"item_veil_of_discord",
		"item_spirit_vessel",
		"item_sheepstick",
		"item_nullifier",
		"item_orchid",
		"item_bloodthorn",
		"item_cyclone",
		"item_dagon_5",
		"item_ethereal_blade",
	],
	Abilities: [
		"mirana_starfall",
		"mirana_arrow",
		"mirana_leap",
	],
})
