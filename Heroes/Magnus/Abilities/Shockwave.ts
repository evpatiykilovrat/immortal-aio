import { ActiveAbility, BlinkDagger, ReversePolarity, Skewer, UnitX } from "immortal-core/Imports";
import { AIONuke, AIOUsable, TargetManager } from "_ICore_/Index";

export class AIOShockwave extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldConditionCast(target: UnitX, abilities: AIOUsable[]) {
		if (this.Owner.Distance(target) < 500)
			return true

		const skewer = abilities.find(x => x.Ability instanceof Skewer)
		if (skewer !== undefined) {
			const polarity = abilities.find(x => x.Ability instanceof ReversePolarity)
			if (polarity !== undefined)
				return false
		}

		const blink = abilities.find(x => x instanceof BlinkDagger)
		const damage = this.Ability.GetDamage(TargetManager.Target!)
		if (blink === undefined || damage > target.Health)
			return true

		if (this.Owner.Distance(target) > 800)
			return false

		return true
	}
}
