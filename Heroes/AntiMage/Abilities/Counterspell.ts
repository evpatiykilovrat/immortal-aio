import { AbilitiesX, ActiveAbility } from "immortal-core/Imports";
import { AIOShield } from "_ICore_/Index";

export class AntiMageCounterspell extends AIOShield {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false
		return AbilitiesX.All.some(x => x instanceof ActiveAbility
			&& x.UnitTargetCast
			&& x.CastPoint <= 0.1 && x.Owner.IsEnemy()
			&& x.CanHit(this.Owner) && x.CanBeCasted()
			&& x.Owner.GetAngle(this.Owner.BaseOwner) <= 2.5,
		)
	}
}
