import { ActiveAbility, Eclipse, HitChanceX, OrbWalker } from "immortal-core/Imports";
import { ArrayExtensions } from "wrapper/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class LunaElipse extends AIONuke {

	constructor(public Ability: ActiveAbility & Eclipse) {
		super(Ability)

	}
	public CanHit() {
		if (!this.Owner.HasAghanimsScepter)
			return super.CanHit()
		return this.Ability.CanHit(TargetManager.Target!, TargetManager.IsValidEnemyHeroes, 1)
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (!this.Owner.HasAghanimsScepter)
			return super.UseAbility(aoe)
		const target = TargetManager.Target!
		const ally = ArrayExtensions.orderBy(TargetManager.IsValidAllyHeroes.filter(x => !x.Equals(this.Owner)
			&& x.Distance(target) <= this.Ability.Radius), x => x.Distance(target))[0]
		if (ally === undefined) {
			const input = this.Ability.GetPredictionInput(TargetManager.Target!, TargetManager.IsValidEnemyHeroes)
			const output = this.Ability.GetPredictionOutput(input)
			if (output.HitChance < HitChanceX.Low)
				return false
			if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
				return false
			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}
		if (!this.Owner.UseAbility(this.Ability, { intent: ally.Position }))
			return false
		const delays = this.Ability.GetCastDelay(ally)
		HERO_DATA.ComboSleeper.Sleep(delays, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delays + 0.5)
		OrbWalker.Sleeper.Sleep(delays, this.Owner.Handle)
		return true
	}
}
