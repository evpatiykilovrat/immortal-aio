import { Ability, Input, Unit } from "wrapper/Imports"

export const RazeDirection = (abil: Ability, owner: Unit) => {
	const pos = Input.CursorOnWorld
	const rotation = owner.FindRotationAngle(pos)
	if (rotation <= 0.15)
		return true
	owner.MoveToDirection(pos)
	owner.CastNoTarget(abil)
	return false
}
