import { item_aeon_disk } from "wrapper/Imports";
import { TargetManager } from "../../Manager/Target";
import { AIODebuff } from "../Debuff";

export class AIONullifier extends AIODebuff {

	public ShouldCast() {
		const target = TargetManager.Target!
		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (this.Ability.BreaksLinkens && target.IsBlockingAbilities)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.HasBuffByName("modifier_eul_cyclone"))
			return true

		if (target.IsInvulnerable) {
			if (this.AIODebuff.UnitTargetCast)
				return false
			if (!this.ChainStun(target, true))
				return false
		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		if (target.Abilities.some(x => x.BaseAbility instanceof item_aeon_disk && x.IsReady))
			return false

		return true
	}
}
