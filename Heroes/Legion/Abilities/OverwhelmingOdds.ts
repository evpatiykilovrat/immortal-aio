import { ActiveAbility, HitChanceX, OrbWalker } from "immortal-core/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class LegionOverwhelmingOdds extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public UseAbility(aoe: boolean) {
		if (aoe) {

			const input = this.Ability.GetPredictionInput(TargetManager.Target!, TargetManager.IsValidEnemyUnits.filter(x => !x.IsMagicImmune))
			const output = this.Ability.GetPredictionOutput(input)

			if (output.HitChance < HitChanceX.Low)
				return false

			if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
				return false

		} else {
			if (!this.Ability.UseAbility(TargetManager.Target!, HitChanceX.Low))
				return false
		}

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
