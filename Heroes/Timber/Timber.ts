import { AbilityX, BladeMail, BlinkDagger, Chakram, EventsX, FlametHrower, Gleipnir, LotusOrb, RodOfAtos, ScytheOfVyse, ShivasGuard, TimberChain, UnitX, WhirlingDeath } from "immortal-core/Imports";
import { EventsSDK, Hero, LifeState_t } from "wrapper/Imports";
import { HERO_DATA } from "_ICore_/data";
import { AbilityHelper, AIODebuff, AIODisable, AIONuke, AIOShield, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { BlinkDaggerTimbersaw } from "./Abilities/BlinkDaggerTimbersaw";
import { TimberChakram } from "./Abilities/Chakram";
import { AIOTimberChain } from "./Abilities/TimberChain";
import { TimberMenu } from "./menu";

const data = {
	chakrams: [] as TimberChakram[],
	atos: undefined as Nullable<AIODisable>,
	lotus: undefined as Nullable<AIOShield>,
	flamet: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	shiva: undefined as Nullable<AIODebuff>,
	bladeMail: undefined as Nullable<AIOShield>,
	whirlingDeath: undefined as Nullable<AIONuke>,
	timberChain: undefined as Nullable<AIOTimberChain>,
	blink: undefined as Nullable<BlinkDaggerTimbersaw>,
}

TimberMenu.HarassKey.OnRelease(() => {
	data.chakrams.forEach(abil => abil.Return())
})

TimberMenu.ComboKey.OnRelease(() => {
	data.chakrams.forEach(abil => abil.Return())
})

export const TAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof LotusOrb)
		data.lotus = new AIOShield(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof WhirlingDeath)
		data.whirlingDeath = new AIONuke(abil)
	if (abil instanceof FlametHrower)
		data.flamet = new AIONuke(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerTimbersaw(abil)
	if (abil instanceof TimberChain)
		data.timberChain = new AIOTimberChain(abil)
	if (abil instanceof Chakram && !data.chakrams.some(x => x.Ability.Handle === abil.Handle))
		data.chakrams.push(new TimberChakram(abil))
}

export const TimberMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const damagingChakrams = data.chakrams.filter(x => menu.Abilities.IsEnabled(x.Ability.Name) && x.IsDamaging()).length
	const returnChakram = data.chakrams.find(x => menu.Abilities.IsEnabled(x.Ability.Name) && x.ShouldReturnChakram(damagingChakrams))

	if (returnChakram?.Return())
		return true

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.whirlingDeath))
		return true

	if (helper.UseAbility(data.flamet))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.lotus, 1200))
		return true

	if (helper.UseAbilityIfCondition(data.blink, data.timberChain, data.whirlingDeath)) {
		if (helper.CanBeCasted(data.whirlingDeath, false, false)) {
			helper.ForceUseAbility(data.whirlingDeath, true)
		}
		return true
	}

	if (helper.UseAbility(data.shiva))
		return true

	if (helper.UseAbility(data.bladeMail, 400))
		return true

	if (helper.UseAbilityIfCondition(data.timberChain, data.blink))
		return true

	const chakram = data.chakrams.find(x => x.Ability.CanBeCasted())

	if (helper.UseAbility(chakram))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			if (key === "chakrams") {
				// @ts-ignore
				data[key] = []
				continue
			}
			// @ts-ignore
			data[key] = undefined
		}
	}
})

EventsSDK.on("LifeStateChanged", unit => {
	if (!TimberMenu.BaseState.value)
		return
	if (!(unit instanceof Hero)
		|| unit.LifeState !== LifeState_t.LIFE_DEAD
		|| unit.Index !== TargetManager.Target?.Handle)
		return
	data.chakrams.forEach(abil => {
		const findAOE = TargetManager.EnemyHeroes.find(x => x.Handle !== unit.Index
			&& x.Position.Distance(abil.СastPosition) <= abil.Ability.Radius)
		if (findAOE !== undefined)
			return
		abil.Return()
	})
})
