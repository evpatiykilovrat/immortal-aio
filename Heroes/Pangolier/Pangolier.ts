import { AbilityX, AbyssalBlade, BlinkDagger, DiffusalBlade, EventsX, Mjollnir, OrbWalker, RollingThunder, ScytheOfVyse, ShieldCrash, Swashbuckle, TickSleeperX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, pangolier_gyroshell, Vector3 } from "wrapper/Imports";
import { HERO_DATA } from "_ICore_/data";
import { AbilityHelper, AIODebuff, AIODisable, AIOShield, BaseOrbwalk, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { BlinkDaggerPangolier } from "./Abilities/BlinkDaggerPangolier";
import { PangoRollingThunder } from "./Abilities/RollingThunder";
import { PangoShieldCrash } from "./Abilities/ShieldCrash";
import { PangoSwashbuckle } from "./Abilities/Swashbuckle";

const ultSleeper = new TickSleeperX()

const data = {
	hex: undefined as Nullable<AIODisable>,
	crash: undefined as Nullable<PangoShieldCrash>,
	swashbuckle: undefined as Nullable<PangoSwashbuckle>,
	diffusal: undefined as Nullable<AIODebuff>,
	mjollnir: undefined as Nullable<AIOShield>,
	blink: undefined as Nullable<BlinkDaggerPangolier>,
	abyssal: undefined as Nullable<AIODisable>,
	thunder: undefined as Nullable<PangoRollingThunder>,
}

export const PAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof ShieldCrash)
		data.crash = new PangoShieldCrash(abil)
	if (abil instanceof Swashbuckle)
		data.swashbuckle = new PangoSwashbuckle(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof Mjollnir)
		data.mjollnir = new AIOShield(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerPangolier(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof RollingThunder)
		data.thunder = new PangoRollingThunder(abil)
}

export const PangoMode = (unit: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || ultSleeper.Sleeping || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.swashbuckle))
		return true

	if (helper.UseAbility(data.mjollnir, 600))
		return true

	if (helper.UseAbility(data.crash))
		return true

	if (helper.UseAbilityIfCondition(data.thunder, data.blink)) {
		HERO_DATA.ComboSleeper.Sleep(((data.thunder?.Ability.CastPoint ?? 0) + 0.1), unit.Handle)
		return true
	}

	if (helper.UseAbility(data.blink))
		return true

	return false
}

export const PangoExecuteOrderer = (order: ExecuteOrder) => {
	const abil = order.Ability
	if (!(abil instanceof pangolier_gyroshell)
		|| order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
		|| data.thunder === undefined)
		return
	ultSleeper.Sleep(data.thunder.Ability.CastDelay + 0.15)
}

export class PangoOrbWalker extends BaseOrbwalk {
	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (OrbWalker.Sleeper.Sleeping(this.Owner.Handle))
			return false
		if (this.Owner.HasBuffByName("modifier_pangolier_gyroshell") && target !== undefined) {
			if (this.Owner.GetAngle(target.Position) > 1.5) {
				const position = data.thunder?.GetPosition ?? new Vector3()
				if (!position.IsZero() && this.Owner.Move(position)) {
					OrbWalker.Sleeper.Sleep(1, this.Owner.Handle)
					return true
				}
			}
			if (this.Owner.Move(target.Position)) {
				OrbWalker.Sleeper.Sleep(0.5, this.Owner.Handle)
				return true
			}
		}
		return super.OrbwalkSet(target, attack, move)
	}
}

EventsX.on("GameEnded", () => {
	ultSleeper.ResetTimer()
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
