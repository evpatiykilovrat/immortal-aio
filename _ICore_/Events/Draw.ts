import { GameX } from "immortal-core/Imports"
import { DOTAGameUIState_t, EventsSDK, GameState } from "wrapper/Imports"
import { HERO_DATA } from "../data"
import { TargetManager } from "../Manager/Target"

EventsSDK.on("Draw", () => {
	if (!GameX.IsInGame || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME)
		return
	TargetManager.AllyHeroes.forEach(unit => {
		if (!unit.IsControllable || unit.IsEnemy())
			return
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return
		hero_module.onDraw(unit)
	})
})
