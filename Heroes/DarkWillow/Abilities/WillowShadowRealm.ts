import { ActiveAbility, ShadowRealm, UnitX } from "immortal-core/Imports";
import { AIONuke, TargetManager } from "_ICore_/Index";

export class WillowShadowRealm extends AIONuke {
	constructor(public Ability: ActiveAbility & ShadowRealm) {
		super(Ability)
	}

	public get Casted() {
		return this.Ability.Casted || this.Ability.DamageMaxed
	}

	public CanHit() {
		if (this.Owner.Distance(TargetManager.Target!.Owner) > 900)
			return false
		// ...
		return true
	}

	public ShouldAttack(target: UnitX) {
		if (this.Owner.IsReflectingDamage && this.Owner.HealthPercentage < 50)
			return false

		if (this.Owner.IsMagicImmune && !this.Ability.PiercesMagicImmunity(target))
			return false

		const damage = this.Ability.GetDamage(target)
		if (damage > target.Health)
			return true

		if (this.Owner.Distance(target) < 700 || target.IsStunned || target.IsRooted || target.IsHexed)
			return this.Ability.DamageMaxed

		if (this.Ability.RealmTime < 1)
			return false

		return true
	}

	public ShouldCast() {
		const target = TargetManager.Target!

		if (!target.IsVisible)
			return false

		return true
	}
}
