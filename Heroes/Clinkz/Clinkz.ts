import { AbilityX, BlinkDagger, Bloodthorn, BurningArmy, DiffusalBlade, EventsX, Gleipnir, MedallionOfCourage, Nullifier, OrchidMalevolence, RodOfAtos, ScytheOfVyse, SolarCrest, Strafe, UnitX } from "immortal-core/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIONullifier, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";

const data = {
	blink: undefined as Nullable<AIOBlink>,
	hex: undefined as Nullable<AIODisable>,
	atos: undefined as Nullable<AIODisable>,
	medal: undefined as Nullable<AIODebuff>,
	strafe: undefined as Nullable<AIOBuff>,
	army: undefined as Nullable<BurningArmy>,
	orchid: undefined as Nullable<AIODisable>,
	diffusal: undefined as Nullable<AIODebuff>,
	nullifier: undefined as Nullable<AIONullifier>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}

export const CAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof BurningArmy)
		data.army = abil
	if (abil instanceof Strafe)
		data.strafe = new AIOBuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof MedallionOfCourage || abil instanceof SolarCrest)
		data.medal = new AIODebuff(abil)
}

export const ClinkzMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const target = TargetManager.Target!
	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 600, 450))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.medal))
		return true

	if (helper.UseAbility(data.strafe, unit.AttackRange(TargetManager.Target!.BaseOwner)))
		return true

	if (data.army?.CanBeCasted()) {
		const targets = TargetManager.EnemyHeroes
		if (data.army?.UseAbility(target, targets))
			return true
	}

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
