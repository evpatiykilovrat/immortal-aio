import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { UrsaMenu } from "./menu";
import { UAbilitiesCreated, UrsaMode, UrsaOrbwalk } from "./Ursa";

HeroService.RegisterHeroModule(UrsaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(UrsaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(UrsaMenu)

function onTick(unit: UnitX) {
	if (!UrsaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = UrsaMenu.TargetMenuTree
	TargetManager.TargetLocked = UrsaMenu.ComboKey.is_pressed || UrsaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		UrsaMode,
		UrsaMenu.ComboMode,
		UrsaMenu,
		UrsaMenu.ComboKey,
	)

	ComboMode(
		unit,
		UrsaMode,
		UrsaMenu.HarrasMode,
		UrsaMenu,
		UrsaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!UrsaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		UrsaMenu.TargetMenuTree.DrawTargetParticleColor,
		UrsaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(UrsaMenu.ComboMode, owner)
	SwitchPanel.Draw(UrsaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!UrsaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (UrsaMenu.ComboKey.is_pressed || UrsaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!UrsaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((UrsaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!UrsaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(UrsaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(UrsaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (UrsaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (UrsaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, UrsaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new UrsaOrbwalk(owner, UrsaMenu))
}

function onDestoyed(owner: UnitX) {
	if (UrsaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== UrsaMenu.npc_name)
		return
	UAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (UrsaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
