import { isIDisable } from "immortal-core/Imports";
import { HERO_DATA } from "../../data";
import { TargetManager } from "../../Manager/Target";
import { AIODebuff } from "../Debuff";

export class AIOEtherealBlade extends AIODebuff {
	public UseAbility(aoe: boolean) {
		if (!this.Owner.UseAbility(this.Ability, { intent: TargetManager.Target! }))
			return false

		const hitTime = this.Ability.GetHitTime(TargetManager.Target!)

		if (isIDisable(this.Ability))
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, hitTime)

		HERO_DATA.ComboSleeper.Sleep(hitTime, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}
}
