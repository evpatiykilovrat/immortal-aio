import { AbilityX, BlackKingBar, Bloodthorn, Chronosphere, DiffusalBlade, EventsX, MantaStyle, MaskOfMadness, Mjollnir, OrchidMalevolence, SilverEdge, TimeDilation, TimeWalk, UnitX } from "immortal-core/Imports";
import { HERO_DATA } from "_ICore_/data";
import { AbilityHelper, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIOShield, AIOUsable, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { VoidChronosphere } from "./Abilities/Chronosphere";
import { VoidTimeDilation } from "./Abilities/TimeDilation";
import { VoidTimeWalk } from "./Abilities/TimeWalk";
import { FacelessVoidMenu } from "./menu";

const data = {
	bkb: undefined as Nullable<AIOShield>,
	orchid: undefined as Nullable<AIOUsable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	dilation: undefined as Nullable<VoidTimeDilation>,
	timeWalk: undefined as Nullable<VoidTimeWalk>,
	chronosphere: undefined as Nullable<VoidChronosphere>,
	diffusal: undefined as Nullable<AIODebuff>,
	silver: undefined as Nullable<AIOBuff>,
	mjollnir: undefined as Nullable<AIOShield>,
	manta: undefined as Nullable<AIOBuff>,
	mom: undefined as Nullable<AIOBuff>,
}

export const FAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof SilverEdge)
		data.silver = new AIOBuff(abil)
	if (abil instanceof Mjollnir)
		data.mjollnir = new AIOShield(abil)
	if (abil instanceof MantaStyle)
		data.manta = new AIOBuff(abil)
	if (abil instanceof MaskOfMadness)
		data.mom = new AIOBuff(abil)
	if (abil instanceof TimeWalk)
		data.timeWalk = new VoidTimeWalk(abil)
	if (abil instanceof Chronosphere)
		data.chronosphere = new VoidChronosphere(abil, FacelessVoidMenu)
	if (abil instanceof TimeDilation)
		data.dilation = new VoidTimeDilation(abil)
}

export const FacelessVoidMode = (unit: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.bkb, 500))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (!helper.CanBeCasted(data.timeWalk, false) || unit.Distance(TargetManager.Target!) < 400)
		if (helper.UseAbility(data.chronosphere))
			return true

	if (helper.UseAbilityIfCondition(data.timeWalk, data.chronosphere))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.dilation))
		return true

	if (helper.UseAbility(data.silver))
		return true

	if (helper.UseAbility(data.mjollnir, 600))
		return true

	if (helper.UseAbility(data.manta, 300))
		return true;

	if (!helper.CanBeCasted(data.chronosphere, false, false))
		if (helper.UseAbilityIfNone(data.mom, data.timeWalk))
			return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
