import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { PhantomAssassinMenu } from "./menu";
import { PAbilitiesCreated, PhantomAssassinMode } from "./PhantomAssassin";

HeroService.RegisterHeroModule(PhantomAssassinMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(PhantomAssassinMenu, oldState)
TargetManager.DestroyParticleDrawTarget(PhantomAssassinMenu)

function onTick(unit: UnitX) {
	if (!PhantomAssassinMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = PhantomAssassinMenu.TargetMenuTree
	TargetManager.TargetLocked = PhantomAssassinMenu.ComboKey.is_pressed || PhantomAssassinMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		PhantomAssassinMode,
		PhantomAssassinMenu.ComboMode,
		PhantomAssassinMenu,
		PhantomAssassinMenu.ComboKey,
	)

	ComboMode(
		unit,
		PhantomAssassinMode,
		PhantomAssassinMenu.HarrasMode,
		PhantomAssassinMenu,
		PhantomAssassinMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!PhantomAssassinMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		PhantomAssassinMenu.TargetMenuTree.DrawTargetParticleColor,
		PhantomAssassinMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(PhantomAssassinMenu.ComboMode, owner)
	SwitchPanel.Draw(PhantomAssassinMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!PhantomAssassinMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (PhantomAssassinMenu.ComboKey.is_pressed || PhantomAssassinMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PhantomAssassinMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((PhantomAssassinMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!PhantomAssassinMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(PhantomAssassinMenu.ComboMode)
		SwitchPanel.MouseLeftDown(PhantomAssassinMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (PhantomAssassinMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (PhantomAssassinMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, PhantomAssassinMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, PhantomAssassinMenu))
}

function onDestoyed(owner: UnitX) {
	if (PhantomAssassinMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== PhantomAssassinMenu.npc_name)
		return
	PAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (PhantomAssassinMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
