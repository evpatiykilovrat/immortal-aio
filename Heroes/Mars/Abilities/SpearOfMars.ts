import { ActiveAbility, EntityX, isIDisable, OrbWalker, PRectangle } from "immortal-core/Imports";
import { BitsExtensions, GridNav, GridNavCellFlags, Vector2, Vector3 } from "wrapper/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class MarsSpearOfMars extends AIONuke {

	private castPosition = new Vector3().Invalidate()

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public CanHit() {

		const target = TargetManager.Target!

		if (target.IsMagicImmune && !this.Ability.CanHitSpellImmuneEnemy)
			return false

		if (this.Owner.Distance(target.Owner) > this.Ability.CastRange)
			return false

		if (target.HasBuffByName("modifier_mars_arena_of_blood_leash"))
			return true

		const mainInput = this.Ability.GetPredictionInput(target)
		mainInput.Range = this.Ability.CastRange
		const mainOutput = this.Ability.PredictionManager.GetSimplePrediction(mainInput)
		const targetPredictedPosition = mainOutput.TargetPosition
		const range = this.Ability.Range - 200
		const width = this.Ability.Radius

		const collisionRec = new PRectangle(this.Owner.Position, targetPredictedPosition, width);
		if (TargetManager.AllEnemyHeroes.some(x => !x.Equals(target) && collisionRec.IsInside(x.Position)))
			return false

		for (const tree of EntityX.Tree.filter(x => x.IsAlive)) {
			const rec = new PRectangle(
				targetPredictedPosition,
				this.Owner.Position.Extend2D(targetPredictedPosition, range),
				width - 50)
			if (rec.IsInside(tree.Position)) {
				this.castPosition = mainOutput.CastPosition
				return true
			}
		}

		for (const building of EntityX.Buildings.filter(x => x.IsAlive)) {
			const rec = new PRectangle(
				targetPredictedPosition,
				this.Owner.Position.Extend2D(targetPredictedPosition, range),
				width)
			if (rec.IsInside(building.Position)) {
				this.castPosition = mainOutput.CastPosition
				return true
			}
		}

		if (GridNav === undefined)
			return false

		const CellCount = 30
		for (let i = 0; i < CellCount; ++i) {
			for (let j = 0; j < CellCount; ++j) {
				const point = new Vector2(
					(GridNav.EdgeSize * (i - (CellCount / 2))) + targetPredictedPosition.x,
					(GridNav.EdgeSize * (j - (CellCount / 2))) + targetPredictedPosition.y)
				const pos = GridNav.GetGridPosForPos(point)
				const flags = GridNav.GetCellFlagsForGridPos(pos.x, pos.y)
				if (BitsExtensions.HasBit(flags, GridNavCellFlags.InteractionBlocker)) {
					const rec = new PRectangle(
						targetPredictedPosition,
						this.Owner.Position.Extend2D(targetPredictedPosition, range),
						width - 100)
					if (rec.IsInside(point)) {
						this.castPosition = mainOutput.CastPosition;
						return true
					}
				}
			}
		}
		return false
	}

	public UseAbility(aoe: boolean) {

		if (!this.castPosition.IsValid)
			return super.UseAbility(aoe)

		if (!this.Ability.UseAbility(TargetManager.Target!, this.castPosition))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		if (isIDisable(this.Ability))
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(TargetManager.Target!))

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
