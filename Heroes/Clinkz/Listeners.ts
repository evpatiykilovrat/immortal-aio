import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { CAbilitiesCreated, ClinkzMode } from "./Clinkz";
import { ClinkzMenu } from "./menu";

HeroService.RegisterHeroModule(ClinkzMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(ClinkzMenu, oldState)
TargetManager.DestroyParticleDrawTarget(ClinkzMenu)

function onTick(unit: UnitX) {
	if (!ClinkzMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = ClinkzMenu.TargetMenuTree
	TargetManager.TargetLocked = ClinkzMenu.ComboKey.is_pressed || ClinkzMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		ClinkzMode,
		ClinkzMenu.ComboMode,
		ClinkzMenu,
		ClinkzMenu.ComboKey,
	)

	ComboMode(
		unit,
		ClinkzMode,
		ClinkzMenu.HarrasMode,
		ClinkzMenu,
		ClinkzMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!ClinkzMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		ClinkzMenu.TargetMenuTree.DrawTargetParticleColor,
		ClinkzMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(ClinkzMenu.ComboMode, owner)
	SwitchPanel.Draw(ClinkzMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!ClinkzMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (ClinkzMenu.ComboKey.is_pressed || ClinkzMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!ClinkzMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((ClinkzMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!ClinkzMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(ClinkzMenu.ComboMode)
		SwitchPanel.MouseLeftDown(ClinkzMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (ClinkzMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (ClinkzMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, ClinkzMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, ClinkzMenu))
}

function onDestoyed(owner: UnitX) {
	if (ClinkzMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== ClinkzMenu.npc_name)
		return
	CAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (ClinkzMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
