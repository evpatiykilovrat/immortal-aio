import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { FAbilitiesCreated, FacelessVoidMode } from "./FacelessVoid";
import { FacelessVoidMenu } from "./menu";

HeroService.RegisterHeroModule(FacelessVoidMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(FacelessVoidMenu, oldState)
TargetManager.DestroyParticleDrawTarget(FacelessVoidMenu)

function onTick(unit: UnitX) {
	if (!FacelessVoidMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = FacelessVoidMenu.TargetMenuTree
	TargetManager.TargetLocked = FacelessVoidMenu.ComboKey.is_pressed || FacelessVoidMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		FacelessVoidMode,
		FacelessVoidMenu.ComboMode,
		FacelessVoidMenu,
		FacelessVoidMenu.ComboKey,
	)

	ComboMode(
		unit,
		FacelessVoidMode,
		FacelessVoidMenu.HarrasMode,
		FacelessVoidMenu,
		FacelessVoidMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!FacelessVoidMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		FacelessVoidMenu.TargetMenuTree.DrawTargetParticleColor,
		FacelessVoidMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(FacelessVoidMenu.ComboMode, owner)
	SwitchPanel.Draw(FacelessVoidMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!FacelessVoidMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (FacelessVoidMenu.ComboKey.is_pressed || FacelessVoidMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!FacelessVoidMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((FacelessVoidMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!FacelessVoidMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(FacelessVoidMenu.ComboMode)
		SwitchPanel.MouseLeftDown(FacelessVoidMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (FacelessVoidMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (FacelessVoidMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, FacelessVoidMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, FacelessVoidMenu))
}

function onDestoyed(owner: UnitX) {
	if (FacelessVoidMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== FacelessVoidMenu.npc_name)
		return
	FAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (FacelessVoidMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
