import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const DrowRangerMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_drow_ranger",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_sheepstick",
		"item_orchid",
		"item_bloodthorn",
		"item_rod_of_atos",
		"item_gungir",
		"item_diffusal_blade",
		"item_force_staff",
		"item_hurricane_pike",
	],
	Abilities: [
		"drow_ranger_frost_arrows",
		"drow_ranger_wave_of_silence",
		"drow_ranger_multishot",
	],
})
