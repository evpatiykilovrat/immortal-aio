import { ActiveAbility, HitChanceX, IBuff, isIToggleable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIOBuff extends AIOUsable {

	public Buff: ActiveAbility & IBuff

	constructor(ability: ActiveAbility) {
		super(ability)
		this.Buff = ability as ActiveAbility & IBuff
	}

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Owner))
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast() {
		const target = this.Owner
		if (target.IsInvulnerable)
			return false

		if (target.Equals(this.Buff.Owner)) {
			if (!this.Buff.BuffsOwner)
				return false
		} else {
			if (!this.Buff.BuffsAlly)
				return false
		}

		if (target.IsMagicImmune && !this.Buff.PiercesMagicImmunity(target))
			return false

		if (target.HasBuffByName(this.Buff.BuffModifierName))
			return false

		if (isIToggleable(this.Buff) && this.Buff.Enabled)
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (typeof args[0] === "number")
			return this.UseAbilityCheckDistance(args[0])
		return this.UseAbility_(aoe)
	}

	private UseAbility_(aoe: boolean, ...args: any[]) {
		if (!this.Ability.UseAbility(TargetManager.Owner, TargetManager.AllyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	private UseAbilityCheckDistance(distance: number) {
		if (this.Owner.Distance(TargetManager.Target!) > distance)
			return false
		return this.UseAbility_(true)
	}
}
