import {  HitChanceX, isIDisable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIONoTarget extends AIOUsable {



	public ForceUseAbility(): boolean {

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast(): boolean {
		return true
	}

	public UseAbility(aoe: boolean): boolean {
		const target = TargetManager.Target!

		if (aoe) {
			if (!this.Ability.UseAbility(target, HitChanceX.Low || HitChanceX.Impossible ||HitChanceX.Medium ||HitChanceX.High ||HitChanceX.Immobile ))
				return false
		}

		const delay = this.Ability.GetCastDelay(target)


		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}

