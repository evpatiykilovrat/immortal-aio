import { ActiveAbility, HitChanceX, OrbWalker, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, AIOUsable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { VoidChronosphere } from "./Chronosphere";

export class VoidTimeWalk extends AIOBlink {
	private readonly blinkPosition = new Vector3().Invalidate()

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const chrono = args.find(x => x.Ability instanceof VoidChronosphere) as VoidChronosphere
		if (chrono === undefined) {
			if (this.Owner.Distance(target) < 300)
				return false
			this.blinkPosition.Invalidate()
			return true
		}
		const input = chrono.Ability.GetPredictionInput(target, TargetManager.Heroes.filter(x => x.IsValid))
		input.CastRange += this.Ability.CastRange
		input.Range += this.Ability.CastRange
		const output = chrono.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low || output.AoeTargetsHit.length < chrono.HitCounts)
			return false

		const range = Math.min(this.Ability.CastRange, this.Owner.Distance(output.CastPosition) - 200)
		this.blinkPosition.CopyFrom(this.Owner.Position.Extend2D(output.CastPosition, range))

		if (this.Owner.Distance(this.blinkPosition) > this.Ability.CastRange)
			return false

		return true
	}

	public UseAbility(aoe: boolean) {
		if (!this.blinkPosition.IsValid) {
			const input = this.Ability.GetPredictionInput(TargetManager.Target!)
			input.Delay += 0.5;
			const output = this.Ability.GetPredictionOutput(input);
			if (output.HitChance < HitChanceX.Low)
				return false
			this.blinkPosition.CopyFrom(output.CastPosition)
		}

		if (!this.Owner.UseAbility(this.Ability, { intent: this.blinkPosition }))
			return false

		const delay = this.Ability.GetCastDelay(this.blinkPosition)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
