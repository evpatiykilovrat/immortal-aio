import { ActiveAbility, BlackHole, HitChanceX, OrbWalker, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, AIOUsable, TargetManager } from "_ICore_/Index";
import { BaseMenu } from "../../../Menu/Base";

export class BlinkDaggerEnigma extends AIOBlink {
	private readonly blinkPosition: Vector3 = new Vector3().Invalidate()

	constructor(ability: ActiveAbility, private menu: BaseMenu) {
		super(ability)
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {
		const blackHole = args.find(x => x !== undefined && x.Ability instanceof BlackHole)
		if (blackHole === undefined)
			return false

		const input = blackHole.Ability.GetPredictionInput(target, TargetManager.IsValidEnemyHeroes)
		input.CastRange += this.Ability.CastRange
		input.Range += this.Ability.CastRange
		const output = blackHole.Ability.GetPredictionOutput(input)
		const counts = this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1)

		if (output.HitChance < HitChanceX.Low || output.AoeTargetsHit.length < counts)
			return false

		if ((this.Owner.Distance(output.CastPosition) - 200) < 100) {
			this.blinkPosition.Invalidate()
			return false
		}

		const range = Math.min(this.Ability.CastRange, this.Owner.Distance(output.CastPosition) - 200)
		this.blinkPosition.CopyFrom(this.Owner.Position.Extend2D(output.CastPosition, range))

		if (this.Owner.Distance(this.blinkPosition) > this.Ability.CastRange)
			return false

		return true
	}

	public UseAbility(aoe: boolean) {
		if (!this.blinkPosition.IsValid)
			return false
		if (!this.Owner.UseAbility(this.Ability, { intent: this.blinkPosition }))
			return false
		OrbWalker.Sleeper.Sleep(0.5, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(0.5)
		return true
	}
}
