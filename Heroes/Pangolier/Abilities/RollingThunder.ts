import { ArrayExtensions, BitsExtensions, GridNav, GridNavCellFlags, Vector2, Vector3 } from "wrapper/Imports";
import { ActiveAbility, BlinkDagger, RollingThunder, UnitX } from "immortal-core/Imports";
import { AIONuke, AIOUsable, TargetManager } from "_ICore_/Index";

export class PangoRollingThunder extends AIONuke {

	private readonly rollingThunder: ActiveAbility & RollingThunder

	constructor(ability: ActiveAbility) {
		super(ability)
		this.rollingThunder = ability as ActiveAbility & RollingThunder
	}

	public get GetPosition() {
		const CellCount = 40
		const center = this.Owner.Position
		const wallPositions: Vector3[] = []

		if (GridNav === undefined)
			return new Vector3()

		for (var i = 0; i < CellCount; ++i) {
			for (var j = 0; j < CellCount; ++j) {
				const point = new Vector2(
					(GridNav.EdgeSize * (i - (CellCount / 2))) + center.x,
					(GridNav.EdgeSize * (j - (CellCount / 2))) + center.x)
				const pos = GridNav.GetGridPosForPos(point)
				const flags = GridNav.GetCellFlagsForGridPos(pos.x, pos.y)
				if (BitsExtensions.HasBit(flags, GridNavCellFlags.InteractionBlocker))
					wallPositions.push(new Vector3(point.x, point.y, 0))
			}
		}
		return ArrayExtensions.orderBy(wallPositions.filter(x => this.CheckWall(x)), x => this.Owner.Distance(x))[0]
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (this.Owner.GetAngle(TargetManager.Target!.Position) > 0.75)
			return false

		return true
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {
		const blink = args.find(x => x !== undefined && x.Ability instanceof BlinkDagger)
		if (blink === undefined)
			return this.Owner.Distance(TargetManager.Target!) < 600
		return this.Owner.Distance(TargetManager.Target!) < blink.Ability.CastRange + 200
	}

	private CheckWall(wall: Vector3) {
		const distance = this.Owner.Distance(wall)
		const turnTime = (this.Owner.GetAngle(wall) / this.rollingThunder.TurnRate) + 0.5
		if (turnTime > (distance / this.rollingThunder.Speed))
			return false

		if (distance > 600)
			return false

		return true
	}
}
