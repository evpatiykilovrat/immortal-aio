import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../../Menu/Base";

export const EarthSpiritMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_earth_spirit",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_force_staff",
		"item_spirit_vessel",
		"item_urn_of_shadows",
		"item_veil_of_discord",
		"item_blade_mail",
		"item_heavens_halberd",
		"item_shivas_guard",
		"item_sheepstick",
		"item_ethereal_blade",
		"item_dagon_5",
	],
	Abilities: [
		"earth_spirit_boulder_smash",
		"earth_spirit_rolling_boulder",
		"earth_spirit_geomagnetic_grip",
		"earth_spirit_magnetize",
		"earth_spirit_stone_caller",
	],
})
