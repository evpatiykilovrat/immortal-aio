import { Menu } from "wrapper/Imports"

export interface SwitchPanel {
	PanelState: Menu.Toggle;
	PanelSize: Menu.Slider;
	PanelSelector: Menu.ImageSelector;
	PanelOpacityState: Menu.Slider;
	PanelPosistionX: Menu.Slider;
	PanelPosistionY: Menu.Slider;
	PanelIconsPosition: Menu.Dropdown
}

export const CreateSwitchPanel = (baseNode: Menu.Node, abilities: string[]): SwitchPanel => {
	const PanelTree = baseNode.AddNode("Switch panel")
	const PanelState = PanelTree.AddToggle("State", baseNode.InternalName !== "Harass", "Enable abilities and items in game")

	const fillerDefault = abilities.filter(x =>
		x === "item_black_king_bar" || x === "item_minotaur_horn" || x === "item_blink",
	)

	const PanelSelector = PanelTree.AddImageSelector(
		"ABILITIES_X", abilities,
	new Map(fillerDefault.map(name => [name, true])))

	const PanelSize = PanelTree.AddSlider("Size", 25.9, 12, 100, 1)
	const PanelOpacityState = PanelTree.AddSlider("Opacity", 73, 30, 100)
	const PanelPosistionX = PanelTree.AddSlider("Position: X", baseNode.InternalName !== "Harass" ? 1262 : 1040, 0, 1920)
	const PanelPosistionY = PanelTree.AddSlider("Position: Y", 888, 0, 1080)
	const PanelIconsPosition = PanelTree.AddDropdown("Icons position", ["Horizontal", "Vertical"])

	return {
		PanelState,
		PanelSize,
		PanelSelector,
		PanelPosistionX,
		PanelPosistionY,
		PanelOpacityState,
		PanelIconsPosition,
	}
}
