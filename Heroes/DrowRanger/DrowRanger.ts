import { AbilityX, BlinkDagger, Bloodthorn, DiffusalBlade, EventsX, ForceStaff, Gleipnir, Gust, HurricanePike, Multishot, OrchidMalevolence, RodOfAtos, ScytheOfVyse, UnitX } from "immortal-core/Imports"
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIOForceStaff, AIOHurricanePike, AIONuke, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base"

const data = {
	gust: undefined as Nullable<AIODisable>,
	shot: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	blink: undefined as Nullable<AIOBlink>,
	atos: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	diffusal: undefined as Nullable<AIODebuff>,
	force: undefined as Nullable<AIOForceStaff>,
	pike: undefined as Nullable<AIOHurricanePike>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}
export const DAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Gust)
		data.gust = new AIODisable(abil)
	if (abil instanceof Multishot)
		data.shot = new AIONuke(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof HurricanePike)
		data.pike = new AIOHurricanePike(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
}

export const DrowRangerMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, unit.GetAttackRange(), 350))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.gust))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.force, unit.GetAttackRange(), 400))
		return true

	if (helper.UseAbility(data.pike, unit.GetAttackRange(), 400))
		return true

	if (helper.UseAbility(data.shot))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
