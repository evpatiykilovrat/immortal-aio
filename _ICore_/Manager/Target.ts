import { EventsX, HeroX, ParticlesX, TickSleeperX, UnitsX, UnitX, UnitXManager } from "immortal-core/Imports";
import { ArrayExtensions, Input, Menu, modifierstate, npc_dota_hero_phoenix } from "wrapper/Imports";
import { BaseMenu } from "../../Menu/Base";
import { TargetMenu } from "../../Menu/Target";

const ParticleManager = new ParticlesX()

export class TargetManager {

	public static TargetLocked = false
	public static TargetDistance = 2500
	public static Owner: HeroX
	public static Target: Nullable<UnitX>
	public static MenuSettings: TargetMenu
	public static TargetSleeper = new TickSleeperX()

	public static get HasValidTarget() {
		return this.Target?.IsValid === true
			&& this.Target.IsAlive
			&& (this.Target.IsVisible || this.Target.IsTeleporting || (this.Target.IsMoving && this.TargetLocked))
	}

	public static get Heroes() {
		return UnitXManager.HeroesX
	}

	public static get AllEnemyHeroes() {
		return this.Heroes.filter(x => x.IsAlive && x.IsEnemy() && !x.Owner.IsIllusion && (x.IsVisible || x.IsTeleporting))
	}

	public static get AllEnemyUnits() {
		return UnitsX.All.filter(x => x.IsAlive && x.IsEnemy() && !x.Owner.IsIllusion)
	}

	public static get AllyHeroes() {
		return this.Heroes.filter(x => !x.IsEnemy() && x.IsAlive && !x.IsInvulnerable && (x.IsVisible || x.IsTeleporting))
	}

	public static get EnemyHeroes() {
		return this.AllEnemyHeroes
	}

	public static get IsValidEnemyHeroes() {
		return this.AllEnemyHeroes.filter(x => this.IsValid(x))
	}

	public static get IsValidAllyHeroes() {
		return this.AllyHeroes.filter(x => this.IsValid(x))
	}

	public static get IsValidAllyUnits() {
		return UnitsX.All.filter(x => !x.IsEnemy() && x.IsAlive)
	}

	public static get IsValidEnemyUnits() {
		return UnitsX.All.filter(x => x.IsEnemy() && this.IsValid(x))
	}

	public static get ClosestToMouse() {
		const MousePosition = Input.CursorOnWorld
		return ArrayExtensions.qorderBy(this.EnemyHeroes.filter(x => x.Distance(MousePosition) <= 750),
			x => x.Distance(MousePosition))
	}

	public static GetTarget() {
		if (this.Owner === undefined)
			return
		switch (this.MenuSettings.FocusTarget.selected_id) {
			case 0:
				this.Target = this.ClosestToMouse
					.find(x => this.IsValidGetTarget(x))
				break;
			case 1:
				this.Target = this.ClosestToUnit(this.Owner)
					.find(x => this.IsValidGetTarget(x))
				break;
			case 2:
				this.Target = this.LowestHealth(this.Owner)
					.find(x => this.IsValidGetTarget(x))
				break;
		}
	}

	public static LowestHealth(unit: UnitX, range: number = this.TargetDistance) {
		return ArrayExtensions.qorderBy(this.EnemyHeroes.filter(x => x.Distance(unit.Position) <= range), x => x.Health)
	}

	public static ClosestAllyHeroToMouse(unit: UnitX, ignoreSelf: boolean = true) {
		const MousePosition = Input.CursorOnWorld
		return ArrayExtensions.qorderBy(this.AllyHeroes.filter(x => (!ignoreSelf || !x.Equals(unit)) && x.Distance(MousePosition) < 500),
			x => x.Distance(MousePosition))
	}

	public static ClosestToUnit(unit: UnitX, range: number = this.TargetDistance) {
		const position = unit.Position
		return ArrayExtensions.qorderBy(this.EnemyHeroes.filter(x => x.Distance(position) <= range), x => x.Distance(position))
	}

	public static DrawTargetParticle(unit: UnitX, activeColor: Menu.ColorPicker, deactivatedColor: Menu.ColorPicker) {

		const owner = unit
		const target = this.Target!

		if ((!target.IsVisible && !this.TargetLocked && !target.IsTeleporting)
			|| (owner.Distance(target.Position) > this.TargetDistance)) {
			this.RemoveTargetParticle(unit)
			return
		}

		ParticleManager.DrawLineToTarget(`SetComboTarget_${owner.Handle}`, owner.Owner, target.Owner, !this.TargetLocked
			? activeColor.selected_color
			: deactivatedColor.selected_color,
		{
			Start: owner.Position,
			End: target.Position,
		})
	}

	public static RemoveTargetParticle(unit?: UnitX) {
		ParticleManager.DestroyByKey(`SetComboTarget_${unit?.Handle}`)
	}

	public static Update() {
		if (this.Owner === undefined || this.MenuSettings === undefined)
			return
		if (this.TargetLocked && this.MenuSettings.LockTarget.value) {
			if (this.MenuSettings.DeathSwitch.value && !this.HasValidTarget)
				this.GetTarget()
		} else {
			this.GetTarget()
		}
	}

	public static Draw(unit: UnitX, activeColor: Menu.ColorPicker, deactivatedColor: Menu.ColorPicker) {
		if (this.Owner === undefined || this.MenuSettings === undefined)
			return
		if (!this.MenuSettings.DrawTargetParticle.value)
			return
		if (!this.HasValidTarget || !this.MenuSettings.BaseState) {
			this.RemoveTargetParticle(unit)
			return
		}
		this.DrawTargetParticle(unit, activeColor, deactivatedColor)
	}

	public static Dispose() {
		this.Owner = undefined!
		this.Target = undefined
		this.TargetLocked = false
		this.TargetSleeper.ResetTimer()
	}

	public static DestroyParticleDrawTarget(menu: BaseMenu) {
		menu.BaseState.OnDeactivate(this.DestroyMenuOnParticles)
		menu.TargetMenuTree.DrawTargetParticle.OnDeactivate(this.DestroyMenuOnParticles)
	}

	private static IsValid(x: UnitX) {
		return x.IsVisible && x.IsAlive
			&& !x.Owner.IsIllusion
			&& !x.IsInvulnerable
	}

	private static IsValidGetTarget(x: UnitX) {
		return !x.UnitState.some(z => (z & (modifierstate.MODIFIER_STATE_UNSELECTABLE | modifierstate.MODIFIER_STATE_COMMAND_RESTRICTED))
			=== (modifierstate.MODIFIER_STATE_UNSELECTABLE | modifierstate.MODIFIER_STATE_COMMAND_RESTRICTED))
	}

	private static DestroyMenuOnParticles() {
		if (TargetManager.AllyHeroes.length === 0)
			return
		TargetManager.AllyHeroes.forEach(unit => TargetManager.RemoveTargetParticle(unit))
	}
}

EventsX.on("UnitCreated", unit => {
	if (!TargetManager.TargetLocked || !unit.IsEnemy() || unit.Name !== "npc_dota_phoenix_sun")
		return
	if (TargetManager.Target?.Name === "npc_dota_hero_phoenix")
		TargetManager.Target = unit
})

EventsX.on("UnitDestroyed", unit => {
	if (!TargetManager.TargetLocked || !unit.IsEnemy() || unit.Name !== "npc_dota_phoenix_sun")
		return
	if (TargetManager.Target?.Equals(unit) === true)
		TargetManager.Target = TargetManager.Heroes.find(x => x.BaseOwner instanceof npc_dota_hero_phoenix)
})
