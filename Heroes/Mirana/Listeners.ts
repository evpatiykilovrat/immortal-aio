import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { MiranaMenu } from "./menu";
import { MAbilitiesCreated, MiranaMode, OrbWalkMirana } from "./Mirana";

HeroService.RegisterHeroModule(MiranaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(MiranaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(MiranaMenu)

function onTick(unit: UnitX) {
	if (!MiranaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = MiranaMenu.TargetMenuTree
	TargetManager.TargetLocked = MiranaMenu.ComboKey.is_pressed || MiranaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		MiranaMode,
		MiranaMenu.ComboMode,
		MiranaMenu,
		MiranaMenu.ComboKey,
	)

	ComboMode(
		unit,
		MiranaMode,
		MiranaMenu.HarrasMode,
		MiranaMenu,
		MiranaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!MiranaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		MiranaMenu.TargetMenuTree.DrawTargetParticleColor,
		MiranaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(MiranaMenu.ComboMode, owner)
	SwitchPanel.Draw(MiranaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!MiranaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (MiranaMenu.ComboKey.is_pressed || MiranaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!MiranaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((MiranaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!MiranaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(MiranaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(MiranaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (MiranaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (MiranaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, MiranaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new OrbWalkMirana(owner, MiranaMenu))
}

function onDestoyed(owner: UnitX) {
	if (MiranaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== MiranaMenu.npc_name)
		return
	MAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (MiranaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
