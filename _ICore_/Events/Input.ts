import { GameX } from "immortal-core/Imports"
import { DOTAGameUIState_t, GameState, InputEventSDK } from "wrapper/Imports"
import { HERO_DATA } from "../data"
import { TargetManager } from "../Manager/Target"

const IsValidKEY = () => {
	return GameX.IsInGame
		&& TargetManager.AllyHeroes.filter(x => x.IsControllable && !x.IsEnemy()).length !== 0
		&& GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
}

InputEventSDK.on("MouseKeyDown", key => {
	if (!IsValidKEY())
		return true
	return TargetManager.AllyHeroes.filter(x => x.IsControllable && !x.IsEnemy()).some(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return true
		return hero_module.onMouseDown(key)
	})
})

InputEventSDK.on("KeyDown", key => {
	if (!IsValidKEY())
		return true
	return TargetManager.AllyHeroes.filter(x => x.IsControllable && !x.IsEnemy()).some(unit => {
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return true
		return hero_module.onKeyDown(key)
	})
})
