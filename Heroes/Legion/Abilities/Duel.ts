import { ActiveAbility } from "immortal-core/Imports";
import { AIOTargetable, TargetManager } from "_ICore_/Index";

export class AIODuel extends AIOTargetable {

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldCast() {
		const target = TargetManager.Target!
		if (!target.IsVisible || target.IsInvulnerable || target.IsLinkensProtected)
			return false

		return true
	}
}
