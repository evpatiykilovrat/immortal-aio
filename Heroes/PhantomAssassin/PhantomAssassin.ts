import { AbilityX, AbyssalBlade, Bloodthorn, DiffusalBlade, EventsX, Gleipnir, OrchidMalevolence, PhantomStrike, RodOfAtos, ScytheOfVyse, StiflingDagger, UnitX } from "immortal-core/Imports"
import { AbilityHelper, AIOBloodthorn, AIODebuff, AIODisable, AIONuke, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base"

const data = {
	abyssal: undefined as Nullable<AIODisable>,
	dagger: undefined as Nullable<AIONuke>,
	strike: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	atos: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	diffusal: undefined as Nullable<AIODebuff>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}

export const PAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof PhantomStrike)
		data.strike = new AIONuke(abil)
	if (abil instanceof StiflingDagger)
		data.dagger = new AIONuke(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
}

export const PhantomAssassinMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.dagger))
		return true

	if (helper.UseAbility(data.strike))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
