import { OrbWalker, PRectangle } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, HERO_DATA, TargetManager } from "_ICore_/Index";

export class SpiritRollingBoulderBlink extends AIOBlink {

	public UseAbility(aoe: boolean, toPosition: Vector3) {
		if (this.Owner.Distance(toPosition) < 200)
			return false

		const position = this.Owner.Position.Extend2D(toPosition, Math.min(this.Ability.CastRange - 25, this.Owner.Distance(toPosition)))
		const rec = new PRectangle(this.Owner.Position, position, this.Ability.Radius)

		if (TargetManager.Heroes.some(x => x.IsEnemy() && x.IsAlive && rec.IsInside(x.Position)))
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: position }))
			return false

		const delay = this.Ability.GetCastDelay(position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true;
	}
}
