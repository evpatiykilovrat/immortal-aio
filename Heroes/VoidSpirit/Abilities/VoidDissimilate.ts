import { AIOAoe, TargetManager } from "_ICore_/Index";

export class VoidDissimilate extends AIOAoe {
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!
		if (this.Owner.Distance(target) > 600 && target.GetImmobilityDuration < 1)
			return false

		return true
	}
}
