import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { LAbilitiesCreated, LunaMode } from "./Luna";
import { LunaMenu } from "./menu";

HeroService.RegisterHeroModule(LunaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(LunaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(LunaMenu)

function onTick(unit: UnitX) {
	if (!LunaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = LunaMenu.TargetMenuTree
	TargetManager.TargetLocked = LunaMenu.ComboKey.is_pressed || LunaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		LunaMode,
		LunaMenu.ComboMode,
		LunaMenu,
		LunaMenu.ComboKey,
	)

	ComboMode(
		unit,
		LunaMode,
		LunaMenu.HarrasMode,
		LunaMenu,
		LunaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!LunaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		LunaMenu.TargetMenuTree.DrawTargetParticleColor,
		LunaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(LunaMenu.ComboMode, owner)
	SwitchPanel.Draw(LunaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!LunaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (LunaMenu.ComboKey.is_pressed || LunaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!LunaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((LunaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!LunaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(LunaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(LunaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (LunaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (LunaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, LunaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, LunaMenu))
}

function onDestoyed(owner: UnitX) {
	if (LunaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== LunaMenu.npc_name)
		return
	LAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (LunaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
