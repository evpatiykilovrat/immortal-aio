import { ActiveAbility, BlinkDagger, EntityX, OrbWalker, PRectangle, TimberChain, UnitX } from "immortal-core/Imports";
import { ArrayExtensions, Vector3 } from "wrapper/Imports";
import { AIONuke, AIOUsable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class AIOTimberChain extends AIONuke {

	private readonly CPosition = new Vector3()
	private readonly timberChain: TimberChain

	constructor(ability: ActiveAbility) {
		super(ability)
		this.timberChain = ability as TimberChain
	}

	public CanHit() {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target)
		input.Delay += this.Owner.Distance(target) / this.Ability.Speed
		const output = this.Ability.GetPredictionOutput(input)
		const ownerPosition = this.Owner.Position
		this.CPosition.CopyFrom(output.CastPosition)

		let polygon = new PRectangle(ownerPosition, this.CPosition, this.timberChain.ChainRadius)
		const availableTrees = EntityX.Tree.filter(x => x.IsAlive && x.Distance2D(ownerPosition) < this.Ability.CastRange)

		for (const tree of availableTrees) {
			if (polygon.IsInside(tree.Position))
				return false
		}

		const chainEndPosition = ownerPosition.Extend2D(this.CPosition, this.Ability.CastRange)
		polygon = new PRectangle(this.CPosition, chainEndPosition, this.timberChain.Radius)

		for (const tree of availableTrees) {
			if (polygon.IsInside(tree.Position)) {
				this.CPosition.CopyFrom(tree.Position)
				return true
			}
		}

		if (this.Ability.Level < 4 || ownerPosition.Distance2D(this.CPosition) < 400)
			return false

		for (const tree of ArrayExtensions.orderBy(availableTrees.filter(x => x.IsValid), x => x.Distance2D(this.CPosition))) {
			const treePosition = tree.Position

			if (treePosition.Distance2D(this.CPosition) > 500 && target.GetAngle(treePosition) > 0.75)
				continue

			if (ownerPosition.Distance2D(this.CPosition) < treePosition.Distance2D(this.CPosition)) {
				continue;
			}

			polygon = new PRectangle(ownerPosition, treePosition, this.timberChain.ChainRadius)
			if (availableTrees.some(x => x.Index !== tree.Index && polygon.IsInside(x.Position)))
				continue

			this.CPosition.CopyFrom(treePosition)
			return true
		}

		return false
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {
		const blink = args.find(x => x?.Ability instanceof BlinkDagger)

		if (blink === undefined)
			return true

		if (this.Owner.Distance(target) < 800)
			return true

		return false
	}

	public UseAbility(aoe: boolean) {
		if (!this.Owner.UseAbility(this.Ability, { intent: this.Owner.Position.Extend2D(this.CPosition, 400) }))
			return false

		const delay = this.Ability.GetCastDelay(this.CPosition)
		this.Ability.ActionSleeper.Sleep(delay)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(this.Ability.GetHitTime(this.CPosition), this.Owner.Handle)
		return true
	}
}
