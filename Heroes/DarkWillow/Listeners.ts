import { AbilityX, EventsX, UnitX } from "immortal-core/Imports"
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports"
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index"
import { WillowMenu } from "./menu"
import { onAbilityCreatedWillow, OrbWalkWillow, WillowMode } from "./Willow"

HeroService.RegisterHeroModule(WillowMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(WillowMenu, oldState)
TargetManager.DestroyParticleDrawTarget(WillowMenu)

function onTick(unit: UnitX) {

	if (!WillowMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = WillowMenu.TargetMenuTree
	TargetManager.TargetLocked = WillowMenu.ComboKey.is_pressed || WillowMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		WillowMode,
		WillowMenu.ComboMode,
		WillowMenu,
		WillowMenu.ComboKey,
	)

	ComboMode(
		unit,
		WillowMode,
		WillowMenu.HarrasMode,
		WillowMenu,
		WillowMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!WillowMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		WillowMenu.TargetMenuTree.DrawTargetParticleColor,
		WillowMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(WillowMenu.ComboMode, owner)
	SwitchPanel.Draw(WillowMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!WillowMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (WillowMenu.ComboKey.is_pressed || WillowMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!WillowMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((WillowMenu.ComboKey.assigned_key as VKeys) === key || (WillowMenu.HarassKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!WillowMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(WillowMenu.ComboMode)
		SwitchPanel.MouseLeftDown(WillowMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((WillowMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (WillowMenu.HarassKey.assigned_key as VMouseKeys) === key))
		return false
	return true
}

function onCreated(owner: UnitX) {
	if (WillowMenu.npc_name !== owner.Name)
		return
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new OrbWalkWillow(owner, WillowMenu))
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, WillowMenu.Shields))
}

function onDestoyed(owner: UnitX) {
	if (WillowMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== WillowMenu.npc_name)
		return
	onAbilityCreatedWillow(abil)
}

EventsX.on("removeControllable", owner => {
	if (WillowMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
