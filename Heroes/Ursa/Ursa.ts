import { AbilityX, AbyssalBlade, BlackKingBar, BlinkDagger, Bloodthorn, DiffusalBlade, Earthshock, Enrage, EventsX, Nullifier, OrchidMalevolence, Overpower, UnitX } from "immortal-core/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIONuke, AIONullifier, AIOShield, BaseOrbwalk, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { UrsaEnrage } from "./Abilities/Enrage";
import { EnrageMode, UrsaMenu } from "./menu";

const data = {
	bkb: undefined as Nullable<AIOShield>,
	shock: undefined as Nullable<AIONuke>,
	overpower: undefined as Nullable<AIOBuff>,
	enrage: undefined as Nullable<UrsaEnrage>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	nullifier: undefined as Nullable<AIONullifier>,
	diffusal: undefined as Nullable<AIODebuff>,
	abyssal: undefined as Nullable<AIODisable>,
	blink: undefined as Nullable<AIOBlink>,
}

export const UAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof Earthshock)
		data.shock = new AIONuke(abil)
	if (abil instanceof Overpower)
		data.overpower = new AIOBuff(abil)
	if (abil instanceof Enrage)
		data.enrage = new UrsaEnrage(abil, UrsaMenu, EnrageMode)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
}

export const UrsaMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.bkb, 400))
		return true

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.overpower))
		return true

	if (unit.Distance(TargetManager.Target!) > unit.GetAttackRange(TargetManager.Target)
		&& helper.UseAbility(data.shock)) {
		return true
	}

	if (helper.UseAbility(data.blink, 400, 0))
		return true

	if (helper.UseAbility(data.enrage, 300))
		return true

	return false
}

export class UrsaOrbwalk extends BaseOrbwalk {
	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (this.Owner.HasBuffByName("modifier_ursa_overpower"))
			return super.OrbwalkSet(target, attack, this.Owner.Distance(target) > this.Owner.AttackRange())
		return super.OrbwalkSet(target, attack, move)
	}
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
