import { ActiveAbility, EventsX, OrbWalker } from "immortal-core/Imports";
import { EventsSDK, Fountain } from "wrapper/Imports";
import { AIOBlink, HERO_DATA, TargetManager } from "_ICore_/Index";

let EnemyFountain: Fountain[] = []

export class AIOAntiMageBlink extends AIOBlink {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!

		const attackRange = this.Owner.GetAttackRange(target)
		const distance = target.Distance(this.Owner)

		if (distance <= attackRange + 100)
			return false

		if (distance <= attackRange + 250 && this.Owner.IdealSpeed > target.IdealSpeed + 50)
			return false

		const fountain = EnemyFountain.find(x => x.IsEnemy())
		if (fountain === undefined)
			return false

		const position = target.GetAngle(this.Owner.BaseOwner) < 1
			? target.Position.Extend2D(fountain.Position, 100)
			: target.PredictedPosition(this.Ability.CastPoint + 0.3)

		if (this.Owner.Distance(position) > this.Ability.CastRange)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: position }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}

EventsSDK.on("EntityCreated", ent => {
	if (ent instanceof Fountain)
		EnemyFountain.push(ent)
})

EventsX.on("GameEnded", () => {
	EnemyFountain = []
})
