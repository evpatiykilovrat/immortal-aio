import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const AntiMageMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_antimage",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_manta",
		"item_orchid",
		"item_diffusal_blade",
		"item_abyssal_blade",
		"item_nullifier",
		"item_bloodthorn",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_solar_crest",
		"item_medallion_of_courage",
		"item_sheepstick",
	],
	Abilities: [
		"antimage_blink",
		"antimage_counterspell",
		"antimage_mana_void",
	],
})
