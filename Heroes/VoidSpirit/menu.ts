import { Attributes } from "wrapper/Imports"
import { BaseHeroMenu } from "../../Menu/Base"

export const VoidSpiritMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_void_spirit",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_mjollnir",
		"item_orchid",
		"item_bloodthorn",
		"item_shivas_guard",
		"item_veil_of_discord",
		"item_cyclone",
		"item_dagon_5",
		"item_spirit_vessel",
		"item_urn_of_shadows",
		"item_nullifier",
		"item_sheepstick",
	],
	Abilities: [
		"void_spirit_aether_remnant",
		"void_spirit_dissimilate",
		"void_spirit_resonant_pulse",
		"void_spirit_astral_step",
	],
})
