import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const PangolierMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_pangolier",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_diffusal_blade",
		"item_abyssal_blade",
		"item_sheepstick",
		"item_mjollnir",
	],
	Abilities: [
		"pangolier_swashbuckle",
		"pangolier_shield_crash",
		"pangolier_gyroshell",
	],
})
