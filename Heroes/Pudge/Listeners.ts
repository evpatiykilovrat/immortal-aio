
import { AbilityX, EventsX, UnitX } from "immortal-core/Imports"
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports"
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index"
import { PudgeDrawPrediction, PudgeMenu } from "./menu"
import { DrawHookPosition, PAbilitiesCreated, PudgeMode } from "./Pudge"

HeroService.RegisterHeroModule(PudgeMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(PudgeMenu, oldState)
TargetManager.DestroyParticleDrawTarget(PudgeMenu)

function onTick(unit: UnitX) {
	if (!PudgeMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = PudgeMenu.TargetMenuTree
	TargetManager.TargetLocked = PudgeMenu.ComboKey.is_pressed || PudgeMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		PudgeMode,
		PudgeMenu.ComboMode,
		PudgeMenu,
		PudgeMenu.ComboKey,
	)

	ComboMode(
		unit,
		PudgeMode,
		PudgeMenu.HarrasMode,
		PudgeMenu,
		PudgeMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!PudgeMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		PudgeMenu.TargetMenuTree.DrawTargetParticleColor,
		PudgeMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	DrawHookPosition(owner, PudgeDrawPrediction)
	SwitchPanel.Draw(PudgeMenu.ComboMode, owner)
	SwitchPanel.Draw(PudgeMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!PudgeMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (PudgeMenu.ComboKey.is_pressed || PudgeMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PudgeMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((PudgeMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!PudgeMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(PudgeMenu.ComboMode)
		SwitchPanel.MouseLeftDown(PudgeMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (PudgeMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (PudgeMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, PudgeMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, PudgeMenu))
}

function onDestoyed(owner: UnitX) {
	if (PudgeMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== PudgeMenu.npc_name)
		return
	PAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (PudgeMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	HERO_DATA.ParticleManager.DestroyByKey("Hook")
	HERO_DATA.ParticleManager.DestroyByKey("FatRing")
})
