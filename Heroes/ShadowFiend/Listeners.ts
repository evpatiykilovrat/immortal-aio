import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, nevermore_shadowraze1, nevermore_shadowraze2, nevermore_shadowraze3, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { NevermoreMenu, StateMouseRaze } from "./menu";
import { RazeDirection } from "./RazeDirection/Index";
import { NevermoreOrbWalker, SFAbilitiesCreated, SFMode } from "./ShadowFiend";

HeroService.RegisterHeroModule(NevermoreMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(NevermoreMenu, oldState)
TargetManager.DestroyParticleDrawTarget(NevermoreMenu)

function onTick(unit: UnitX) {

	if (!NevermoreMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = NevermoreMenu.TargetMenuTree
	TargetManager.TargetLocked = NevermoreMenu.ComboKey.is_pressed || NevermoreMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		SFMode,
		NevermoreMenu.ComboMode,
		NevermoreMenu,
		NevermoreMenu.ComboKey,
	)

	ComboMode(
		unit,
		SFMode,
		NevermoreMenu.HarrasMode,
		NevermoreMenu,
		NevermoreMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!NevermoreMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		NevermoreMenu.TargetMenuTree.DrawTargetParticleColor,
		NevermoreMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(NevermoreMenu.ComboMode, owner)
	SwitchPanel.Draw(NevermoreMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!NevermoreMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (NevermoreMenu.ComboKey.is_pressed || NevermoreMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	const abil = order.Ability
	const owner = order.Issuers[0]

	if (StateMouseRaze.value
		&& typeof abil !== "number"
		&& !Input.IsKeyDown(VKeys.CONTROL)
		&& !(NevermoreMenu.ComboKey.is_pressed || NevermoreMenu.HarassKey.is_pressed)
		&& (abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3)) {
		return RazeDirection(abil, owner)
	}

	return true
}

function onKeyDown(key: VKeys) {
	if (!NevermoreMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((NevermoreMenu.ComboKey.assigned_key as VKeys) === key || (NevermoreMenu.HarassKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!NevermoreMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(NevermoreMenu.ComboMode)
		SwitchPanel.MouseLeftDown(NevermoreMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((NevermoreMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (NevermoreMenu.HarassKey.assigned_key as VMouseKeys) === key))
		return false
	return true
}

function onCreated(owner: UnitX) {
	if (NevermoreMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, NevermoreMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new NevermoreOrbWalker(owner, NevermoreMenu))
}

function onDestoyed(owner: UnitX) {
	if (NevermoreMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== NevermoreMenu.npc_name)
		return
	SFAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (NevermoreMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
