import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const LunaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_luna",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_rod_of_atos",
		"item_sheepstick",
		"item_gungir",
		"item_orchid",
		"item_bloodthorn",
		"item_force_staff",
		"item_hurricane_pike",
		"item_mask_of_madness",
	],
	Abilities: [
		"luna_lucent_beam",
		"luna_eclipse",
	],
	LinkenBreak: ["luna_lucent_beam"],
})
