import { AbilityX, BlinkDagger, Bloodthorn, Eclipse, EventsX, ForceStaff, Gleipnir, HurricanePike, LucentBeam, MaskOfMadness, OrchidMalevolence, RodOfAtos, ScytheOfVyse, UnitX } from "immortal-core/Imports";
import { ModeCombo } from "../../Menu/Base";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODisable, AIOForceStaff, AIOHurricanePike, AIONuke, HERO_DATA, ShieldBreakerMap, TargetManager } from "../../_ICore_/Index";
import { LunaElipse } from "./Abilities/LunaEclipse";

const data = {
	blink: undefined as Nullable<AIOBlink>,
	mom: undefined as Nullable<AIOBuff>,
	beam: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	atos: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	eclipse: undefined as Nullable<LunaElipse>,
	force: undefined as Nullable<AIOForceStaff>,
	pike: undefined as Nullable<AIOHurricanePike>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}

export const LAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof MaskOfMadness)
		data.mom = new AIOBuff(abil)
	if (abil instanceof LucentBeam)
		data.beam = new AIONuke(abil)
	if (abil instanceof Eclipse)
		data.eclipse = new LunaElipse(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof HurricanePike)
		data.pike = new AIOHurricanePike(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
}

export const LunaMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 700, 350))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.beam))
		return true

	if (helper.UseAbility(data.eclipse))
		return true

	if (helper.UseAbility(data.force, unit.GetAttackRange(), 400))
		return true

	if (helper.UseAbility(data.pike, unit.GetAttackRange(), 400))
		return true

	if (!helper.CanBeCasted(data.eclipse, false))
		if (helper.UseAbility(data.mom))
			return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
