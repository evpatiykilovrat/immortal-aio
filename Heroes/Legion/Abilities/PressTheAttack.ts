import { ActiveAbility, HitChanceX, OrbWalker } from "immortal-core/Imports";
import { DOTA_ABILITY_BEHAVIOR } from "wrapper/Imports";
import { AIOBuff, HERO_DATA, TargetManager } from "_ICore_/Index";

export class LegionPressTheAttack extends AIOBuff {

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public UseAbility(aoe: boolean) {

		if (this.Ability.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {
			const input = this.Ability.GetPredictionInput(TargetManager.Owner, TargetManager.AllyHeroes.filter(x => !x.IsMagicImmune))
			const output = this.Ability.GetPredictionOutput(input)

			if (output.HitChance < HitChanceX.Low)
				return false

			const castPosition = (output.AoeTargetsHit.length !== 0)
				? output.CastPosition
				: output.CastPosition.Extend2D(TargetManager.Target!.Position, 30)

			if (!this.Owner.UseAbility(this.Ability, { intent: castPosition }))
				return false

		} else {
			if (!this.Ability.UseAbility(TargetManager.Owner, HitChanceX.Low))
				return false
		}

		const delay = this.Ability.GetCastDelay(TargetManager.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
