import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { TemplarMenu } from "./menu";
import { TAbilitiesCreated, TemplarComboMode, TemplarOrbWalk } from "./Templar";
import { TrapExplodeCombo } from "./Unit/PsionicTrap";

HeroService.RegisterHeroModule(TemplarMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(TemplarMenu, oldState)
TargetManager.DestroyParticleDrawTarget(TemplarMenu)

function onTick(unit: UnitX) {
	if (!TemplarMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = TemplarMenu.TargetMenuTree
	TargetManager.TargetLocked = TemplarMenu.ComboKey.is_pressed || TemplarMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		TemplarComboMode,
		TemplarMenu.ComboMode,
		TemplarMenu,
		TemplarMenu.ComboKey,
	)

	if (TargetManager.TargetLocked)
		if (TrapExplodeCombo(TemplarMenu.ComboMode))
			return

	ComboMode(
		unit,
		TemplarComboMode,
		TemplarMenu.HarrasMode,
		TemplarMenu,
		TemplarMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!TemplarMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		TemplarMenu.TargetMenuTree.DrawTargetParticleColor,
		TemplarMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(TemplarMenu.ComboMode, owner)
	SwitchPanel.Draw(TemplarMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!TemplarMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (TemplarMenu.ComboKey.is_pressed || TemplarMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!TemplarMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(TemplarMenu.ComboMode)
		SwitchPanel.MouseLeftDown(TemplarMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((TemplarMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (TemplarMenu.HarassKey.assigned_key as VMouseKeys) === key))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!TemplarMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((TemplarMenu.ComboKey.assigned_key) === key || (TemplarMenu.HarassKey.assigned_key) === key)
}

function onCreated(owner: UnitX) {
	if (TemplarMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, TemplarMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new TemplarOrbWalk(owner, TemplarMenu))
}

function onDestoyed(owner: UnitX) {
	if (TemplarMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== TemplarMenu.npc_name)
		return
	TAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (TemplarMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
