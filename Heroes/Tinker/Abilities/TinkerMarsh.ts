import { OrbWalker } from "immortal-core/Imports";
import { AIOUsable } from "_ICore_/Index";
export { HERO_DATA } from "../../data"



export class TinkerMarshs extends AIOUsable  {


    public UseAbility(aoe: boolean) {
        const position = this.Owner.Position.Extend2D(this.Owner.Position, 50)
        if (!this.Owner.UseAbility(this.Ability, { intent: position }))
            return false
        const delay = this.Ability.CastDelay
        HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
        this.Ability.ActionSleeper.Sleep(delay + 0.5)
        OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
        return true
    }    
}
