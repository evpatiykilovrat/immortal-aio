import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/TinkerMenu";

export const TinkerMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_tinker",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_blink",
		"item_lotus_orb",
		"item_shivas_guard",
		"item_gungir",
		"item_rod_of_atos",
		"item_sheepstick",
		"item_blade_mail",
        "item_black_king_bar",
        "item_ethereal_blade",
        "item_rod_of_atos",
        "item_veil_of_discord",
        "item_dagon",
        "item_glimmer_cape",
        "item_ghost",
        "item_soul_ring",
	],

	KillstealItems: [
		"item_shivas_guard",
		"item_ethereal_blade",
		"item_dagon",
	],
	
	DeffendSpell: [
		"tinker_heat_seeking_missile",
		"tinker_march_of_the_machines",
		"tinker_defense_matrix",
	],

	KillstealAbillities: [
		"tinker_laser",
		"tinker_heat_seeking_missile",
	],

	Abilities: [
		"tinker_laser",
		"tinker_heat_seeking_missile",
		"tinker_march_of_the_machines",
		"tinker_defense_matrix",
		"tinker_rearm",
	],
})
