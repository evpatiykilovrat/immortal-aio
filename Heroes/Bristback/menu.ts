import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const BristbackMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_bristleback",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_shivas_guard",
		"item_rod_of_atos",
		"item_sheepstick",
		"item_gungir",
		"item_blade_mail",
		"item_medallion_of_courage",
		"item_solar_crest",
		"item_abyssal_blade",
		"item_orchid",
		"item_bloodthorn",
	],
	Abilities: [
		"bristleback_viscous_nasal_goo",
		"bristleback_quill_spray",
		"bristleback_hairball",
	],
	LinkenBreak: ["bristleback_viscous_nasal_goo"],
})
