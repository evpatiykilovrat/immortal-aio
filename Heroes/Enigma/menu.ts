import { PathX } from "immortal-core/Imports";
import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const EnigmaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_enigma",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_black_king_bar",
		"item_ghost",
		"item_blink",
		"item_refresher",
		"item_refresher_shard",
		"item_shivas_guard",
	],
	Abilities: [
		"enigma_malefice",
		"enigma_midnight_pulse",
		"enigma_black_hole",
	],
	HitMenu: { default: 1, name: "Black Hole", imageName: PathX.DOTAAbilities("enigma_black_hole")},
})
