import { ActiveAbility, AssassinPsionicTrap, OrbWalker, SelfTrap, UnitsX } from "immortal-core/Imports";
import { ArrayExtensions } from "wrapper/Imports";
import { AIODebuff, HERO_DATA, TargetManager } from "_ICore_/Index";

export class TrapExplode extends AIODebuff {
	constructor(public Ability: ActiveAbility & SelfTrap) {
		super(Ability)
	}

	public ShouldCast() {
		const target = TargetManager.Target!
		const modifier = target.GetBuffByName(this.AIODebuff.DebuffModifierName)

		if ((modifier?.RemainingTime ?? 0) > 2.5)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable || target.IsRooted || target.IsStunned || target.IsHexed)
			return false

		if (TargetManager.TargetSleeper.Sleeping)
			return false

		if (this.Ability.IsFullyCharged)
			return true

		if (this.Owner.Distance(target) > (this.Ability.Radius * 0.6) && target.GetAngle(this.Owner.Position) > 2)
			return true

		return false
	}

	public UseAbility(aoe: boolean) {
		const closestTrap = ArrayExtensions.orderBy(UnitsX.All.filter(x => x instanceof AssassinPsionicTrap
			&& x.IsAlive
			&& x.IsControllable
			&& x.Distance(TargetManager.Target!) <= 400), x => x.Distance(TargetManager.Target!),
		)[0]

		if (closestTrap !== undefined && closestTrap === this.Owner) {
			const explodeAbility = this.Owner.Abilities.find(x => x instanceof SelfTrap) as ActiveAbility
			if (explodeAbility?.CanBeCasted() === true && explodeAbility.UseAbility(TargetManager.Target!)) {
				HERO_DATA.ComboSleeper.Sleep(0.1, this.Owner.Handle)
				OrbWalker.Sleeper.Sleep(0.1, this.Owner.Handle)
				TargetManager.TargetSleeper.Sleep(0.3)
				return true
			}
		}
		if (!super.UseAbility(aoe))
			return false
		TargetManager.TargetSleeper.Sleep(0.3)
		return true
	}
}
