import { ActiveAbility, AssassinPsionicTrap, UnitsX } from "immortal-core/Imports";
import { AIODebuff, TargetManager } from "_ICore_/Index";

export class TemplarPsionicTrap extends AIODebuff {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (TargetManager.Target!.IsTeleporting
			|| this.Owner.HasBuffByName("modifier_templar_assassin_meld")
			|| this.Owner.BaseOwner.InvisibilityLevel > 0
		) return false

		if (UnitsX.All.some(x => x instanceof AssassinPsionicTrap
			&& x.IsAlive
			&& !x.IsEnemy()
			&& x.Distance(TargetManager.Target!) < this.Ability.Radius))
			return false

		return true
	}
}
