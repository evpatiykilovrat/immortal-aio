import { EventsX, GameX, HeroX } from "immortal-core/Imports";
import { Attributes, Menu } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const NevermoreMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_nevermore",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_abyssal_blade",
		"item_veil_of_discord",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_ethereal_blade",
		"item_sheepstick",
		"item_blink",
		"item_cyclone",
		"item_spider_legs",
		"item_hurricane_pike",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_refresher",
		"item_refresher_shard",
		"item_mask_of_madness",
	],
	Abilities: [
		"nevermore_shadowraze1",
		"nevermore_shadowraze2",
		"nevermore_shadowraze3",
		"nevermore_requiem",
	],
	FailSafeDefault: false,
})

export const APCHeroes = NevermoreMenu.ComboSettings.AddImageSelector("AUTO_PAUSE_1", [])
export const APHHeroes = NevermoreMenu.HarassSettings.AddImageSelector("AUTO_PAUSE_2", [])
export const StateMouseRaze = NevermoreMenu.menuBase.AddToggle("Shadow Raze (Direction)", true, "Use shadow raze in mouse position")

EventsX.on("UnitCreated", async hero => {
	if (!(hero instanceof HeroX) || !hero.IsEnemy() || !GameX.IsInGame)
		return
	if (!APCHeroes.values.includes(hero.Name)) {
		APCHeroes.values.push(hero.Name)
		await APCHeroes.Update()
	}

	if (!APHHeroes.values.includes(hero.Name)) {
		APHHeroes.values.push(hero.Name)
		await APHHeroes.Update()
	}
})

EventsX.on("GameEnded", () => {
	APCHeroes.values = []
	APHHeroes.values = []
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["AUTO_PAUSE_1", "Авто пауза"],
	["AUTO_PAUSE_2", "Авто пауза"],
	["Shadow Raze (Direction)", "Shadow Raze (Направленые)"],
	["Use shadow raze in mouse position", "Использовать койлы в направление мыши"],
]))

Menu.Localization.AddLocalizationUnit("english", new Map([
	["AUTO_PAUSE_1", "Auto pause"],
	["AUTO_PAUSE_2", "Auto pause"],
]))
