import { AbilityX, AbyssalBlade, AntiMageBlink, BlackKingBar, Bloodthorn, Counterspell, DiffusalBlade, EventsX, ManaVoid, MantaStyle, MedallionOfCourage, Nullifier, OrchidMalevolence, ScytheOfVyse, SolarCrest, UnitX } from "immortal-core/Imports";
import { ModeCombo } from "../../Menu/Base";
import { AbilityHelper, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIONullifier, AIOShield, HERO_DATA, ShieldBreakerMap, TargetManager } from "../../_ICore_/Index";
import { AIOAntiMageBlink } from "./Abilities/AntiMageBlink";
import { AntiMageCounterspell } from "./Abilities/Counterspell";
import { AntiMageManaVoid } from "./Abilities/ManaVoid";

const data = {
	manta: undefined as Nullable<AIOBuff>,
	bkb: undefined as Nullable<AIOShield>,
	hex: undefined as Nullable<AIODisable>,
	medal: undefined as Nullable<AIODebuff>,
	orchid: undefined as Nullable<AIODisable>,
	diffusal: undefined as Nullable<AIODebuff>,
	abyssal: undefined as Nullable<AIODisable>,
	blink: undefined as Nullable<AIOAntiMageBlink>,
	nullifier: undefined as Nullable<AIONullifier>,
	manavoid: undefined as Nullable<AntiMageManaVoid>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	counterspell: undefined as Nullable<AntiMageCounterspell>,
}

export const AAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof MantaStyle)
		data.manta = new AIOBuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof AntiMageBlink)
		data.blink = new AIOAntiMageBlink(abil)
	if (abil instanceof ManaVoid)
		data.manavoid = new AntiMageManaVoid(abil)
	if (abil instanceof Counterspell)
		data.counterspell = new AntiMageCounterspell(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof MedallionOfCourage || abil instanceof SolarCrest)
		data.medal = new AIODebuff(abil)
}

export const AntiMageMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.manavoid))
		return true

	if (helper.CanBeCasted(data.manavoid, false) && !helper.CanBeCasted(data.manavoid))
		if (helper.UseAbility(data.blink, 200, 0))
			return true

	if (helper.UseAbility(data.blink))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.manta, unit.GetAttackRange()))
		return true

	if (helper.UseAbility(data.counterspell))
		return true

	if (helper.UseAbility(data.diffusal))
		return true

	if (!unit.IsSpellShieldProtected && helper.UseAbility(data.bkb, 500))
		return true

	if (helper.UseAbility(data.medal))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
