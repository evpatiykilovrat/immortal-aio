import { EventsX } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";

EventsX.on("GameEnded", () => {
	HERO_DATA.Dispose()
	TargetManager.Dispose()
})
