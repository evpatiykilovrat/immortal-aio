import { AbilityX, EventsX, UnitX } from "immortal-core/Imports"
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports"
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index"
import { TimberMenu } from "./menu"
import { TAbilitiesCreated, TimberMode } from "./Timber"

HeroService.RegisterHeroModule(TimberMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(TimberMenu, oldState)
TargetManager.DestroyParticleDrawTarget(TimberMenu)

function onTick(unit: UnitX) {
	if (!TimberMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = TimberMenu.TargetMenuTree
	TargetManager.TargetLocked = TimberMenu.ComboKey.is_pressed || TimberMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		TimberMode,
		TimberMenu.ComboMode,
		TimberMenu,
		TimberMenu.ComboKey,
	)

	ComboMode(
		unit,
		TimberMode,
		TimberMenu.HarrasMode,
		TimberMenu,
		TimberMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!TimberMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		TimberMenu.TargetMenuTree.DrawTargetParticleColor,
		TimberMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(TimberMenu.ComboMode, owner)
	SwitchPanel.Draw(TimberMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!TimberMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (TimberMenu.ComboKey.is_pressed || TimberMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!TimberMenu.BaseState.value || !TargetManager.HasValidTarget)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(TimberMenu.ComboMode)
		SwitchPanel.MouseLeftDown(TimberMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((TimberMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (TimberMenu.HarassKey.assigned_key as VMouseKeys) === key))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!TimberMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL) || !TargetManager.HasValidTarget)
		return true
	return !((TimberMenu.ComboKey.assigned_key as VKeys) === key || (TimberMenu.HarassKey.assigned_key as VKeys) === key)
}

function onCreated(owner: UnitX) {
	if (TimberMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, TimberMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, TimberMenu))
}

function onDestoyed(owner: UnitX) {
	if (TimberMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== TimberMenu.npc_name)
		return
	TAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (TimberMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	TargetManager.RemoveTargetParticle()
})
