import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../../Menu/Base";

export const WindrangerMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_windrunner",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_blink",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_sheepstick",
		"item_diffusal_blade",
	],
	Abilities: [
		"windrunner_shackleshot",
		"windrunner_powershot",
		"windrunner_windrun",
		"windrunner_focusfire",
	],
	LinkenBreak: ["windrunner_shackleshot"],
})
