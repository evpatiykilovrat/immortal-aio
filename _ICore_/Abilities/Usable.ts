import { ActiveAbility, UnitX } from "immortal-core/Imports";
import { TargetManager } from "../Manager/Target";

export abstract class AIOUsable {

	public Owner!: UnitX
	public Ability!: ActiveAbility

	constructor(ability: ActiveAbility) {
		this.Ability = ability
		this.Owner = ability.Owner
	}

	public CanBeCasted(channelingCheck: boolean) {
		return this.Ability.CanBeCasted(channelingCheck)
	}

	public CanHit() {
		return this.Ability.CanHit(TargetManager.Target!)
	}

	public ShouldConditionCast(target: UnitX, args: Nullable<AIOUsable>[]) {
		return true
	}

	public abstract ShouldCast(): boolean
	public abstract ForceUseAbility(): boolean
	public abstract UseAbility(aoe: boolean, ...args: any[]): boolean

	protected ChainStun(target: UnitX, invulnerability: boolean) {
		const immobile = invulnerability ? target.GetInvulnerabilityDuration : target.GetImmobilityDuration
		if (immobile <= 0)
			return false
		let hitTime = this.Ability.GetHitTime(target)
		if (target.IsInvulnerable) {
			hitTime -= 0.1
		}
		return hitTime > immobile
	}
}
