import { AbilityX, AbyssalBlade, ArenaOfBlood, ArmletOfMordiggian, BlackKingBar, BladeMail, BlinkDagger, Bloodthorn, EulsScepterOfDivinity, EventsX, GodsRebuke, LotusOrb, MedallionOfCourage, MinotaurHorn, Nullifier, OrchidMalevolence, ScytheOfVyse, SolarCrest, SpearOfMars, UnitX, WindWaker } from "immortal-core/Imports";
import { HERO_DATA } from "_ICore_/data";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIOEulsScepter, AIONullifier, AIOShield, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { MarsArenaOfBlood } from "./Abilities/ArenaOfBlood";
import { MarsGodsRebuke } from "./Abilities/GodsRebuke";
import { MarsSpearOfMars } from "./Abilities/SpearOfMars";

const data = {
	horn: undefined as Nullable<AIOShield>,
	euls: undefined as Nullable<AIOEulsScepter>,
	blink: undefined as Nullable<AIOBlink>,
	bkb: undefined as Nullable<AIOShield>,
	hex: undefined as Nullable<AIODisable>,
	spear: undefined as Nullable<MarsSpearOfMars>,
	rebuke: undefined as Nullable<MarsGodsRebuke>,
	arena: undefined as Nullable<MarsArenaOfBlood>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	nullifier: undefined as Nullable<AIONullifier>,
	solar: undefined as Nullable<AIODebuff>,
	medallion: undefined as Nullable<AIODebuff>,
	bladeMail: undefined as Nullable<AIOShield>,
	armlet: undefined as Nullable<AIOBuff>,
	abyssal: undefined as Nullable<AIODisable>,
	lotus: undefined as Nullable<AIOShield>,
}

export const MAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof MinotaurHorn)
		data.horn = new AIOShield(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof EulsScepterOfDivinity || abil instanceof WindWaker)
		data.euls = new AIOEulsScepter(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof GodsRebuke)
		data.rebuke = new MarsGodsRebuke(abil)
	if (abil instanceof ArenaOfBlood)
		data.arena = new MarsArenaOfBlood(abil)
	if (abil instanceof SpearOfMars)
		data.spear = new MarsSpearOfMars(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof ArmletOfMordiggian)
		data.armlet = new AIOBuff(abil)
	if (abil instanceof MedallionOfCourage)
		data.medallion = new AIODebuff(abil)
	if (abil instanceof SolarCrest)
		data.solar = new AIODebuff(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
	if (abil instanceof LotusOrb)
		data.lotus = new AIOShield(abil)
}

export const MarsMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (!unit.IsMagicImmune)
		if (helper.UseAbility(data.horn, 400))
			return true

	if (!unit.IsMagicImmune)
		if (helper.UseAbility(data.bkb, 400))
			return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.solar))
		return true

	if (helper.UseAbility(data.medallion))
		return true

	if (helper.UseAbility(data.blink, data.euls?.Ability.CastRange ?? 400,  0))
		return true

	if (helper.UseAbility(data.armlet, 400))
		return true

	if (!helper.CanBeCasted(data.arena) && helper.CanBeCasted(data.spear)
		&& !TargetManager.Target!.HasBuffByName("modifier_mars_arena_of_blood_leash")) {
		if (helper.UseAbility(data.euls))
			return true
	}

	if (helper.UseAbility(data.arena)) {
		HERO_DATA.ComboSleeper.Sleep(0.1, unit.Handle)
		return true
	}

	if ((data.arena?.Ability.ActionSleeper.Sleeping === true)
		|| TargetManager.Target!.HasBuffByName("modifier_mars_arena_of_blood_leash")) {
		if (helper.UseAbility(data.rebuke))
			return true

		if (helper.UseAbility(data.spear))
			return true

	} else {

		if (helper.UseAbility(data.spear))
			return true

		if (helper.UseAbility(data.rebuke))
			return true

	}

	if (helper.UseAbility(data.lotus, 400))
		return true

	if (helper.UseAbility(data.bladeMail, 400))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
