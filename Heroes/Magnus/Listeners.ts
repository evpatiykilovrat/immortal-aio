import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, FailSafeManager, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, OrbWallkerManager, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { MAbilitiesCreated, MagnusMode, OrbWalkMagnus, SkewerMode } from "./Magnus";
import { AllySkewerKey, MagnusMenu } from "./Menu/Index";

HeroService.RegisterHeroModule(MagnusMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(MagnusMenu, oldState)
TargetManager.DestroyParticleDrawTarget(MagnusMenu)

function onTick(unit: UnitX) {
	if (!MagnusMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = MagnusMenu.TargetMenuTree
	TargetManager.TargetLocked = MagnusMenu.ComboKey.is_pressed || MagnusMenu.HarassKey.is_pressed || AllySkewerKey.is_pressed

	if (AllySkewerKey.is_pressed) {
		FailSafeManager.Update(unit)
		if (SkewerMode(unit))
			return
		OrbWallkerManager.Update()
		return
	}

	ComboMode(
		unit,
		MagnusMode,
		MagnusMenu.ComboMode,
		MagnusMenu,
		MagnusMenu.ComboKey,
	)

	ComboMode(
		unit,
		MagnusMode,
		MagnusMenu.HarrasMode,
		MagnusMenu,
		MagnusMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!MagnusMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		MagnusMenu.TargetMenuTree.DrawTargetParticleColor,
		MagnusMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(MagnusMenu.ComboMode, owner)
	SwitchPanel.Draw(MagnusMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!MagnusMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (MagnusMenu.ComboKey.is_pressed || MagnusMenu.HarassKey.is_pressed || AllySkewerKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!MagnusMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((MagnusMenu.ComboKey.assigned_key as VKeys) === key
		|| (MagnusMenu.HarassKey.assigned_key as VKeys) === key
		|| (AllySkewerKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!MagnusMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(MagnusMenu.ComboMode)
		SwitchPanel.MouseLeftDown(MagnusMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (MagnusMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (MagnusMenu.HarassKey.assigned_key as VMouseKeys) === key
		|| (AllySkewerKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (MagnusMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, MagnusMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new OrbWalkMagnus(owner, MagnusMenu))
}

function onDestoyed(owner: UnitX) {
	if (MagnusMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== MagnusMenu.npc_name)
		return
	MAbilitiesCreated(abil, MagnusMenu)
}

EventsX.on("removeControllable", owner => {
	if (MagnusMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
