import { ActiveAbility } from "immortal-core/Imports";
import { BaseMenu } from "Menu/Base";
import { Menu } from "wrapper/Imports";
import { AIOShield } from "_ICore_/Index";

export class UrsaEnrage extends AIOShield {
	constructor(ability: ActiveAbility, private menu: BaseMenu, private State: {ComboStateHP: Menu.Slider; HarassStateHP: Menu.Slider; }) {
		super(ability)
	}

	public get StateHP() {
		return this.menu.ComboKey.is_pressed
			? (this.State.ComboStateHP?.value ?? 1)
			: (this.State.HarassStateHP?.value ?? 1)
	}

	public CanBeCasted(channelingCheck: boolean) {
		if (!super.CanBeCasted(channelingCheck))
			return false

		if (!this.Owner.CanAttack())
			return false

		if (this.Owner.HealthPercentage > this.StateHP)
			return false

		return true
	}
}
