import { EventsX, GameX, HeroX } from "immortal-core/Imports"
import { LocalPlayer, Menu } from "wrapper/Imports"
import { ModeCombo } from "../../../Menu/Base"
import { MagnusMenu } from "./Base"

const SkewerTree = MagnusMenu.menuBase.AddNode("Blink + Skewer")
export const AllySkewerKey = SkewerTree.AddKeybind("Key")
export const AllySkewerSelector = SkewerTree.AddImageSelector("SKEWER_TO_ALLY", [])

export const MoveMenu = SkewerTree.AddToggle("Move", true)
MoveMenu.IsHidden = true
export const AttackMenu = SkewerTree.AddToggle("Attack")
AttackMenu.IsHidden = true
export const IgnoreInvis = SkewerTree.AddToggle("Ignore Invis", true)
IgnoreInvis.IsHidden = true
const Selector = SkewerTree.AddImageSelector("SKEWER_TO_ALLY", [])

export const BlinkSkewerMode: ModeCombo = {
	Items: Selector,
	Abilities: Selector,
	Move: MoveMenu,
	Attack: AttackMenu,
	IgnoreInvis,
	BaseState: MagnusMenu.BaseState,
}

EventsX.on("UnitCreated", async hero => {
	if (!(hero instanceof HeroX) || hero.IsEnemy() || !GameX.IsInGame || LocalPlayer?.Hero?.Index === hero.Handle)
		return
	if (!AllySkewerSelector.values.includes(hero.Name)) {
		AllySkewerSelector.values.push(hero.Name)
		await AllySkewerSelector.Update()
	}
})

EventsX.on("GameEnded", () => {
	AllySkewerSelector.values = []
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Blink + Skewer", "Блинк + Skewer"],
]))
