import { OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { AIOUsable } from "./Usable";

export class AIOUntargetable extends AIOUsable {
	public ShouldCast(): boolean {
		return true
	}
	public ForceUseAbility(): boolean {
		return this.UseAbility(true)
	}
	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		if (!this.Owner.UseAbility(this.Ability))
			return false
		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
