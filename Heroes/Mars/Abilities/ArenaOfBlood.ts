import { ActiveAbility, HitChanceX, OrbWalker } from "immortal-core/Imports";
import { AIOAoe, HERO_DATA, TargetManager } from "_ICore_/Index";

export class MarsArenaOfBlood extends AIOAoe {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public UseAbility(aoe: boolean) {
		const castRange = this.Ability.CastRange
		for (var i = 50; i <= castRange; i += 50) {
			const input = this.Ability.GetPredictionInput(TargetManager.Target!, TargetManager.EnemyHeroes)
			input.CastRange = i
			input.Radius -= 125
			const output = this.Ability.GetPredictionOutput(input);
			if (output.HitChance < HitChanceX.Low)
				continue
			if (!this.Ability.UseAbility(TargetManager.Target!, output.CastPosition))
				continue
			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}
		return false
	}
}
