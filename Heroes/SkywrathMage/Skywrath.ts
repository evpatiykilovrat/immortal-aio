import { AbilityX, AncientSeal, ArcaneBolt, BlinkDagger, Bloodthorn, ConcussiveShot, Dagon, EtherealBlade, EventsX, Gleipnir, MysticFlare, Nullifier, OrchidMalevolence, RodOfAtos, ScytheOfVyse, UnitX, VeilOfDiscord } from "immortal-core/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIOEtherealBlade, AIOForceStaff, AIONuke, AIONullifier, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { HexSkywrath } from "./Abilities/SkywrathHex";
import { SkywrathMysticFlare } from "./Abilities/SkywrathMysticFlare";

export const data = {
	bolt: undefined as Nullable<AIONuke>,
	dagon: undefined as Nullable<AIONuke>,
	seal: undefined as Nullable<AIODebuff>,
	atos: undefined as Nullable<AIODisable>,
	veil: undefined as Nullable<AIODebuff>,
	blink: undefined as Nullable<AIOBlink>,
	hex: undefined as Nullable<HexSkywrath>,
	orchid: undefined as Nullable<AIODisable>,
	force: undefined as Nullable<AIOForceStaff>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	concussive: undefined as Nullable<AIODebuff>,
	nullifier: undefined as Nullable<AIONullifier>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
	flare: undefined as Nullable<SkywrathMysticFlare>,
}

export const SAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof ArcaneBolt)
		data.bolt = new AIONuke(abil)
	if (abil instanceof ConcussiveShot)
		data.concussive = new AIODebuff(abil)
	if (abil instanceof AncientSeal)
		data.seal = new AIODebuff(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof MysticFlare)
		data.flare = new SkywrathMysticFlare(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new HexSkywrath(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
}

export const SkywrathMode = (unit: UnitX, menu: ModeCombo): boolean => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const abilityHelper = new AbilityHelper(unit, menu)

	if (abilityHelper.UseAbility(data.atos))
		return true

	if (abilityHelper.UseAbility(data.hex))
		return true

	if (abilityHelper.UseAbility(data.orchid))
		return true

	if (abilityHelper.UseAbility(data.bloodthorn))
		return true

	if (abilityHelper.UseAbility(data.nullifier))
		return true

	if (abilityHelper.UseAbility(data.seal))
		return true

	if (abilityHelper.UseAbility(data.ethereal))
		return true

	if (abilityHelper.UseAbility(data.dagon))
		return true

	if (abilityHelper.UseAbility(data.concussive)) {
		data.concussive?.Ability.ActionSleeper.ExtendSleep(0.15)
		return true
	}

	if (abilityHelper.UseAbility(data.veil))
		return true

	if (abilityHelper.UseAbility(data.blink, 850, 600))
		return true

	if (abilityHelper.UseAbility(data.force, 850, 600))
		return true

	if (abilityHelper.UseAbilityIfNone(data.flare, data.atos))
		return true

	if (abilityHelper.UseAbility(data.bolt))
		return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
