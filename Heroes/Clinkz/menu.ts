import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const ClinkzMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_clinkz",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_sheepstick",
		"item_orchid",
		"item_nullifier",
		"item_bloodthorn",
		"item_orchid",
		"item_gungir",
		"item_solar_crest",
		"item_medallion_of_courage",
		"item_diffusal_blade",
		"item_rod_of_atos",
	],
	Abilities: [
		"clinkz_strafe",
		"clinkz_searing_arrows",
		"clinkz_burning_army",
	],
})
