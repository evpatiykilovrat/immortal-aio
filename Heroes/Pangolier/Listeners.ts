import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { PangolierMenu } from "./menu";
import { PAbilitiesCreated, PangoExecuteOrderer, PangoMode, PangoOrbWalker } from "./Pangolier";

HeroService.RegisterHeroModule(PangolierMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(PangolierMenu, oldState)
TargetManager.DestroyParticleDrawTarget(PangolierMenu)

function onTick(unit: UnitX) {
	if (!PangolierMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = PangolierMenu.TargetMenuTree
	TargetManager.TargetLocked = PangolierMenu.ComboKey.is_pressed || PangolierMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		PangoMode,
		PangolierMenu.ComboMode,
		PangolierMenu,
		PangolierMenu.ComboKey,
	)

	ComboMode(
		unit,
		PangoMode,
		PangolierMenu.HarrasMode,
		PangolierMenu,
		PangolierMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!PangolierMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		PangolierMenu.TargetMenuTree.DrawTargetParticleColor,
		PangolierMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(PangolierMenu.ComboMode, owner)
	SwitchPanel.Draw(PangolierMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!PangolierMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	PangoExecuteOrderer(order)

	if (TargetManager.HasValidTarget && (PangolierMenu.ComboKey.is_pressed || PangolierMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PangolierMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((PangolierMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!PangolierMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(PangolierMenu.ComboMode)
		SwitchPanel.MouseLeftDown(PangolierMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (PangolierMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (PangolierMenu.HarassKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (PangolierMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, PangolierMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new PangoOrbWalker(owner, PangolierMenu))
}

function onDestoyed(owner: UnitX) {
	if (PangolierMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== PangolierMenu.npc_name)
		return
	PAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (PangolierMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
