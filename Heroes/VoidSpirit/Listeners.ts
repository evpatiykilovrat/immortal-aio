import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { VoidSpiritMenu } from "./menu";
import { VAbilitiesCreated, VoidSpiritMode } from "./VoidSpirit";

HeroService.RegisterHeroModule(VoidSpiritMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(VoidSpiritMenu, oldState)
TargetManager.DestroyParticleDrawTarget(VoidSpiritMenu)

function onTick(unit: UnitX) {
	if (!VoidSpiritMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = VoidSpiritMenu.TargetMenuTree
	TargetManager.TargetLocked = VoidSpiritMenu.ComboKey.is_pressed || VoidSpiritMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		VoidSpiritMode,
		VoidSpiritMenu.ComboMode,
		VoidSpiritMenu,
		VoidSpiritMenu.ComboKey,
	)

	ComboMode(
		unit,
		VoidSpiritMode,
		VoidSpiritMenu.HarrasMode,
		VoidSpiritMenu,
		VoidSpiritMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!VoidSpiritMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		VoidSpiritMenu.TargetMenuTree.DrawTargetParticleColor,
		VoidSpiritMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(VoidSpiritMenu.ComboMode, owner)
	SwitchPanel.Draw(VoidSpiritMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!VoidSpiritMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (VoidSpiritMenu.ComboKey.is_pressed || VoidSpiritMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!VoidSpiritMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(VoidSpiritMenu.ComboMode)
		SwitchPanel.MouseLeftDown(VoidSpiritMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((VoidSpiritMenu.ComboKey.assigned_key as VMouseKeys) === key
		|| (VoidSpiritMenu.HarassKey.assigned_key as VMouseKeys) === key))
		return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!VoidSpiritMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((VoidSpiritMenu.ComboKey.assigned_key as VKeys) === key || (VoidSpiritMenu.HarassKey.assigned_key as VKeys) === key)
}

function onCreated(owner: UnitX) {
	if (VoidSpiritMenu.npc_name !== owner.Name)
		return

	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, VoidSpiritMenu))

	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, VoidSpiritMenu.Shields))
}

function onDestoyed(owner: UnitX) {
	if (VoidSpiritMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== VoidSpiritMenu.npc_name)
		return
	VAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (VoidSpiritMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
