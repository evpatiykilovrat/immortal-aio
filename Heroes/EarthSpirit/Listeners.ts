import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, FailSafeManager, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, OrbWallkerManager, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { EAbilitiesCreated, EarthSpiritMode, RollSmashCombo } from "./EarthSpirit";
import { EarthSpiritMenu, SmashMenu } from "./Menu/Index";

HeroService.RegisterHeroModule(EarthSpiritMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(EarthSpiritMenu, oldState)
TargetManager.DestroyParticleDrawTarget(EarthSpiritMenu)

function onTick(unit: UnitX) {
	if (!EarthSpiritMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = EarthSpiritMenu.TargetMenuTree
	TargetManager.TargetLocked = EarthSpiritMenu.ComboKey.is_pressed || EarthSpiritMenu.HarassKey.is_pressed || SmashMenu.Key.is_pressed

	if (SmashMenu.Key.is_pressed) {
		FailSafeManager.Update(unit)
		RollSmashCombo(unit, SmashMenu)
		OrbWallkerManager.Update()
		return
	}

	ComboMode(
		unit,
		EarthSpiritMode,
		EarthSpiritMenu.ComboMode,
		EarthSpiritMenu,
		EarthSpiritMenu.ComboKey,
	)

	ComboMode(
		unit,
		EarthSpiritMode,
		EarthSpiritMenu.HarrasMode,
		EarthSpiritMenu,
		EarthSpiritMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!EarthSpiritMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		EarthSpiritMenu.TargetMenuTree.DrawTargetParticleColor,
		EarthSpiritMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(EarthSpiritMenu.ComboMode, owner)
	SwitchPanel.Draw(EarthSpiritMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!EarthSpiritMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (EarthSpiritMenu.ComboKey.is_pressed || EarthSpiritMenu.HarassKey.is_pressed || SmashMenu.Key.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!EarthSpiritMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((EarthSpiritMenu.ComboKey.assigned_key as VKeys) === key)
		|| !((EarthSpiritMenu.HarassKey.assigned_key as VKeys) === key)
		|| !((SmashMenu.Key.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!EarthSpiritMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(EarthSpiritMenu.ComboMode)
		SwitchPanel.MouseLeftDown(EarthSpiritMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((EarthSpiritMenu.ComboKey.assigned_key as VMouseKeys) === key)
		|| (EarthSpiritMenu.HarassKey.assigned_key as VMouseKeys) === key
		|| (SmashMenu.Key.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (EarthSpiritMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, EarthSpiritMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, EarthSpiritMenu))
}

function onDestoyed(owner: UnitX) {
	if (EarthSpiritMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== EarthSpiritMenu.npc_name)
		return
	EAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (EarthSpiritMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
