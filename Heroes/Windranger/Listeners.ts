import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { WindrangerMenu } from "./Menu/Base";
import { WAbilitiesCreated, WindrangerMode, WindrangerOrbWalk } from "./Windranger";

HeroService.RegisterHeroModule(WindrangerMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(WindrangerMenu, oldState)
TargetManager.DestroyParticleDrawTarget(WindrangerMenu)

function onTick(unit: UnitX) {
	if (!WindrangerMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = WindrangerMenu.TargetMenuTree
	TargetManager.TargetLocked = WindrangerMenu.ComboKey.is_pressed || WindrangerMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		WindrangerMode,
		WindrangerMenu.ComboMode,
		WindrangerMenu,
		WindrangerMenu.ComboKey,
	)

	ComboMode(
		unit,
		WindrangerMode,
		WindrangerMenu.HarrasMode,
		WindrangerMenu,
		WindrangerMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!WindrangerMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		WindrangerMenu.TargetMenuTree.DrawTargetParticleColor,
		WindrangerMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(WindrangerMenu.ComboMode, owner)
	SwitchPanel.Draw(WindrangerMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!WindrangerMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (WindrangerMenu.ComboKey.is_pressed || WindrangerMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!WindrangerMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((WindrangerMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!WindrangerMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(WindrangerMenu.ComboMode)
		SwitchPanel.MouseLeftDown(WindrangerMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (WindrangerMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (WindrangerMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, WindrangerMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new WindrangerOrbWalk(owner, WindrangerMenu))
}

function onDestoyed(owner: UnitX) {
	if (WindrangerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== WindrangerMenu.npc_name)
		return
	WAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (WindrangerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
