import { AbilitiesX, AbilityX, ActiveAbility, AreaOfEffectAbility, CircleAbility, ConeAbility, EventsX, HitChanceX, isIBlink, isIHasRadius, LineAbility, OrbWalker, PCircle, Polygon, PRectangle, PredictionAbility, PTrapezoid, TickSleeperX, UnitX } from "immortal-core/Imports"
import { dotaunitorder_t, EventsSDK, GameState, Vector3 } from "wrapper/Imports"
import { HERO_DATA } from "../data"
import { TargetManager } from "./Target"

export class FailSafeManager {

	public static IsTread = false
	public static Sleeper = new TickSleeperX()
	public static abilityPositions = new Map<number, Vector3>()
	public static abilityTimings = new Map<ActiveAbility, number>()

	public static Update(unit: UnitX) {
		if (this.IsTread)
			return

		const abilities = unit.Abilities.filter(x => x instanceof ActiveAbility && x.Owner.Handle === unit.Handle)
		abilities.forEach(ability => {

			if (ability === undefined || !ability.IsControllable || this.IsIgnored(ability))
				return

			if (!(ability instanceof AreaOfEffectAbility) && !(ability instanceof PredictionAbility))
				return

			if (ability.BaseAbility.IsInAbilityPhase) {
				if (ability instanceof AreaOfEffectAbility) {
					if (ability.CastRange > 0) {
						this.abilityPositions.set(ability.Handle, ability.Owner.InFront(ability.CastRange))
					} else {
						this.abilityPositions.set(ability.Handle, ability.Owner.Position)
					}
				}

				this.abilityTimings.set(ability, GameState.RawGameTime + ability.CastPoint)
				this.IsTread = true

			} else {
				if (this.abilityTimings.size !== 0)
					return
				this.IsTread = false
			}
		})
	}

	public static Dispose() {
		this.IsTread = false
		this.Sleeper.ResetTimer()
		this.abilityTimings.clear()
		this.abilityPositions.clear()
	}

	private static ignoredAbilities: string[] = [
		"arc_warden_magnetic_field",
		"storm_spirit_ball_lightning",
		"leshrac_diabolic_edict",
		"shredder_timber_chain",
		"magnataur_skewer",
		"disruptor_kinetic_field",
		"nevermore_requiem",
		"disruptor_static_storm",
		"crystal_maiden_freezing_field",
		"skywrath_mage_mystic_flare",
		"phoenix_icarus_dive",
		"kunkka_torrent",
		"kunkka_ghostship",
		"elder_titan_echo_stomp_spirit",
		"elder_titan_echo_stomp",
		"bloodseeker_blood_bath",
		"phantom_lancer_doppelwalk",
		"mars_gods_rebuke",
		"beastmaster_wild_axes",
	]

	private static IsIgnored(abil: AbilityX) {
		if (this.ignoredAbilities.includes(abil.Name))
			return true
		return isIBlink(abil)
	}
}

EventsSDK.on("Tick", () => {
	if (!FailSafeManager.IsTread || FailSafeManager.Sleeper.Sleeping)
		return
	const target = TargetManager.Target
	if (target?.IsValid !== true || !target.IsVisible)
		return
	for (const item of FailSafeManager.abilityTimings) {
		const ability = item[0]
		if (!ability.IsValid || !ability.IsInAbilityPhase)
			continue
		const owner = ability.Owner
		const input = ability.GetPredictionInput(target)
		input.Delay = Math.max(item[1] - GameState.RawGameTime, 0) + ability.ActivationDelay
		const output = ability.GetPredictionOutput(input)

		if (!isIHasRadius(ability))
			continue

		const abilPos = FailSafeManager.abilityPositions.get(ability.Handle)
		if (abilPos === undefined)
			continue

		let polygon: Nullable<Polygon>

		if (ability instanceof CircleAbility || ability instanceof AreaOfEffectAbility)
			polygon = new PCircle(abilPos, ability.Radius + 50)

		if (ability instanceof ConeAbility)
			polygon = new PTrapezoid(owner.Position.Extend2D(abilPos, -ability.Radius / 2),
				owner.Position.Extend2D(abilPos, ability.Range),
				ability.Radius + 50,
				ability.EndRadius + 100)

		if (ability instanceof LineAbility)
			polygon = new PRectangle(owner.Position, owner.Position.Extend2D(abilPos, ability.Range), ability.Radius + 50)

		if (polygon === undefined)
			continue

		if (!target.IsAlive || output.HitChance === HitChanceX.Impossible || !polygon.IsInside(output.TargetPosition)) {
			FailSafeManager.Sleeper.Sleep(0.15)
			FailSafeManager.abilityTimings.delete(ability)
			FailSafeManager.abilityPositions.delete(ability.Handle)
			ability.ActionSleeper.ResetTimer()
			target.RefreshUnitState()
			ability.Owner.Stop()
			OrbWalker.Sleeper.ResetKey(ability.Owner.Handle)
			HERO_DATA.ComboSleeper.ResetKey(ability.Owner.Handle)
		}
	}
})

EventsX.on("OnExecuteOrder", order => {
	if (order.orderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION || order.ability === undefined)
		return
	const ability = AbilitiesX.All.find(x => order.ability!.Handle === x.Handle)
	if (ability === undefined || order.position === undefined)
		return
	FailSafeManager.abilityPositions.set(ability.Handle, order.position)
})

EventsX.on("GameEnded", () => {
	FailSafeManager.Dispose()
})
