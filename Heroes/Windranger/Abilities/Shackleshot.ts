import { ActiveAbility, EntityX, OrbWalker, Shackleshot, UnitX } from "immortal-core/Imports";
import { ArrayExtensions, Vector3 } from "wrapper/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { BaseMenu } from "../../../Menu/Base";
import { ShackleMenu } from "../Menu/Move";

export class WindrangerShackleshot extends AIODisable {

	private shackleTarget: Nullable<UnitX>
	private readonly shackleshot!: Shackleshot

	constructor(ability: ActiveAbility, private menu: BaseMenu, private shackle: ShackleMenu) {
		super(ability)
		this.shackleshot = ability as Shackleshot
	}

	public CanHit() {

		const target = TargetManager.Target!
		if (target.IsMagicImmune && !this.shackleshot.CanHitSpellImmuneEnemy)
			return false

		if (this.Owner.Distance(target) > this.shackleshot.CastRange + this.shackleshot.ShackleRange)
			return false

		let mainInput = this.shackleshot.GetPredictionInput(target)
		mainInput.Range = this.shackleshot.CastRange
		let mainOutput = this.shackleshot.PredictionManager.GetSimplePrediction(mainInput)
		let targetPredictedPosition = mainOutput.TargetPosition
		const ownerPosition = this.Owner.Position
		const targetPosition = target.Position

		if (!target.IsBlockingAbilities) {
			for (const unit of TargetManager.IsValidEnemyUnits) {
				if (unit.Equals(target) || unit.IsMagicImmune || unit.Distance(target) > this.shackleshot.ShackleRange)
					continue
				const input = this.shackleshot.GetPredictionInput(unit)
				input.Delay -= target.Distance(unit) / this.shackleshot.Speed
				const output = this.shackleshot.PredictionManager.GetSimplePrediction(input)
				const unitPredictedPosition = output.TargetPosition
				const unitPosition = unit.Position
				if (unitPredictedPosition.Distance(targetPredictedPosition) > this.shackleshot.ShackleRange)
					continue

				const predictedAngle = targetPredictedPosition.Subtract(ownerPosition)
					.AngleBetweenVectors(unitPredictedPosition.Subtract(targetPredictedPosition))
				const angle = targetPosition.Subtract(ownerPosition)
					.AngleBetweenVectors(unitPosition.Subtract(targetPosition))

				if ((predictedAngle < this.shackleshot.Angle && angle < this.shackleshot.Angle)
					|| predictedAngle < this.shackleshot.Angle / 2) {
					this.shackleTarget = target;
					return true
				}
			}
		}

		for (const unit of TargetManager.IsValidEnemyUnits) {
			if (unit.Equals(target) || unit.IsMagicImmune)
				continue
			if (unit.Distance(target) > this.shackleshot.ShackleRange || unit.Distance(targetPredictedPosition) < 50)
				continue
			const input = this.shackleshot.GetPredictionInput(unit)
			const output = this.shackleshot.PredictionManager.GetSimplePrediction(input)
			const unitPredictedPosition = output.TargetPosition
			const unitPosition = unit.Position
			if (unitPredictedPosition.Distance(targetPredictedPosition) > this.shackleshot.ShackleRange)
				continue
			mainInput = this.shackleshot.GetPredictionInput(target)
			mainInput.Range = this.shackleshot.CastRange
			mainInput.Delay -= target.Distance(unit) / this.shackleshot.Speed
			mainOutput = this.shackleshot.PredictionManager.GetSimplePrediction(mainInput)
			targetPredictedPosition = mainOutput.TargetPosition
			const predictedAngle = unitPredictedPosition.Subtract(ownerPosition)
				.AngleBetweenVectors(targetPredictedPosition.Subtract(unitPredictedPosition))
			const angle = unitPosition.Subtract(ownerPosition)
				.AngleBetweenVectors(targetPosition.Subtract(unitPosition))
			if ((predictedAngle < this.shackleshot.Angle && angle < this.shackleshot.Angle)
				|| predictedAngle < this.shackleshot.Angle / 2) {
				this.shackleTarget = unit
				return true
			}
		}

		if (!target.IsBlockingAbilities) {
			for (const tree of EntityX.Tree.filter(x => x.IsValid && x.IsAlive)) {
				if (tree.Distance2D(targetPredictedPosition) > this.shackleshot.ShackleRange)
					continue
				const predictedAngle = targetPredictedPosition.Subtract(ownerPosition)
					.AngleBetweenVectors(tree.Position.Subtract(targetPredictedPosition))
				const angle = targetPosition.Subtract(ownerPosition)
					.AngleBetweenVectors(tree.Position.Subtract(targetPosition))
				if ((predictedAngle < this.shackleshot.Angle && angle < this.shackleshot.Angle)
					|| predictedAngle < this.shackleshot.Angle / 2) {
					this.shackleTarget = target
					return true
				}
			}
		}

		return false
	}

	public GetBlinkPosition(target: UnitX, range: number) {
		if (target.IsMagicImmune || target.IsEthereal || target.IsInvulnerable || !target.IsVisible || target.IsBlockingAbilities)
			return new Vector3()

		const mainInput = this.shackleshot.GetPredictionInput(target)
		mainInput.Range = this.shackleshot.CastRange
		mainInput.Delay -= Math.max((this.Owner.Distance(target) - 200) / this.Ability.Speed, 0)
		const mainOutput = this.shackleshot.PredictionManager.GetSimplePrediction(mainInput)
		const targetPosition = mainOutput.TargetPosition

		for (const unit of TargetManager.IsValidEnemyHeroes) {

			if (unit.Distance(targetPosition) < 50)
				continue
			if (unit.IsMagicImmune && !this.shackleshot.CanHitSpellImmuneEnemy)
				continue;
			if (unit.Equals(target) || unit.IsInvulnerable)
				continue
			if (unit.Distance(targetPosition) > this.shackleshot.ShackleRange)
				continue

			const input = this.Ability.GetPredictionInput(unit)
			input.Delay = mainInput.Delay
			const output = this.Ability.GetPredictionOutput(input)
			const position = output.TargetPosition.Extend2D(targetPosition, output.TargetPosition.Distance2D(targetPosition) + 200)
			if (this.Owner.Distance(position) < range)
				return position
		}

		if (!target.IsBlockingAbilities) {
			for (const tree of EntityX.Tree.filter(x => x.IsValid && x.IsAlive)) {
				if (tree.Distance(targetPosition) > this.shackleshot.ShackleRange)
					continue
				const position = tree.Position.Extend2D(targetPosition, tree.Distance(targetPosition) + 200)
				if (this.Owner.Distance(position) < range)
					return position
			}
		}
		return new Vector3()
	}

	public ShouldCast() {

		const target = TargetManager.Target!

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable)
			return false

		if (target.IsStunned)
			return this.ChainStun(target, false)

		if (target.IsHexed)
			return this.ChainStun(target, false);

		if (target.IsSilenced)
			return !this.IsSilence(false) || this.ChainStun(target, false);

		if (target.IsRooted)
			return !this.IsRoot || this.ChainStun(target, false);

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true;
	}

	public UseAbility(aoe: boolean) {

		const target = this.shackleTarget ?? TargetManager.Target!
		if (!this.Owner.UseAbility(this.Ability, { intent: target }))
			return false

		const hitTime = this.Ability.GetHitTime(target) + 0.5
		const delay = this.Ability.GetCastDelay(target)
		target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		if (this.shackleTarget?.Equals(TargetManager.Target) === false)
			TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		this.shackleTarget = undefined
		return true
	}

	public GetMovePosition(windrun: boolean) {
		const state = (this.menu.ComboKey.is_pressed
			? (this.shackle.StateShackleCombo?.value ?? false)
			: (this.shackle.StateShackleHarass?.value ?? false))

		if (!state)
			return new Vector3()

		const target = TargetManager.Target!
		if (target.IsMoving && this.Owner.IdealSpeed * (windrun ? 1.5 : 0.9) < target.IdealSpeed)
			return new Vector3()

		const possiblePositions: Vector3[] = []
		const targetPosition = target.Position

		for (const unit of TargetManager.IsValidEnemyUnits) {
			if (unit.Equals(target))
				continue
			if (unit.Distance(target) > this.shackleshot.ShackleRange)
				continue
			const p1 = unit.Position.Extend2D(targetPosition, -200)
			const p2 = targetPosition.Extend2D(unit.Position, -200)
			if (this.Owner.Distance(p1) < 500 && !unit.IsBlockingAbilities)
				possiblePositions.push(p1)
			if (this.Owner.Distance(p2) < 500 && !target.IsBlockingAbilities)
				possiblePositions.push(p2)
		}

		if (!target.IsBlockingAbilities) {
			for (const tree of EntityX.Tree.filter(x => x.IsValid && x.IsAlive)) {
				if (targetPosition.Distance2D(tree.Position) > this.shackleshot.ShackleRange)
					continue
				const p1 = targetPosition.Extend2D(tree.Position, -200)
				if (this.Owner.Distance(p1) < 500)
					possiblePositions.push(p1)
			}
		}

		if (possiblePositions.length > 0)
			return ArrayExtensions.orderBy(possiblePositions.filter(x => x.IsValid), x => this.Owner.Distance(x))[0]

		return new Vector3()
	}
}
