import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const AxeMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_axe",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_manta",
		"item_force_staff",
		"item_blade_mail",
		"item_black_king_bar",
		"item_shivas_guard",
		"item_mjollnir",
		"item_meteor_hammer",
		"item_dagon_5",
	],
	Abilities: [
		"axe_berserkers_call",
		"axe_battle_hunger",
		"axe_culling_blade",
	],
})
