import { AbilityX, AbyssalBlade, ArmletOfMordiggian, BlackKingBar, BladeMail, BlinkDagger, Bloodthorn, Duel, EventsX, LotusOrb, MedallionOfCourage, Mjollnir, Nullifier, OrchidMalevolence, OverwhelmingOdds, PressTheAttack, ShivasGuard, SolarCrest, UnitX } from "immortal-core/Imports"
import { AbilityHelper, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIONullifier, AIOShield, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base"
import { AIODuel } from "./Abilities/Duel"
import { LegionBlink } from "./Abilities/LegionBlink"
import { LegionOverwhelmingOdds } from "./Abilities/OverwhelmingOdds"
import { LegionPressTheAttack } from "./Abilities/PressTheAttack"

const data = {
	duel: undefined as Nullable<AIODuel>,
	bkb: undefined as Nullable<AIOShield>,
	blink: undefined as Nullable<LegionBlink>,
	bladeMail: undefined as Nullable<AIOShield>,
	odds: undefined as Nullable<LegionOverwhelmingOdds>,
	pressAttack: undefined as Nullable<LegionPressTheAttack>,
	solar: undefined as Nullable<AIODebuff>,
	medallion: undefined as Nullable<AIODebuff>,
	mjollnir: undefined as Nullable<AIOShield>,
	armlet: undefined as Nullable<AIOBuff>,
	abyssal: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	shiva: undefined as Nullable<AIODebuff>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	nullifier: undefined as Nullable<AIONullifier>,
	lotus: undefined as Nullable<AIOShield>,
}

export const LAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Duel)
		data.duel = new AIODuel(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new LegionBlink(abil)
	if (abil instanceof OverwhelmingOdds)
		data.odds = new LegionOverwhelmingOdds(abil)
	if (abil instanceof PressTheAttack)
		data.pressAttack = new LegionPressTheAttack(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof SolarCrest)
		data.solar = new AIODebuff(abil)
	if (abil instanceof MedallionOfCourage)
		data.medallion = new AIODebuff(abil)
	if (abil instanceof Mjollnir)
		data.mjollnir = new AIOShield(abil)
	if (abil instanceof ArmletOfMordiggian)
		data.armlet = new AIOBuff(abil)
	if (abil instanceof AbyssalBlade)
		data.abyssal = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof LotusOrb)
		data.lotus = new AIOShield(abil)
}

export const LegionMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)
	const distance = unit.Distance(TargetManager.Target!)

	if (helper.CanBeCasted(data.duel, false, false) && ((distance <= 1400 && helper.CanBeCasted(data.blink)) || distance < 500)) {

		if (helper.CanBeCasted(data.pressAttack, false))
			if (helper.ForceUseAbility(data.pressAttack))
				return true

		if (helper.CanBeCasted(data.lotus, false))
			if (helper.ForceUseAbility(data.lotus))
				return true

		if (helper.CanBeCasted(data.bladeMail, false))
			if (helper.ForceUseAbility(data.bladeMail))
				return true

		if (helper.CanBeCasted(data.bkb, false))
			if (helper.ForceUseAbility(data.bkb))
				return true

		if (helper.CanBeCasted(data.mjollnir, false))
			if (helper.ForceUseAbility(data.mjollnir))
				return true

		if (helper.CanBeCasted(data.armlet, false))
			if (helper.ForceUseAbility(data.armlet))
				return true

		if (!helper.CanBeCasted(data.duel) && helper.CanBeCasted(data.blink, false))
			if (helper.ForceUseAbility(data.blink))
				return true
	}

	if (helper.UseAbility(data.abyssal))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.bkb, 500))
		return true

	if (helper.UseAbility(data.lotus, 500))
		return true

	if (helper.UseAbility(data.bladeMail, 500))
		return true

	if (helper.UseAbility(data.mjollnir, 400))
		return true

	if (helper.UseAbility(data.medallion))
		return true

	if (helper.UseAbility(data.solar))
		return true

	if (helper.UseAbility(data.pressAttack, 400))
		return true

	if (helper.UseAbility(data.armlet, 300))
		return true

	if (helper.UseAbility(data.shiva))
		return true

	if (helper.UseAbility(data.duel))
		return true

	if (helper.UseAbility(data.blink, 300, 0)) {
		HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
		return true
	}

	if (helper.CanBeCasted(data.odds) && !helper.CanBeCasted(data.duel) && !helper.CanBeCasted(data.blink)
		&& data.blink?.Ability.ActionSleeper.Sleeping !== true)
		if (helper.UseAbility(data.odds))
			return true

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
