import { HitChanceX, isIDisable, OrbWalker } from "immortal-core/Imports";
import { AIODebuff, HERO_DATA, TargetManager } from "_ICore_/Index";

export class WillowCursedCrown extends AIODebuff {
	public UseAbility(aoe: boolean): boolean {
		if (!this.Ability.UseAbility(TargetManager.Target!, TargetManager.EnemyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		if (isIDisable(this.Ability)) {
			const hitTime = this.Ability.GetHitTime(TargetManager.Target!)
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, hitTime)
		}
		HERO_DATA.ComboSleeper.Sleep(delay + 0.05, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
