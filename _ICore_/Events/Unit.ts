import { EventsX, HeroX } from "immortal-core/Imports"
import { EventsSDK, LifeState_t, npc_dota_hero_pudge, Unit } from "wrapper/Imports"
import { HERO_DATA } from "../data"
import { TargetManager } from "../Manager/Target"

EventsX.on("UnitCreated", ent => {
	if (!(ent instanceof HeroX) || ent.IsEnemy() || !ent.IsControllable)
		return
	const hero_module = HERO_DATA.HeroModules.get(ent.Name)
	if (hero_module === undefined)
		return
	hero_module.onCreated(ent)
})

EventsX.on("UnitDestroyed", ent => {
	if (!(ent instanceof HeroX) || ent.IsEnemy())
		return
	const hero_module = HERO_DATA.HeroModules.get(ent.Name)
	if (hero_module === undefined)
		return
	hero_module.onDestoyed(ent)
})

EventsSDK.on("LifeStateChanged", ent => {
	if (!(ent instanceof Unit) || ent.IsIllusion || ent.LifeState !== LifeState_t.LIFE_DEAD)
		return
	for (const hero of TargetManager.Heroes) {
		if (hero.IsIllusion || ent.Index !== hero.Handle)
			continue
		TargetManager.RemoveTargetParticle(hero)
		if (!(hero.BaseOwner instanceof npc_dota_hero_pudge))
			continue
		HERO_DATA.ParticleManager.DestroyByKey("Hook")
		HERO_DATA.ParticleManager.DestroyByKey("FatRing")
	}
})
