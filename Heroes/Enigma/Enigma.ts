import { AbilityX, BlackHole, BlackKingBar, BlinkDagger, EventsX, GhostScepter, Malefice, MidnightPulse, RefresherOrb, RefresherShard, ScytheOfVyse, ShivasGuard, TickSleeperX, UnitX } from "immortal-core/Imports";
import { AbilityHelper, AIOAoe, AIODebuff, AIODisable, AIOShield, AIOUntargetable, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { BaseMenu, ModeCombo } from "../../Menu/Base";
import { AIOBlackHole } from "./Abilities/BlackHole";
import { BlinkDaggerEnigma } from "./Abilities/BlinkDaggerEnigma";

const awaitForMalefice = new TickSleeperX()

const data = {
	bkb: undefined as Nullable<AIOShield>,
	hex: undefined as Nullable<AIODisable>,
	ghost: undefined as Nullable<AIOShield>,
	shiva: undefined as Nullable<AIODebuff>,
	pulse: undefined as Nullable<AIOAoe>,
	malefice: undefined as Nullable<AIODisable>,
	blackHole: undefined as Nullable<AIOBlackHole>,
	blink: undefined as Nullable<BlinkDaggerEnigma>,
	refresher: undefined as Nullable<AIOUntargetable>,
	refresher1: undefined as Nullable<AIOUntargetable>,
}
export const EAbilitiesCreated = (abil: AbilityX, menu: BaseMenu) => {
	if (abil instanceof MidnightPulse)
		data.pulse = new AIOAoe(abil)
	if (abil instanceof Malefice)
		data.malefice = new AIODisable(abil)
	if (abil instanceof BlackHole)
		data.blackHole = new AIOBlackHole(abil, menu)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof GhostScepter)
		data.ghost = new AIOShield(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerEnigma(abil, menu)
	if (abil instanceof RefresherOrb)
		data.refresher = new AIOUntargetable(abil)
	if (abil instanceof RefresherShard)
		data.refresher1 = new AIOUntargetable(abil)
}

export const EnigmaMode = (unit: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.CanBeCasted(data.blackHole)) {

		if (helper.CanBeCasted(data.bkb, false, false))
			if (helper.ForceUseAbility(data.bkb))
				return true

		if (helper.HasMana(data.pulse, data.blackHole) && helper.UseAbility(data.pulse))
			return true

		if (helper.HasMana(data.shiva, data.blackHole) && helper.UseAbility(data.shiva))
			return true

		if (helper.UseAbility(data.blackHole))
			return true
	}

	if (helper.CanBeCastedIfCondition(data.blink, data.blackHole)) {
		if (helper.CanBeCasted(data.bkb, false, false))
			if (helper.ForceUseAbility(data.bkb))
				return true
		if (helper.CanBeCasted(data.ghost, false, false))
			if (helper.ForceUseAbility(data.ghost))
				return true
	}

	if (helper.UseAbilityIfCondition(data.blink, data.blackHole))
		return true

	const canBeUsedRShard = data.refresher1 === undefined
		|| data.refresher1.Ability.RemainingCooldown > 0

	if (helper.CanBeCasted(data.refresher) || helper.CanBeCasted(data.refresher1)) {
		if (helper.CanBeCasted(data.blackHole, true, true, true, false) && !data.blackHole?.Ability.IsReady) {
			const useRefresher = !canBeUsedRShard ? data.refresher1 : data.refresher
			if (helper.HasMana(data.blackHole, useRefresher))
				if (helper.UseAbility(useRefresher)) {
					awaitForMalefice.Sleep(0.3)
					return true
				}
		}
	}

	if (helper.CanBeCasted(data.blackHole) || !awaitForMalefice.Sleeping)
		if (helper.UseAbility(data.malefice))
			return true

	return false
}

EventsX.on("AbilityDestroyed", abil => {
	if (abil instanceof RefresherShard)
		data.refresher1 = undefined
})

EventsX.on("GameEnded", () => {
	awaitForMalefice.ResetTimer()

	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
