import { ActiveAbility, HitChanceX, OrbWalker, SkillShotTypeX, UnitX } from "immortal-core/Imports";
import { ArrayExtensions, Tower } from "wrapper/Imports";
import { AIOUsable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { BaseMenu } from "../../../Menu/Base";
import { SkewerComboMode, SkewerHarassMode } from "../Menu/Index";
import { AIOReversePolarity } from "./ReversePolarity";

export class AIOSkewer extends AIOUsable {

	private ally: Nullable<UnitX>

	constructor(ability: ActiveAbility, private menu: BaseMenu) {
		super(ability)
	}

	public CanHit() {
		return true
	}

	public ForceUseAbility() {
		return false
	}

	public CanBeCasted(channelingCheck: boolean) {
		if (!super.CanBeCasted(channelingCheck))
			return false
		this.ally = this.GetPreferedAlly(TargetManager.Target!)
		return true;
	}

	public ShouldCast(): boolean {
		const target = TargetManager.Target!

		if (!target.IsStunned)
			return false

		if (!target.IsVisible || target.IsInvulnerable)
			return false

		if (target.IsMagicImmune && !this.Ability.PiercesMagicImmunity(target))
			return false

		return true
	}

	public GetPreferedAlly(target: UnitX) {

		const menu = this.menu.ComboKey.is_pressed ? SkewerComboMode : SkewerHarassMode

		let ally = ArrayExtensions.orderBy(TargetManager.AllyHeroes.filter(x => !x.Equals(this.Owner)
			&& menu.Heroes.IsEnabled(x.Name) && x.Distance(target) < this.Ability.CastRange + 600,
		), x => x.Distance(target))[0]

		if (menu.TowerState && ally === undefined)
			ally = ArrayExtensions.orderBy(TargetManager.IsValidAllyUnits.filter(x =>
				x.BaseOwner instanceof Tower && x.Distance(target) < this.Ability.CastRange + 500),
				x => x.Distance(target))[0]

		return ally
	}

	public UseAbilityOnTarget(target: UnitX) {
		if (!this.Ability.UseAbility(target, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(target), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		if (this.ally === undefined)
			return false

		const target = TargetManager.Target!
		const angle = target.Position.Subtract(this.ally.Position).AngleBetweenVectors(this.Owner.Position.Subtract(target.Position))

		if (angle > 40) {
			this.Owner.Move(this.ally.Position.Extend2D(target.Position, this.ally.Distance(target) + 100))
			this.Ability.ActionSleeper.Sleep(0.1)
			OrbWalker.Sleeper.Sleep(0.1, this.Owner.Handle)
			return true
		}

		if (!this.Owner.UseAbility(this.Ability, { intent: this.ally.Position.Extend2D(this.Owner.Position, 200)}))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityIfCondition(polarity: Nullable<AIOReversePolarity>, blink: boolean, force: boolean) {
		if (blink || force || polarity === undefined)
			return false

		const input = polarity.Ability.GetPredictionInput(TargetManager.Target!, TargetManager.IsValidEnemyHeroes)
		input.Delay += this.Ability.CastPoint + 0.5
		input.Range += this.Ability.CastRange
		input.CastRange = this.Ability.CastRange
		input.SkillShotType = SkillShotTypeX.Circle
		const output = polarity.Ability.GetPredictionOutput(input)
		const counts = this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1)
		if (output.HitChance < HitChanceX.Low || output.AoeTargetsHit.length < counts)
			return false

		const blinkPosition = output.CastPosition;
		if (this.Owner.Distance(blinkPosition) > this.Ability.CastRange)
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(TargetManager.Target!), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return this.Owner.UseAbility(this.Ability, { intent: blinkPosition })
	}
}
