import { ActiveAbility, HitChanceX, OrbWalker, UnitX } from "immortal-core/Imports";
import { BaseMenu } from "Menu/Base";
import { ArrayExtensions, Vector3 } from "wrapper/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class VoidChronosphere extends AIODisable {

	private readonly castPosition = new Vector3().Invalidate()

	constructor(ability: ActiveAbility, private menu: BaseMenu) {
		super(ability)
	}

	public get HitCounts() {
		return this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1)
	}

	public CanHit() {
		if (!super.CanHit())
			return false

		const counts = this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1)

		if (!this.Ability.CanHit(TargetManager.Target!, TargetManager.IsValidEnemyHeroes, counts))
			return false

		return true
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (this.Owner.HasBuffByName("modifier_faceless_void_time_walk"))
			return false

		return true
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target, TargetManager.IsValidEnemyHeroes)
		const inputDelay = input.Delay
		const output = this.Ability.GetPredictionOutput(input)
		const radius = this.Ability.Radius + 25

		if (output.HitChance < HitChanceX.Low)
			return false

		this.castPosition.CopyFrom(output.CastPosition)
		const allies = TargetManager.AllyHeroes.filter(x => !x.Equals(this.Owner))
		const allyPositions = allies.map(x => x.PredictedPosition(inputDelay))

		const castPositions = new Map<Vector3, number>()
		castPositions.set(this.castPosition, allyPositions.filter(x => x.Distance2D(this.castPosition) < radius).length)

		if (castPositions.get(this.castPosition)! > 0) {
			const ownerPosition = this.Owner.Position
			const lineCount = Math.ceil(radius / 50)
			for (let i = 0; i < lineCount; i++) {
				const alpha = (Math.PI / lineCount) * i
				const polar = new Vector3(Math.cos(alpha), Math.sin(alpha), 0)
				for (let j = 0.25; j <= 1; j += 0.25) {
					const range = polar.MultiplyScalar(input.CastRange * j)
					const start = ownerPosition.Subtract(range)
					const end = ownerPosition.Add(range)
					if (output.AoeTargetsHit.every(x => x.TargetPosition.Distance2D(start) < this.Ability.Radius))
						castPositions.set(start, allyPositions.filter(x => x.Distance2D(start) < radius).length)
					if (output.AoeTargetsHit.every(x => x.TargetPosition.Distance2D(end) < this.Ability.Radius))
						castPositions.set(end, allyPositions.filter(x => x.Distance2D(start) < radius).length)
				}
			}

			this.castPosition.CopyFrom(ArrayExtensions.orderByRevert(
				[...castPositions.entries()].map(x => x[0]), x => x.Distance2D(this.castPosition),
			)[0])
		}

		if (!this.Owner.UseAbility(this.Ability, { intent: this.castPosition }))
			return false

		const hitTime = this.Ability.GetHitTime(TargetManager.Target!) + 0.5
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	protected ChainStun(target: UnitX, invulnerability: boolean) {
		return true
	}
}
