import { ActiveAbility, HitChanceX, OrbWalker, ReversePolarity, SkillShotTypeX, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, AIOUsable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { AIOReversePolarity } from "./ReversePolarity";

export class BlinkDaggerMagnus extends AIOBlink {
	private readonly blinkPosition = new Vector3().Invalidate()
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const polarity = args.find(x => x.Ability instanceof ReversePolarity && x instanceof AIOReversePolarity) as AIOReversePolarity
		if (polarity === undefined)
			return false

		const input = polarity.Ability.GetPredictionInput(target, TargetManager.IsValidEnemyHeroes)
		input.Range += this.Ability.CastRange
		input.CastRange = this.Ability.CastRange
		input.SkillShotType = SkillShotTypeX.Circle
		const output = polarity.Ability.GetPredictionOutput(input)

		if (output.HitChance < HitChanceX.Low || output.AoeTargetsHit.length < polarity.TargetsToHit)
			return false

		this.blinkPosition.CopyFrom(output.CastPosition)

		if (this.Owner.Distance(this.blinkPosition) > this.Ability.CastRange)
			return false

		return true
	}

	public UseAbility(aoe: boolean) {
		if (!this.blinkPosition.IsValid)
			return false
		if (!this.Owner.UseAbility(this.Ability, { intent: this.blinkPosition }))
			return false
		HERO_DATA.ComboSleeper.Sleep(0.3, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(0.5, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(this.Ability.GetCastDelay(TargetManager.Target!) + 0.5)
		return true
	}
}
