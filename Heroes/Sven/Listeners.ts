import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { SvenMenu } from "./menu";
import { SAbilitiesCreated, SvenMode } from "./Sven";

HeroService.RegisterHeroModule(SvenMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(SvenMenu, oldState)
TargetManager.DestroyParticleDrawTarget(SvenMenu)

function onTick(unit: UnitX) {
	if (!SvenMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = SvenMenu.TargetMenuTree
	TargetManager.TargetLocked = SvenMenu.ComboKey.is_pressed || SvenMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		SvenMode,
		SvenMenu.ComboMode,
		SvenMenu,
		SvenMenu.ComboKey,
	)

	ComboMode(
		unit,
		SvenMode,
		SvenMenu.HarrasMode,
		SvenMenu,
		SvenMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!SvenMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		SvenMenu.TargetMenuTree.DrawTargetParticleColor,
		SvenMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(SvenMenu.ComboMode, owner)
	SwitchPanel.Draw(SvenMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!SvenMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (SvenMenu.ComboKey.is_pressed || SvenMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!SvenMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((SvenMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!SvenMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(SvenMenu.ComboMode)
		SwitchPanel.MouseLeftDown(SvenMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (SvenMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (SvenMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, SvenMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, SvenMenu))
}

function onDestoyed(owner: UnitX) {
	if (SvenMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== SvenMenu.npc_name)
		return
	SAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (SvenMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
