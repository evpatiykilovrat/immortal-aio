import { PathX } from "immortal-core/Imports";
import { Attributes,  Menu } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const UrsaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_ursa",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_black_king_bar",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_diffusal_blade",
		"item_abyssal_blade",
	],
	Abilities: [
		"ursa_earthshock",
		"ursa_overpower",
		"ursa_enrage",
	],
})

const ComboEnrageTree = UrsaMenu.HarassSettings.AddNode("Enrage", PathX.DOTAAbilities("ursa_enrage"))
export const ComboStateHP = ComboEnrageTree.AddSlider("Health (%)", 30, 5, 100, 0, "Use enrage if Health Percentage equals specified")
const HarassEnrageTree = UrsaMenu.ComboSettings.AddNode("Enrage", PathX.DOTAAbilities("ursa_enrage"))
export const HarassStateHP = HarassEnrageTree.AddSlider("Health (%)", 30, 5, 100, 0, "Use enrage if Health Percentage equals specified")

export const EnrageMode: {
	ComboStateHP: Menu.Slider;
	HarassStateHP: Menu.Slider;
} = {
	ComboStateHP,
	HarassStateHP,
}

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Health (%)", "Здоровье (%)"],
	["Use enrage if Health Percentage equals specified", "Использует enrage, если процент здоровья равен указанному"],
]))
