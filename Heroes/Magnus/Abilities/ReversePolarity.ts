import { ActiveAbility, OrbWalker, UnitX } from "immortal-core/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { BaseMenu } from "../../../Menu/Base";
import { AIOSkewer } from "./Skewer";

export class AIOReversePolarity extends AIODisable {
	private ally: Nullable<UnitX>
	private skewer!: AIOSkewer

	constructor(ability: ActiveAbility, private menu: BaseMenu) {
		super(ability)
	}

	public get TargetsToHit() {
		return (this.menu.ComboKey.is_pressed
			? (this.menu.ComboHitCountMenu?.value ?? 1)
			: (this.menu.HarassHitCountMenu?.value ?? 1))
	}

	public AddSkewer(skewer_: AIOSkewer) {
		this.skewer = skewer_
	}

	public CanBeCasted(channelingCheck: boolean) {
		if (!super.CanBeCasted(channelingCheck))
			return false
		this.ally = this.skewer.GetPreferedAlly(TargetManager.Target!)
		return true;
	}

	public CanHit() {
		if (!super.CanHit())
			return false
		return this.Ability.CanHit(TargetManager.Target!, TargetManager.IsValidEnemyHeroes, this.TargetsToHit)
	}

	public UseAbility(aoe: boolean) {

		if (this.ally !== undefined)
			this.Owner.Move(this.ally.Position)

		const target = TargetManager.Target!
		if (!this.Ability.UseAbility(target))
			return false
		const hitTime = this.Ability.GetHitTime(target) + 0.5
		const delay = this.Ability.GetCastDelay(target)
		target.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}
}
