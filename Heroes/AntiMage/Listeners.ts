import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { AAbilitiesCreated, AntiMageMode } from "./AntiMage";
import { AntiMageMenu } from "./menu";

HeroService.RegisterHeroModule(AntiMageMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(AntiMageMenu, oldState)
TargetManager.DestroyParticleDrawTarget(AntiMageMenu)

function onTick(unit: UnitX) {
	if (!AntiMageMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = AntiMageMenu.TargetMenuTree
	TargetManager.TargetLocked = AntiMageMenu.ComboKey.is_pressed || AntiMageMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		AntiMageMode,
		AntiMageMenu.ComboMode,
		AntiMageMenu,
		AntiMageMenu.ComboKey,
	)

	ComboMode(
		unit,
		AntiMageMode,
		AntiMageMenu.HarrasMode,
		AntiMageMenu,
		AntiMageMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!AntiMageMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		AntiMageMenu.TargetMenuTree.DrawTargetParticleColor,
		AntiMageMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(AntiMageMenu.ComboMode, owner)
	SwitchPanel.Draw(AntiMageMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!AntiMageMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (AntiMageMenu.ComboKey.is_pressed || AntiMageMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!AntiMageMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((AntiMageMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!AntiMageMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(AntiMageMenu.ComboMode)
		SwitchPanel.MouseLeftDown(AntiMageMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (AntiMageMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (AntiMageMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, AntiMageMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, AntiMageMenu))
}

function onDestoyed(owner: UnitX) {
	if (AntiMageMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== AntiMageMenu.npc_name)
		return
	AAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (AntiMageMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
