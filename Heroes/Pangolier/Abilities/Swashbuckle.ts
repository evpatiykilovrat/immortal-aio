import { ActiveAbility, HitChanceX, OrbWalker, Swashbuckle } from "immortal-core/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class PangoSwashbuckle extends AIONuke {
	private readonly swashbuckle: Swashbuckle

	constructor(ability: ActiveAbility) {
		super(ability)
		this.swashbuckle = ability as Swashbuckle
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		if (this.Owner.HasBuffByName("modifier_pangolier_gyroshell"))
			return false

		return true
	}

	public UseAbility(aoe: boolean) {

		const input = this.Ability.GetPredictionInput(TargetManager.Target!, TargetManager.EnemyHeroes)

		input.CastRange = this.Ability.CastRange
		input.Range = this.Ability.Range
		input.UseBlink = true
		input.AreaOfEffect = true

		const output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low || this.Owner.Distance(output.TargetPosition) > this.Ability.CastRange + 100)
			return false

		if (!this.swashbuckle.UseStartDirection(TargetManager.Target!, output.BlinkLinePosition, output.CastPosition))
			return false

		const delay = this.Ability.GetHitTime(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
