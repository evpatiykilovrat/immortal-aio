// import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
// import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
// import { BaseOrbwalk, ComboMode, HeroService, OrbWallker, ShieldBreaker, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
// import { HAbilitiesCreated, HoodwinkMode } from "./Hoodwink";
// import { HoodwinkMenu } from "./menu";

// HeroService.RegisterHeroModule(HoodwinkMenu.npc_name, {
// 	onTick,
// 	onDraw,
// 	onOrders,
// 	onKeyDown,
// 	onMouseDown,
// 	onCreated,
// 	onDestoyed,
// 	onAttackStarted,
// 	onAbilityCreated,
// })

// HoodwinkMenu.BaseState.OnDeactivate(() =>
// 	TargetManager.RemoveTargetParticle())

// function onTick(unit: UnitX) {
// 	if (!HoodwinkMenu.BaseState.value)
// 		return

// 	if (!unit.Owner.IsIllusion)
// 		TargetManager.Owner = unit

// 	TargetManager.Update()
// 	TargetManager.MenuSettings = HoodwinkMenu.TargetMenuTree
// 	TargetManager.TargetLocked = HoodwinkMenu.ComboKey.is_pressed || HoodwinkMenu.HarassKey.is_pressed

// 	if (!TargetManager.HasValidTarget)
// 		return

// 	ComboMode(
// 		unit,
// 		HoodwinkMode,
// 		HoodwinkMenu.ComboMode,
// 		HoodwinkMenu,
// 		HoodwinkMenu.ComboKey,
// 	)

// 	ComboMode(
// 		unit,
// 		HoodwinkMode,
// 		HoodwinkMenu.HarrasMode,
// 		HoodwinkMenu,
// 		HoodwinkMenu.HarassKey,
// 	)
// }

// function onDraw(owner: UnitX) {
// 	if (!HoodwinkMenu.BaseState.value || owner.BaseOwner.IsIllusion)
// 		return
// 	TargetManager.Draw(owner)
// }

// function onOrders(order: ExecuteOrder) {
// 	if (!HoodwinkMenu.BaseState.value || order.Issuers[0].IsEnemy())
// 		return true

// 	if (TargetManager.HasValidTarget && (HoodwinkMenu.ComboKey.is_pressed || HoodwinkMenu.HarassKey.is_pressed)
// 		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
// 		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
// 	) return false

// 	return true
// }

// function onKeyDown(key: VKeys) {
// 	if (!HoodwinkMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
// 		return true
// 	return !((HoodwinkMenu.ComboKey.assigned_key as VKeys) === key)
// }

// function onMouseDown(key: VMouseKeys) {
// 	if (!HoodwinkMenu.BaseState.value)
// 		return true

// 	if (!Input.IsKeyDown(VKeys.CONTROL) && (HoodwinkMenu.ComboKey.assigned_key as VMouseKeys) === key)
// 		return false

// 	return true
// }

// function onCreated(owner: UnitX) {
// 	if (HoodwinkMenu.npc_name !== owner.Name)
// 		return
// 	if (!ShieldBreakerMap.has(owner))
// 		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, HoodwinkMenu.Shields))
// 	if (!OrbWallker.has(owner.Handle))
// 		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, HoodwinkMenu))
// }

// function onDestoyed(owner: UnitX) {
// 	if (HoodwinkMenu.npc_name !== owner.Name)
// 		return
// 	OrbWallker.delete(owner.Handle)
// 	ShieldBreakerMap.delete(owner)
// }

// function onAttackStarted(unit: UnitX) {
// 	const orbWallker = OrbWallker.get(unit)
// 	if (orbWallker === undefined)
// 		return
// 	orbWallker.OnAttackStart()
// }

// function onAbilityCreated(owner: UnitX, abil: AbilityX) {
// 	if (owner.Name !== HoodwinkMenu.npc_name)
// 		return
// 	HAbilitiesCreated(abil)
// }

// EventsX.on("removeControllable", owner => {
// 	if (HoodwinkMenu.npc_name !== owner.Name)
// 		return
// 	OrbWallker.delete(owner.Handle)
// })
