import { TargetManager } from "../../Manager/Target";
import { AIODebuff } from "../Debuff";

export class AIOShivasGuard extends AIODebuff {

	public CanHit() {
		const target = TargetManager.Target!

		if (this.Owner.Distance(target) > 600)
			return false

		if (target.IsMagicImmune && !this.Ability.PiercesMagicImmunity(target))
			return false

		return true
	}
}
