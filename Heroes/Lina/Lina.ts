import { AbilityX, BlinkDagger, Dagon, DragonSlave, EtherealBlade, EulsScepterOfDivinity, EventsX, ForceStaff, Gleipnir, HurricanePike, LagunaBlade, LightStrikeArray, OrchidMalevolence, RodOfAtos, ScytheOfVyse, TickSleeperX, UnitX, VeilOfDiscord, WindWaker } from "immortal-core/Imports"
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIOEtherealBlade, AIOEulsScepter, AIOForceStaff, AIOHurricanePike, AIONuke, BaseOrbwalk, HERO_DATA, OrbWallker, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base"

const AttackSleeper = new TickSleeperX()

const data = {
	dragon: undefined as Nullable<AIONuke>,
	array: undefined as Nullable<AIODisable>,
	laguna: undefined as Nullable<AIONuke>,
	force: undefined as Nullable<AIOForceStaff>,
	pike: undefined as Nullable<AIOHurricanePike>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
	blink: undefined as Nullable<AIOBlink>,
	euls: undefined as Nullable<AIOEulsScepter>,
	hex: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	dagon: undefined as Nullable<AIONuke>,
	veil: undefined as Nullable<AIODebuff>,
	atos: undefined as Nullable<AIODisable>,
}

export const LAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof DragonSlave)
		data.dragon = new AIONuke(abil)
	if (abil instanceof LightStrikeArray)
		data.array = new AIODisable(abil)
	if (abil instanceof LagunaBlade)
		data.laguna = new AIONuke(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof EulsScepterOfDivinity || abil instanceof WindWaker)
		data.euls = new AIOEulsScepter(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	if (abil instanceof HurricanePike)
		data.pike = new AIOHurricanePike(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
}

export const LinaMode = (unit: UnitX, menu: ModeCombo) => {
	if (TargetManager.TargetSleeper.Sleeping)
		return false

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 550, 400))
		return true

	if (helper.UseAbility(data.force, 550, 400))
		return true

	if (helper.UseAbility(data.pike, 550, 400))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.ethereal))
		return true

	if (helper.UseAbility(data.dagon))
		return true

	if (helper.UseAbilityIfAny(data.euls, data.array))
		return true

	if (helper.CanBeCasted(data.euls, false) && helper.CanBeCasted(data.array, false)) {
		if (unit.IdealSpeed > TargetManager.Target!.IdealSpeed + 50) {
			AttackSleeper.Sleep(0.5)
			return false
		}
	}

	if (helper.UseAbility(data.array, false))
		return true

	if (helper.UseAbility(data.laguna)) {
		HERO_DATA.ComboSleeper.ExtendSleep(500, unit.Handle)
		return true
	}

	if (helper.UseAbility(data.dragon, false))
		return true

	if (helper.CanBeCasted(data.pike) && !OrbWallker.get(unit.Handle)?.MoveSleeper.Sleeping)
		if (data.pike?.UseAbilityOnTarget())
			return true

	return false
}

export class LinaOrbWalk extends BaseOrbwalk {
	protected Attack(target: UnitX) {
		if (AttackSleeper.Sleeping)
			return false
		return super.Attack(target)
	}
}

EventsX.on("GameEnded", () => {
	AttackSleeper.ResetTimer()
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
