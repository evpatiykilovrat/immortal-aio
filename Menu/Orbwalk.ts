import { Menu } from "wrapper/Imports";

export interface OrbwalkMenu {
	DangerRange: Menu.Slider;
	OrbwalkingMode: Menu.Dropdown;
	BaseNodeOrbwalk: Menu.Node;
	DangerMoveToMouse: Menu.Toggle;
	OrbwalkerStopOnStanding: Menu.Toggle;
}

export const OrbwalkMenu = (menu: Menu.Node): OrbwalkMenu => {

	const BaseNodeOrbwalk = menu.AddNode("Orbwalk", undefined, "Orbwalk or just attack")
	const OrbwalkingMode = BaseNodeOrbwalk.AddDropdown("Orbwalk mode", ["Move to mouse", "Move to target"])
	const OrbwalkerStopOnStanding = BaseNodeOrbwalk.AddToggle("Stop orbwalk if standing", true, "Unit will not orbwalk if target is standing")
	const DangerRange = BaseNodeOrbwalk.AddSlider("Danger range", 0, 0, 1200, 0, "Unit will not move closer to the target")
	const DangerMoveToMouse = BaseNodeOrbwalk.AddToggle("Danger move to mouse", false, "Unit will move to mouse without attacking when in danger range")

	return {
		DangerRange,
		OrbwalkingMode,
		BaseNodeOrbwalk,
		DangerMoveToMouse,
		OrbwalkerStopOnStanding,
	}
}

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Orbwalk", "Орбвалк"],
	["Orbwalk mode", "Режим орбвалка"],
	["Move to target", "Двигатся к цели"],
	["Move to mouse", "Двигаться к мышке"],
	["Danger range", "Радиус опасности"],
	["Stop orbwalk if standing", "Останавливать орбвалк"],
	["Danger move to mouse", "В опасности двигаться к мышке"],
	["Orbwalk or just attack", "Использовать орбвалкер или просто атаковать"],
	["Unit will not move closer to the target", "Юнит не будет двигаться ближе к цели"],
	["Unit will not orbwalk if target is standing", "Не использовать орбвалк, если цель не двигается"],
	["Unit will move to mouse without attacking when in danger range", "В радиусе опасности юнит будет двигаться к мышке не атакуя врага"],
]))
