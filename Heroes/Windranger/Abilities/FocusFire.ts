import { ActiveAbility, FocusFire, Powershot, UnitX } from "immortal-core/Imports";
import { AIOTargetable, AIOUsable, TargetManager } from "_ICore_/Index";

export class WindrangerFocusFire extends AIOTargetable {
	constructor(public Ability: ActiveAbility & FocusFire) {
		super(Ability)
	}
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!

		if (target.IsEthereal || TargetManager.Owner.HasBuffByName(this.Ability.BuffModifierName))
			return false

		if (target.Distance(this.Owner) < 300 && target.HealthPercentage < 50)
			return true

		if (!target.IsStunned && !target.IsRooted && !target.IsHexed)
			return false

		return true
	}

	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {

		const powershot = args.find(x => x.Ability instanceof Powershot)
		if (powershot === undefined)
			return true

		const damage = this.Ability.GetDamage(target)
		if (damage > target.Health)
			return false

		return true
	}
}
