import {  HitChanceX, isIDisable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIONuke extends AIOUsable {

	protected breakShields = [
		"modifier_ember_spirit_flame_guard",
		"modifier_item_pipe_barrier",
		"modifier_abaddon_aphotic_shield",
		"modifier_oracle_false_promise_timer",
	]

	public ForceUseAbility(): boolean {
		if (!this.Ability.UseAbility(TargetManager.Target!))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		if (isIDisable(this.Ability))
			TargetManager.Target!.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(TargetManager.Target!))

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast(): boolean {

		const target = TargetManager.Target!

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (target.IsReflectingDamage)
			return false

		if (this.Ability.BreaksLinkens && target.IsBlockingAbilities)
			return false

		const damage = this.Ability.GetDamage(target)
		if (damage <= 0 && !target.BaseOwner.ModifiersBook.HasAnyBuffByNames(this.breakShields))
			return false

		if (target.IsInvulnerable) {
			if (this.Ability.UnitTargetCast)
				return false
			if (!this.ChainStun(target, true))
				return false
		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {
		const target = TargetManager.Target!

		if (aoe) {
			if (!this.Ability.UseAbility(target, TargetManager.EnemyHeroes, HitChanceX.Low))
				return false
		} else {
			if (!this.Ability.UseAbility(target, HitChanceX.Low))
				return false
		}

		const delay = this.Ability.GetCastDelay(target)

		if (isIDisable(this.Ability))
			target.SetExpectedUnitState(this.Ability.AppliesUnitState, this.Ability.GetHitTime(target))

		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
