import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { BAbilitiesCreated, BristbackMode } from "./Bristback";
import { BristbackMenu } from "./menu";

HeroService.RegisterHeroModule(BristbackMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(BristbackMenu, oldState)
TargetManager.DestroyParticleDrawTarget(BristbackMenu)

function onTick(unit: UnitX) {
	if (!BristbackMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = BristbackMenu.TargetMenuTree
	TargetManager.TargetLocked = BristbackMenu.ComboKey.is_pressed || BristbackMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		BristbackMode,
		BristbackMenu.ComboMode,
		BristbackMenu,
		BristbackMenu.ComboKey,
	)

	ComboMode(
		unit,
		BristbackMode,
		BristbackMenu.HarrasMode,
		BristbackMenu,
		BristbackMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!BristbackMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		BristbackMenu.TargetMenuTree.DrawTargetParticleColor,
		BristbackMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(BristbackMenu.ComboMode, owner)
	SwitchPanel.Draw(BristbackMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!BristbackMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (BristbackMenu.ComboKey.is_pressed || BristbackMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!BristbackMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((BristbackMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!BristbackMenu.BaseState.value)
		return true

	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(BristbackMenu.ComboMode)
		SwitchPanel.MouseLeftDown(BristbackMenu.HarrasMode)
		return true
	}

	if (!Input.IsKeyDown(VKeys.CONTROL) && (BristbackMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (BristbackMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, BristbackMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, BristbackMenu))
}

function onDestoyed(owner: UnitX) {
	if (BristbackMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== BristbackMenu.npc_name)
		return
	BAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (BristbackMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
