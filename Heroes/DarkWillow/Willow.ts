import { AbilityX, Bedlam, BlinkDagger, Bloodthorn, BrambleMaze, CursedCrown, EulsScepterOfDivinity, EventsX, ForceStaff, Gleipnir, Nullifier, OrchidMalevolence, RodOfAtos, ScytheOfVyse, ShadowRealm, SpiritVessel, Terrorize, UnitX, UrnOfShadows, VeilOfDiscord, WindWaker } from "immortal-core/Imports"
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIOForceStaff, AIONuke, AIONullifier, BaseOrbwalk, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base";
import { WillowBrambleMaze } from "./Abilities/WillowBrambleMaze"
import { WillowCursedCrown } from "./Abilities/WillowCrown";
import { WillowEulsScepter } from "./Abilities/WillowEulsScepter"
import { WillowShadowRealm } from "./Abilities/WillowShadowRealm"

const data = {
	blink: undefined as Nullable<AIOBlink>,
	crown: undefined as Nullable<AIODebuff>,
	bedlam: undefined as Nullable<AIONuke>,
	terror: undefined as Nullable<AIODisable>,
	euls: undefined as Nullable<WillowEulsScepter>,
	maze: undefined as Nullable<WillowBrambleMaze>,
	realm: undefined as Nullable<WillowShadowRealm>,
	atos: undefined as Nullable<AIODisable>,
	veil: undefined as Nullable<AIODebuff>,
	force: undefined as Nullable<AIOForceStaff>,
	urn: undefined as Nullable<AIODebuff>,
	vessel: undefined as Nullable<AIODebuff>,
	hex: undefined as Nullable<AIODisable>,
	nullifier: undefined as Nullable<AIONullifier>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
}

export const onAbilityCreatedWillow = (abil: AbilityX) => {
	if (abil instanceof BrambleMaze)
		data.maze = new WillowBrambleMaze(abil)
	if (abil instanceof ShadowRealm)
		data.realm = new WillowShadowRealm(abil)
	if (abil instanceof CursedCrown)
		data.crown = new WillowCursedCrown(abil)
	if (abil instanceof Bedlam)
		data.bedlam = new AIONuke(abil)
	if (abil instanceof Terrorize)
		data.terror = new AIODisable(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof EulsScepterOfDivinity || abil instanceof WindWaker)
		data.euls = new WillowEulsScepter(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof UrnOfShadows)
		data.urn = new AIODebuff(abil)
	if (abil instanceof SpiritVessel)
		data.vessel = new AIODebuff(abil)
}

export const WillowMode = (unit: UnitX, menu: ModeCombo): boolean => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)
	if (helper.UseAbility(data.blink, 600, 450))
		return true

	if (helper.UseAbility(data.force, 600, 450))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.crown))
		return true

	if (helper.UseAbilityIfCondition(data.euls, data.crown))
		return true

	if (helper.UseAbility(data.realm))
		return true

	if (helper.UseAbility(data.maze))
		return true

	if (helper.UseAbility(data.bedlam))
		return true

	if (helper.UseAbility(data.vessel))
		return true

	if (helper.UseAbility(data.urn))
		return true

	if (helper.UseAbility(data.terror))
		return true

	return false
}

export class OrbWalkWillow extends BaseOrbwalk {
	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (target !== undefined && !this.Owner.HasAghanimsScepter) {
			if (data.realm?.Casted) {
				if (target.IsInvulnerable && this.OrbwalkMenu.OrbwalkerStopOnStanding.value)
					return super.ForceMove(target, false)
				return super.OrbwalkSet(target, data.realm.ShouldAttack(target), move)
			}
			if (this.Owner.HasBuffByName("modifier_dark_willow_bedlam")
				&& this.Owner.Distance(target) > data.bedlam!.Ability.Radius * 0.5) {
				return this.ForceMove(target, true)
			}
		}
		return super.OrbwalkSet(target, attack, move)
	}
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
// ...
