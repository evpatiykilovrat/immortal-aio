import { ActiveAbility } from "immortal-core/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class PudgeDismember extends AIODisable {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public CanHit() {
		const target = TargetManager.Target!
		if (target.HasBuffByName("modifier_pudge_meat_hook") && target.Distance(this.Owner) < 500)
			return true
		return super.CanHit()
	}

	public ShouldCast() {
		const target = TargetManager.Target!
		if (target.IsVisible && target.HasBuffByName("modifier_pudge_meat_hook") && target.Distance(this.Owner) < 500)
			return true
		return super.ShouldCast()
	}

	public ForceUseAbility() {
		if (!super.ForceUseAbility())
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay * 2, this.Owner.Handle)
		return true
	}
}
