import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const PhantomAssassinMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_phantom_assassin",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_sheepstick",
		"item_orchid",
		"item_bloodthorn",
		"item_rod_of_atos",
		"item_gungir",
		"item_diffusal_blade",
		"item_abyssal_blade",
	],
	Abilities: [
		"phantom_assassin_stifling_dagger",
		"phantom_assassin_phantom_strike",
	],
})
