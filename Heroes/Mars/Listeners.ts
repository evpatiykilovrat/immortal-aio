import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { MAbilitiesCreated, MarsMode } from "./Mars";
import { MarsMenu } from "./menu";

HeroService.RegisterHeroModule(MarsMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(MarsMenu, oldState)
TargetManager.DestroyParticleDrawTarget(MarsMenu)

function onTick(unit: UnitX) {
	if (!MarsMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = MarsMenu.TargetMenuTree
	TargetManager.TargetLocked = MarsMenu.ComboKey.is_pressed || MarsMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		MarsMode,
		MarsMenu.ComboMode,
		MarsMenu,
		MarsMenu.ComboKey,
	)

	ComboMode(
		unit,
		MarsMode,
		MarsMenu.HarrasMode,
		MarsMenu,
		MarsMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!MarsMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		MarsMenu.TargetMenuTree.DrawTargetParticleColor,
		MarsMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(MarsMenu.ComboMode, owner)
	SwitchPanel.Draw(MarsMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!MarsMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (MarsMenu.ComboKey.is_pressed || MarsMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!MarsMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((MarsMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!MarsMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(MarsMenu.ComboMode)
		SwitchPanel.MouseLeftDown(MarsMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (MarsMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (MarsMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, MarsMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, MarsMenu))
}

function onDestoyed(owner: UnitX) {
	if (MarsMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== MarsMenu.npc_name)
		return
	MAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (MarsMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
