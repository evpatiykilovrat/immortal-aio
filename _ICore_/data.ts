import { AbilityX, GameSleeperX, ParticlesX, UnitX } from "immortal-core/Imports"
import { ExecuteOrder, GameState, VKeys, VMouseKeys } from "wrapper/Imports"
import { TargetManager } from "./Manager/Target"

export interface HeroModule {
	onDraw(unit: UnitX): void
	onKeyDown(key: VKeys): boolean
	onMouseDown(key: VMouseKeys): boolean
	onOrders(order: ExecuteOrder): boolean
	onTick(unit: UnitX): void
	onCreated(unit: UnitX): void
	onDestoyed(unit: UnitX): void
	onAbilityCreated(unit: UnitX, abil: AbilityX): void
}

export class HERO_DATA {

	public static ComboSleeper = new GameSleeperX()
	public static ParticleManager = new ParticlesX()
	public static HeroModules = new Map<string, HeroModule>()

	public static get ModeAttack() {
		return ConVars.GetInt(this.NameAutoAttackMode)
	}

	public static set ModeAttack(value) {
		GameState.ExecuteCommand(`${this.NameAutoAttackMode} ${value}`)
	}

	public static Dispose() {
		this.DestroyAllParticle()
		this.ComboSleeper.FullReset()
		TargetManager.Target = undefined
	}

	private static NameAutoAttackMode = "dota_player_units_auto_attack_mode"

	private static DestroyAllParticle() {
		this.ParticleManager.AllParticles.forEach(particle => {
			if (particle === undefined || !particle.IsValid)
				return
			particle.Destroy(true)
		})
	}
}
