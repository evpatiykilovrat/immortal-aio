import { ActiveAbility, AetherRemnant, OrbWalker } from "immortal-core/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class VoidAetherRemnant extends AIODisable {
	private remnant: AetherRemnant

	constructor(ability: ActiveAbility) {
		super(ability)
		this.remnant = ability as AetherRemnant
	}

	public ForceUseAbility() {
		const position = TargetManager.Target!.Position
		if (!this.remnant.UseAbility(TargetManager.Target!, position.Extend2D(this.Owner.Position, 100), position))
			return false
		const hitTime = this.Ability.GetHitTime(TargetManager.Target!) + 0.5
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	public ShouldCast() {
		const target = TargetManager.Target!

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable)
			return true

		const damage = this.Ability.GetDamage(target)
		if (damage < target.Health) {

			if (target.IsStunned)
				return this.ChainStun(target, false);

			if (target.IsHexed)
				return this.ChainStun(target, false);

			if (target.IsSilenced)
				return !this.IsSilence(false) || this.ChainStun(target, false)

			if (target.IsRooted)
				return !this.IsRoot || this.ChainStun(target, false);

		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true
	}
}
