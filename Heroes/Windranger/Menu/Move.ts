import { PathX } from "immortal-core/Imports"
import { Menu } from "wrapper/Imports"
import { WindrangerMenu } from "./Base"

export interface ShackleMenu {
	StateShackleCombo: Menu.Toggle;
	StateShackleHarass: Menu.Toggle;
}

const ShackleComboTree = WindrangerMenu.ComboSettings
	.AddNode("Shackle Shot", PathX.DOTAAbilities("windrunner_shackleshot"))
const StateShackleCombo = ShackleComboTree.AddToggle("Move to shackle", true, "Auto position your hero to shackle the enemy")
const ShackleHarassTree = WindrangerMenu.HarassSettings
	.AddNode("Shackle Shot", PathX.DOTAAbilities("windrunner_shackleshot"))
const StateShackleHarass = ShackleHarassTree.AddToggle("Move to shackle", true, "Auto position your hero to shackle the enemy")

export const ShackleMove = {
	StateShackleCombo,
	StateShackleHarass,
}

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Move to shackle", "Двигаться"],
	["Auto position your hero to shackle the enemy", "Автоматически двигаться чтобы использовать Shackle Shot"],
]))
