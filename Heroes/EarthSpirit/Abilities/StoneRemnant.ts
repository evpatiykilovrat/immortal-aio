import { HitChanceX, OrbWalker, SkillShotTypeX } from "immortal-core/Imports";
import { AIOTargetable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { SpiritBoulderSmash } from "./BoulderSmash";
import { SpiritGeomagneticGrip } from "./GeomagneticGrip";

export class SpiritStoneRemnant extends AIOTargetable {

	public ForceUseAbility() {
		const target = TargetManager.Target!
		if (!this.Owner.UseAbility(this.Ability, { intent: this.Owner.Position.Extend2D(target.Position, 100)}))
			return false
		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(1)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbility(aoe: boolean, args?: Nullable<SpiritBoulderSmash | SpiritGeomagneticGrip>) {

		if (args instanceof SpiritBoulderSmash)
			return this.UseAbilityBoulderSmash(aoe, args)
		if (args instanceof SpiritGeomagneticGrip)
			return this.UseAbilityGeomagneticGrip(aoe, args)

		const target = TargetManager.Target!
		const modifier = target.GetBuffByName("modifier_earth_spirit_magnetize")
		if (modifier === undefined || modifier.RemainingTime > 0.75)
			return false

		const targets = TargetManager.IsValidEnemyHeroes.filter(x => x.HasBuffByName("modifier_earth_spirit_magnetize"))

		const input = this.Ability.GetPredictionInput(target, targets)
		input.Radius = 400
		input.AreaOfEffect = true
		input.SkillShotType = SkillShotTypeX.Circle
		const output = this.Ability.GetPredictionOutput(input)

		if (output.HitChance < HitChanceX.Low)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(1)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityBoulderSmash(aoe: boolean, smash: SpiritBoulderSmash) {
		const target = TargetManager.Target!
		const input = smash.Ability.GetPredictionInput(target)
		const output = smash.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false
		if (!this.Owner.UseAbility(this.Ability, { intent: this.Owner.Position.Extend2D(output.CastPosition, 100) }))
			return false
		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(1)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityGeomagneticGrip(aoe: boolean, grip: SpiritGeomagneticGrip) {
		const target = TargetManager.Target!

		const input = grip.Ability.GetPredictionInput(target)
		const output = grip.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false
		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(1)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
