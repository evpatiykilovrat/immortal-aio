import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const SvenMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_sven",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_sheepstick",
		"item_orchid",
		"item_bloodthorn",
		"item_rod_of_atos",
		"item_gungir",
		"item_diffusal_blade",
		"item_abyssal_blade",
		"item_mask_of_madness",
	],
	Abilities: [
		"sven_storm_bolt",
		"sven_warcry",
		"sven_gods_strength",
	],
	LinkenBreak: ["sven_storm_bolt"],
})
