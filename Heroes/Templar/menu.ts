import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const TemplarMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_templar_assassin",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_AGILITY,
	Items: [
		"item_blink",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_force_staff",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_hurricane_pike",
		"item_sheepstick",
		"item_solar_crest",
		"item_medallion_of_courage",
	],
	Abilities: [
		"templar_assassin_refraction",
		"templar_assassin_meld",
		"templar_assassin_psionic_trap",
	],
})
