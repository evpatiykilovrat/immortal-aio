import { EventsX, HeroX } from "immortal-core/Imports";
import { HERO_DATA } from "../data";

EventsX.on("AbilityCreated", abil => {
	if (!(abil.Owner instanceof HeroX) || abil.Owner.IsEnemy() || !abil.Owner.IsControllable)
		return
	const hero_module = HERO_DATA.HeroModules.get(abil.Owner.Name)
	if (hero_module === undefined)
		return
	hero_module.onAbilityCreated(abil.Owner, abil)
})
