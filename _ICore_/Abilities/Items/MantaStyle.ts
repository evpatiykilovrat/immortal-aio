import { TargetManager } from "../../Manager/Target";
import { AIODebuff } from "../Debuff";

export class AIOMantaStyle extends AIODebuff {
	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!

		if (target.IsAttackImmune || this.Owner.Distance(target) > this.Owner.GetAttackRange(target, 200))
			return false

		return true
	}
}
