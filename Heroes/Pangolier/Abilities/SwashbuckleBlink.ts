import { ActiveAbility, OrbWalker, Swashbuckle } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIOBlink, HERO_DATA, TargetManager } from "_ICore_/Index";

export class PangoSwashbuckleBlink extends AIOBlink {

	private readonly swashbuckle: ActiveAbility & Swashbuckle

	constructor(ability: ActiveAbility) {
		super(ability)
		this.swashbuckle = ability as ActiveAbility & Swashbuckle
	}

	public UseAbility(aoe: boolean, toPosition: Vector3) {
		const target = TargetManager.Target
		const position = this.Owner.Position.Extend2D(toPosition, Math.min(this.Ability.CastRange - 25, this.Owner.Distance(toPosition)))

		if (target !== undefined) {
			if (!this.swashbuckle.UseAbility(position, target.PredictedPosition(this.Ability.GetHitTime(target.Position))))
				return false
		} else {
			if (!this.swashbuckle.UseAbility(position, position))
				return false
		}
		const delay = this.Ability.GetCastDelay(position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.swashbuckle.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
