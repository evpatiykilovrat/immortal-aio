import { AbilityX, BlinkDagger, Bloodthorn, Dagon, DiffusalBlade, EtherealBlade, EulsScepterOfDivinity, EventsX, Gleipnir, Leap, Nullifier, OrbWalker, OrchidMalevolence, RodOfAtos, SacredArrow, ScytheOfVyse, SpiritVessel, Starstorm, UnitX, UrnOfShadows, VeilOfDiscord, WindWaker } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIOEtherealBlade, AIOEulsScepter, AIOForceStaff, AIONuke, AIONullifier, BaseOrbwalk, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";

let helper: AbilityHelper
const cyclonePos = new Vector3().Invalidate()
const cycloneBuffs = ["modifier_eul_cyclone", "modifier_wind_waker"]

const data = {
	atos: undefined as Nullable<AIODisable>,
	blink: undefined as Nullable<AIOBlink>,
	starstorm: undefined as Nullable<AIONuke>,
	arrow: undefined as Nullable<AIODisable>,
	leap: undefined as Nullable<AIOForceStaff>,
	diffusal: undefined as Nullable<AIODebuff>,
	veil: undefined as Nullable<AIODebuff>,
	urn: undefined as Nullable<AIODebuff>,
	vessel: undefined as Nullable<AIODebuff>,
	hex: undefined as Nullable<AIODisable>,
	nullifier: undefined as Nullable<AIONullifier>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	orchid: undefined as Nullable<AIODisable>,
	euls: undefined as Nullable<AIOEulsScepter>,
	dagon: undefined as Nullable<AIONuke>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
}

export const MAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof  EulsScepterOfDivinity || abil instanceof WindWaker)
		data.euls = new AIOEulsScepter(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	if (abil instanceof SpiritVessel)
		data.vessel = new AIODebuff(abil)
	if (abil instanceof UrnOfShadows)
		data.urn = new AIODebuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof Leap)
		data.leap = new AIOForceStaff(abil)
	if (abil instanceof DiffusalBlade)
		data.diffusal = new AIODebuff(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof SacredArrow)
		data.arrow = new AIODisable(abil)
	if (abil instanceof Starstorm)
		data.starstorm = new AIONuke(abil)
}

export const MiranaMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	if (helper === undefined)
		helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.ethereal))
		return true

	if (helper.UseAbilityIfNone(data.dagon, data.ethereal))
		return true

	if (!helper.CanBeCasted(data.euls) || !helper.CanBeCasted(data.arrow))
		if (helper.UseAbility(data.nullifier))
			return true

	if (!helper.CanBeCasted(data.euls) || !helper.CanBeCasted(data.arrow)) {
		if (helper.UseAbility(data.starstorm))
			return true
	}

	if (helper.UseAbility(data.diffusal))
		return true

	if (helper.UseAbility(data.blink, unit.GetAttackRange(), 350))
		return true

	if (helper.CanBeCasted(data.arrow))
		if (helper.UseAbility(data.euls)) {
			cyclonePos.CopyFrom(TargetManager.Target!.Position)
			return true
		}

	if (data.euls === undefined || !helper.CanBeCasted(data.euls) || cyclonePos.IsValid) {
		if (helper.UseAbility(data.arrow)) {
			cyclonePos.Invalidate()
			return true
		}
	}

	if (data.euls === undefined || !helper.CanBeCasted(data.euls))
		if (helper.UseAbility(data.leap, unit.GetAttackRange(TargetManager.Target) + 100, 550))
			return true

	if (helper.UseForceStaffAway(data.leap, 200)) {
		OrbWalker.Sleeper.Sleep(0.2, unit.Handle)
		HERO_DATA.ComboSleeper.Sleep(0.2, unit.Handle)
		data.leap?.Ability.ActionSleeper.Sleep(2)
		return true
	}

	if (helper.UseAbility(data.vessel))
		return true

	if (helper.UseAbility(data.urn))
		return true

}

export class OrbWalkMirana extends BaseOrbwalk {
	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (!target.HasAnyBuffByNames(cycloneBuffs))
			return super.OrbwalkSet(target, attack, move)
		if (target !== undefined && cyclonePos.IsValid) {
			attack = false
			if (this.Owner.Distance(target.Owner) >= 600) {
				move = false
				return false
			}

			if (data.leap!.Ability.Charges >= 3 && this.Owner.GetAngle(cyclonePos) <= 0.1)
				helper.UseAbility(data.leap)

			return super.Move(cyclonePos.Extend2D(this.Owner.Position, 600))
		}
		return super.OrbwalkSet(target, attack, move)
	}
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
	cyclonePos.Invalidate()
})
