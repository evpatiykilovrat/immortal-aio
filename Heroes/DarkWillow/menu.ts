import { BaseHeroMenu } from "Menu/Base";
import { Attributes } from "wrapper/Imports";

export const WillowMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_dark_willow",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_blink",
		"item_force_staff",
		"item_veil_of_discord",
		"item_sheepstick",
		"item_gungir",
		"item_rod_of_atos",
		"item_orchid",
		"item_bloodthorn",
		"item_nullifier",
		"item_cyclone",
		"item_spirit_vessel",
		"item_urn_of_shadows",
	],
	Abilities: [
		"dark_willow_bramble_maze",
		"dark_willow_cursed_crown",
		"dark_willow_shadow_realm",
		"dark_willow_bedlam",
		"dark_willow_terrorize",
	],
	LinkenBreak: ["dark_willow_cursed_crown"],
})
