import { ActiveAbility } from "immortal-core/Imports";
import { AIONuke, TargetManager } from "_ICore_/Index";

export class MarsGodsRebuke extends AIONuke {
	constructor(ability: ActiveAbility) {
		super(ability)
	}
	public ShouldCast() {
		if (!super.ShouldCast())
			return false
		if (TargetManager.Target!.HasBuffByName("modifier_mars_spear_impale"))
			return false
		return true
	}
}
