import { PathX } from "immortal-core/Imports";
import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../../Menu/Base";

export const MagnusMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_magnataur",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_force_staff",
		"item_shivas_guard",
		"item_refresher",
		"item_refresher_shard",
	],
	Abilities: [
		"magnataur_shockwave",
		"magnataur_skewer",
		// "magnataur_horn_toss",
		"magnataur_reverse_polarity",
	],
	HitMenu: { default: 1, name: "Reverse Polarity", imageName: PathX.DOTAAbilities("magnataur_reverse_polarity") },
})
