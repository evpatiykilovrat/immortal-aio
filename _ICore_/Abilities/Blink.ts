import { HitChanceX, OrbWalker } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIOBlink extends AIOUsable {

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Target!))
			return false
		const time = this.Ability.GetCastDelay(TargetManager.Target!) + 0.5
		this.Ability.ActionSleeper.Sleep(time)
		return true
	}

	public CanHit() {
		return true
	}

	public ShouldCast(): boolean {
		if (TargetManager.Target === undefined)
			return true

		const target = TargetManager.Target

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (target.HasBuffByName("modifier_pudge_meat_hook"))
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any): boolean {
		const IsValid = args.length !== 0 && args[0] !== undefined

		if (IsValid && args[0] instanceof Vector3)
			return this.UseAbilityPosition(args[0])

		if (IsValid && typeof args[0] === "number")
			return this.UseAbilityMinDistance(args[0], args[1])

		if (!this.Ability.UseAbility(TargetManager.Target!, TargetManager.EnemyHeroes, HitChanceX.Low))
			return false

		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityPosition(toPosition: Vector3) {
		if (this.Owner.Distance(toPosition) < 200)
			return false
		const position = this.Owner.Position.Extend2D(toPosition, Math.min(this.Ability.CastRange - 25, this.Owner.Distance(toPosition)));
		if (!this.Ability.UseAbility(position))
			return false
		const delay = this.Ability.GetCastDelay(position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityMinDistance(minDistance: number, blinkDistance: number) {
		if (this.Owner.Distance(TargetManager.Target!.BaseOwner) < minDistance)
			return false
		const position = TargetManager.Target!.Position.Extend2D(this.Owner.Position, blinkDistance)
		if (this.Owner.Distance(position) > this.Ability.CastRange)
			return false
		if (!this.Ability.UseAbility(position))
			return false
		const delay = this.Ability.GetCastDelay(position) + 0.1
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
