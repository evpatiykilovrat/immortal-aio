import { HitChanceX, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIOAoe extends AIOUsable {

	public ShouldCast() {
		const target = TargetManager.Target!

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (target.IsMagicImmune && !this.Ability.PiercesMagicImmunity(target))
			return false

		if (target.IsInvulnerable) {
			if (this.Ability.UnitTargetCast)
				return false

			if (!this.ChainStun(target, true))
				return false
		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public ForceUseAbility() {
		return this.UseAbility(true)
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (!this.Ability.UseAbility(TargetManager.Target!, TargetManager.EnemyHeroes, HitChanceX.Low))
			return false
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
