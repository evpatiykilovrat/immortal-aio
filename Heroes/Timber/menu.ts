import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const TimberMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_shredder",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_lotus_orb",
		"item_shivas_guard",
		"item_gungir",
		"item_rod_of_atos",
		"item_sheepstick",
		"item_blade_mail",
	],
	Abilities: [
		"shredder_whirling_death",
		"shredder_timber_chain",
		"shredder_flamethrower",
		"shredder_chakram",
		"shredder_chakram_2",
	],
})
