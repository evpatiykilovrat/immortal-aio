import { GameX } from "immortal-core/Imports"
import { EventsSDK } from "wrapper/Imports"
import { HERO_DATA } from "../data"
import { TargetManager } from "../Manager/Target"

EventsSDK.on("Tick", () => {
	if (!GameX.IsInGame)
		return
	TargetManager.Heroes.forEach(unit => {
		if (!unit.IsControllable || unit.IsEnemy())
			return
		const hero_module = HERO_DATA.HeroModules.get(unit.Name)
		if (hero_module === undefined)
			return
		hero_module.onTick(unit)
	})
})
