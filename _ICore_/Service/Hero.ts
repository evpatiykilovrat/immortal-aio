import { HeroModule, HERO_DATA } from "../data";

export class HeroService {
	public static RegisterHeroModule(name: string, module: HeroModule) {
		HERO_DATA.HeroModules.set(name, module)
	}
}
