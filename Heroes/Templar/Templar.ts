import { AbilityX, BlackKingBar, BlinkDagger, Bloodthorn, EventsX, ForceStaff, HurricanePike, MedallionOfCourage, Meld, MinotaurHorn, Nullifier, OrbWalker, OrchidMalevolence, PsiBlades, PsionicTrap, Refraction, ScytheOfVyse, SolarCrest, TickSleeperX, UnitsX, UnitX, Vector3Extend } from "immortal-core/Imports";
import { ArrayExtensions, Building, Creep, Flow_t, ProjectileManager, Unit, Vector3 } from "wrapper/Imports";
import { AbilityHelper, AIOBlink, AIOBloodthorn, AIOBuff, AIODebuff, AIODisable, AIOForceStaff, AIOHurricanePike, AIONuke, AIONullifier, AIOShield, BaseOrbwalk, HERO_DATA, OrbWallker, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { TemplarPsionicTrap } from "./Abilities/PsionicTrap";
import { TemplarMenu } from "./menu";

const TrapSleeper = new TickSleeperX()
const PreventAttackSleeper = new TickSleeperX()

const data = {
	meld: undefined as Nullable<AIONuke>,
	blink: undefined as Nullable<AIOBlink>,
	hex: undefined as Nullable<AIODisable>,
	bkb: undefined as Nullable<AIOShield>,
	horn: undefined as Nullable<AIOShield>,
	orchid: undefined as Nullable<AIODisable>,
	refraction: undefined as Nullable<AIOBuff>,
	force: undefined as Nullable<AIOForceStaff>,
	solar: undefined as Nullable<AIODebuff>,
	medallion: undefined as Nullable<AIODebuff>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	trap: undefined as Nullable<TemplarPsionicTrap>,
	nullifier: undefined as Nullable<AIONullifier>,
	pike: undefined as Nullable<AIOHurricanePike>,
}

const MoveToProjectile = (owner: UnitX, target: UnitX) => {
	if (!owner.CanMove())
		return false
	const projectile = ProjectileManager.AllTrackingProjectiles.find(x => x.IsValid
		&& x.Source instanceof Unit
		&& x.Target instanceof Unit
		&& x.Source.Index === owner.Handle && x.Target.IsValid)
	if (projectile === undefined)
		return false
	const projectileTarget = projectile.Target! as Unit
	const targetPredictedPosition = target.PredictedPosition(
		(projectile.Position.Distance2D(projectileTarget.Position) / projectile.Speed) + GetAvgLatency(Flow_t.IN))
	const psiPosition = projectileTarget.Position.Extend2D(targetPredictedPosition, -owner.Distance(projectileTarget.Position))
	owner.Move(psiPosition)
	return true
}

export const TAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof ForceStaff || abil instanceof HurricanePike)
		data.force = (abil instanceof HurricanePike) ? new AIOHurricanePike(abil) : new AIOForceStaff(abil)
	if (abil instanceof Meld)
		data.meld = new AIONuke(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof SolarCrest)
		data.solar = new AIODebuff(abil)
	if (abil instanceof MedallionOfCourage)
		data.medallion = new AIODebuff(abil)
	if (abil instanceof Refraction)
		data.refraction = new AIOBuff(abil)
	if (abil instanceof PsionicTrap)
		data.trap = new TemplarPsionicTrap(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof MinotaurHorn)
		data.horn = new AIOShield(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
}

export const TemplarComboMode = (unit: UnitX, menu: ModeCombo): boolean => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.refraction, 1300))
		return true

	if (!unit.IsMagicImmune)
		if (helper.UseAbility(data.horn, 1300))
			return true

	if (!unit.IsMagicImmune)
		if (helper.UseAbility(data.bkb, 500))
			return true

	if (helper.UseAbility(data.blink, 500, 0))
		return true

	if (helper.UseAbility(data.force, 500, 0))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.solar))
		return true

	if (helper.UseAbility(data.medallion))
		return true

	if (helper.CanBeCasted(data.meld, false)) {
		const orbWalker = OrbWallker.get(unit.Handle)
		if ((orbWalker?.AttackSleeper.RemainingSleepTime ?? 0) <= 0.1 && !TrapSleeper.Sleeping) {
			if (helper.UseAbility(data.meld)) {
				if (!TemplarMenu.HarassKey.is_pressed)
					PreventAttackSleeper.Sleep(0.05)
				TrapSleeper.ExtendSleep(0.15)
				HERO_DATA.ComboSleeper.ExtendSleep(0.15, unit.Owner)
				return true
			}
		}
	}

	if (!TrapSleeper.Sleeping && (!helper.CanBeCasted(data.blink) || unit.Distance(TargetManager.Target!) < 1000))
		if (helper.UseAbility(data.trap))
			return true

	return false
}

export class TemplarOrbWalk extends BaseOrbwalk {

	protected OrbwalkSet(target: UnitX, attack: boolean, move: boolean) {
		if (OrbWalker.Sleeper.Sleeping(this.Owner.Handle))
			return false

		if (target !== undefined && TemplarMenu.HarassKey.is_pressed
			&& target.Distance(this.Owner) > this.Owner.GetAttackRange(target)) {

			if (this.MoveSleeper.Sleeping)
				return false

			if (MoveToProjectile(this.Owner, target))
				return true

			const ownerPosition = this.Owner.Position
			const psiBlades = this.Owner.Abilities.find(x => x instanceof PsiBlades) as PsiBlades
			const attackDelay = this.Owner.GetAttackPoint() + GetAvgLatency(Flow_t.IN) + 0.3
			const targetPredictedPosition = target.PredictedPosition(attackDelay)
			const units = UnitsX.All.filter(x => x.IsValid)

			const unitTarget = ArrayExtensions.orderBy(units.filter(x => x.IsAlive
				&& x.IsVisible
				&& !x.IsInvulnerable
				&& !x.Equals(target)
				&& !x.Equals(this.Owner)
				&& !(x.BaseOwner instanceof Building)
				&& (x.IsEnemy() || x.BaseOwner instanceof Creep && x.HealthPercentage <= 50)
				&& x.Distance(target) < psiBlades.Range), x =>
					Vector3Extend.AngleBetween(ownerPosition, x.Position, targetPredictedPosition))[0]

			if (unitTarget !== undefined) {

				const unitTargetPosition = unitTarget.Position
				if (this.CanAttack(unitTarget) && Vector3Extend.AngleBetween(ownerPosition, unitTargetPosition, targetPredictedPosition) < 15) {
					this.LastMovePosition = new Vector3()
					this.LastTarget = unitTarget
					OrbWalker.Sleeper.Sleep(0.05, this.Owner.Handle)
					TrapSleeper.ResetTimer()
					return this.Attack(unitTarget)
				}

				const range = Math.min(Math.max(unitTarget.Distance(ownerPosition), 150), this.Owner.GetAttackRange())
				const movePosition = unitTargetPosition.Extend2D(targetPredictedPosition, -range)
				OrbWalker.Sleeper.Sleep(0.05, this.Owner.Handle)
				return this.Move(movePosition)
			}

			attack = false
		}

		return super.OrbwalkSet(target, attack, move)
	}

	protected Attack(target: UnitX) {
		if (PreventAttackSleeper.Sleeping)
			return false
		return super.Attack(target)
	}
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
	PreventAttackSleeper.ResetTimer()
})
