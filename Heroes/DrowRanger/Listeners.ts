import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { DAbilitiesCreated, DrowRangerMode } from "./DrowRanger";
import { DrowRangerMenu } from "./menu";

HeroService.RegisterHeroModule(DrowRangerMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(DrowRangerMenu, oldState)
TargetManager.DestroyParticleDrawTarget(DrowRangerMenu)

function onTick(unit: UnitX) {
	if (!DrowRangerMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = DrowRangerMenu.TargetMenuTree
	TargetManager.TargetLocked = DrowRangerMenu.ComboKey.is_pressed || DrowRangerMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		DrowRangerMode,
		DrowRangerMenu.ComboMode,
		DrowRangerMenu,
		DrowRangerMenu.ComboKey,
	)

	ComboMode(
		unit,
		DrowRangerMode,
		DrowRangerMenu.HarrasMode,
		DrowRangerMenu,
		DrowRangerMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!DrowRangerMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		DrowRangerMenu.TargetMenuTree.DrawTargetParticleColor,
		DrowRangerMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(DrowRangerMenu.ComboMode, owner)
	SwitchPanel.Draw(DrowRangerMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!DrowRangerMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (DrowRangerMenu.ComboKey.is_pressed || DrowRangerMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!DrowRangerMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((DrowRangerMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!DrowRangerMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(DrowRangerMenu.ComboMode)
		SwitchPanel.MouseLeftDown(DrowRangerMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (DrowRangerMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (DrowRangerMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, DrowRangerMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, DrowRangerMenu))
}

function onDestoyed(owner: UnitX) {
	if (DrowRangerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== DrowRangerMenu.npc_name)
		return
	DAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (DrowRangerMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
