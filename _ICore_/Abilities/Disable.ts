
import { ActiveAbility, HitChanceX, IDisable, OrbWalker } from "immortal-core/Imports";
import { BitsExtensions, modifierstate } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIODisable extends AIOUsable {

	protected AIODisable: ActiveAbility & IDisable

	constructor(ability: ActiveAbility) {
		super(ability)
		this.AIODisable = ability as ActiveAbility & IDisable
	}

	protected get IsHex() {
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_HEXED)
	}

	protected get IsRoot() {
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_ROOTED)
	}

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Target!))
			return false
		const hitTime = this.Ability.GetHitTime(TargetManager.Target!) + 0.5
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	public ShouldCast(): boolean {

		const target = TargetManager.Target!

		if (this.AIODisable.UnitTargetCast && !target.IsVisible)
			return false

		if (this.AIODisable.BreaksLinkens && target.IsBlockingAbilities)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable) {

			if (this.AIODisable.UnitTargetCast)
				return false

			if (!this.ChainStun(target, true))
				return false
		}

		if (this.Ability.GetDamage(target) < target.Health) {

			if (target.IsStunned)
				return this.ChainStun(target, false)

			if (target.IsHexed)
				return this.ChainStun(target, false)

			if (target.IsSilenced)
				return !this.IsSilence(false) || this.ChainStun(target, false)

			if (target.IsRooted)
				return !this.IsRoot || this.ChainStun(target, false)
		}

		if (target.IsRooted && !this.Ability.UnitTargetCast && target.GetImmobilityDuration <= 0)
			return false

		return true
	}

	public UseAbility(aoe: boolean): boolean {

		if (aoe) {
			if (!this.Ability.UseAbility(TargetManager.Target!, TargetManager.EnemyHeroes, HitChanceX.Low)) {
				return false
			}
		} else {
			if (!this.Ability.UseAbility(TargetManager.Target!, HitChanceX.Low)) {
				return false
			}
		}
		const hitTime = this.Ability.GetHitTime(TargetManager.Target!) + 0.5
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay)
		return true
	}

	public IsSilence(hex: boolean = true) {
		if (!hex && this.IsHex)
			return false
		return !BitsExtensions.HasBit(this.AIODisable.AppliesUnitState, modifierstate.MODIFIER_STATE_SILENCED)
	}
}
