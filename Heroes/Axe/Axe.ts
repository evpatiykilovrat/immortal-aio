import { AbilityX, BattleHunger, BerserkersCall, BlackKingBar, BladeMail, BlinkDagger, CullingBlade, Dagon, EventsX, ForceStaff, MantaStyle, MeteorHammer, Mjollnir, ShivasGuard, UnitX } from "immortal-core/Imports";
import { ModeCombo } from "../../Menu/Base";
import { AbilityHelper, AIOBlinkDagger, AIOBuff, AIODebuff, AIODisable, AIOForceStaff, AIONuke, AIOShield, HERO_DATA, ShieldBreakerMap, TargetManager } from "../../_ICore_/Index";
import { AxeCullingBlade } from "./Abilities/CullingBlade";
import { AxeMeteorHammer } from "./Abilities/MeteorHammer";

const data = {
	call: undefined as Nullable<AIODisable>,
	hunger: undefined as Nullable<AIODebuff>,
	manta: undefined as Nullable<AIOBuff>,
	blade: undefined as Nullable<AxeCullingBlade>,
	blink: undefined as Nullable<AIOBlinkDagger>,
	force: undefined as Nullable<AIOForceStaff>,
	bladeMail: undefined as Nullable<AIOShield>,
	bkb: undefined as Nullable<AIOShield>,
	shiva: undefined as Nullable<AIODebuff>,
	mjollnir: undefined as Nullable<AIOShield>,
	meteor: undefined as Nullable<AxeMeteorHammer>,
	dagon: undefined as Nullable<AIONuke>,
}

export const AAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof CullingBlade)
		data.blade = new AxeCullingBlade(abil)
	if (abil instanceof BerserkersCall)
		data.call = new AIODisable(abil)
	if (abil instanceof BattleHunger)
		data.hunger = new AIODebuff(abil)
	if (abil instanceof MantaStyle)
		data.manta = new AIOBuff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlinkDagger(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof Mjollnir)
		data.mjollnir = new AIOShield(abil)
	if (abil instanceof MeteorHammer)
		data.meteor = new AxeMeteorHammer(abil)
}

export const AxeMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.bkb, 400))
		return true

	if (helper.UseAbility(data.blade))
		return true

	if (helper.CanBeCasted(data.blade, false) && !helper.CanBeCasted(data.blade))
		if (helper.UseAbility(data.blink, 200, 0))
			return true

	if (helper.UseDoubleBlinkCombo(data.force, data.blink))
		return true

	if (helper.CanBeCastedIfCondition(data.blink, data.call))
		if (helper.UseAbility(data.bkb))
			return true

	if (helper.UseAbility(data.call)) {
		data.call?.Ability.ActionSleeper.ExtendSleep(1)
		return true
	}

	if (helper.CanBeCasted(data.meteor))
		if (TargetManager.Target!.HasBuffByName("modifier_axe_berserkers_call") && helper.UseAbility(data.meteor))
			return true

	if (helper.UseAbilityIfCondition(data.blink, data.call))
		return true

	if (helper.UseAbility(data.force, 500, 0))
		return true;

	if (!helper.CanBeCasted(data.call, false))
		if (helper.UseAbility(data.bladeMail, 400))
			return true

	if (helper.UseAbility(data.manta, unit.GetAttackRange()))
		return true

	if (helper.UseAbility(data.mjollnir, 400))
		return true

	if (helper.UseAbility(data.shiva))
		return true

	if (helper.UseAbility(data.dagon))
		return true

	if (helper.CanBeCasted(data.hunger)) {

		if (helper.CanBeCasted(data.call))
			return false

		if (helper.CanBeCasted(data.meteor) && data.call?.Ability.ActionSleeper.Sleeping === true)
			return false

		if (helper.UseAbility(data.hunger))
			return true
	}

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
