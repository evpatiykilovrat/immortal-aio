import { ActiveAbility, OrbWalker, Powershot, PRectangle, UnitX } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIONuke, AIOUsable, HERO_DATA, TargetManager } from "_ICore_/Index";
import { WindrangerShackleshot } from "./Shackleshot";

export class WindrangerPowershot extends AIONuke {
	public readonly castPosition = new Vector3()
	private Shackleshot: Nullable<WindrangerShackleshot>
	private readonly powershot!: Powershot
	constructor(ability: ActiveAbility) {
		super(ability)
		this.powershot = ability as Powershot
	}
	public AddShake(add: Nullable<WindrangerShackleshot>) {
		this.Shackleshot = add
	}
	public CancelChanneling(target: UnitX) {
		if (!this.Ability.IsChanneling || !this.Ability.BaseAbility.IsChanneling)
			return false
		if (target.IsStunned || target.IsRooted)
			return false
		const polygon = new PRectangle(
			this.Owner.Position,
			this.Owner.Position.Extend2D(this.castPosition, this.Ability.Range),
			this.Ability.Radius - 75)
		const input = this.Ability.GetPredictionInput(target)
		input.Delay = this.powershot.ChannelTime - this.Ability.BaseAbility.ChannelTime
		const output = this.Ability.GetPredictionOutput(input)
		if (!polygon.IsInside(output.TargetPosition) || this.powershot.GetCurrentDamage(target) > target.Health)
			return this.Owner.Stop()
	}
	public ShouldConditionCast(target: UnitX, args: AIOUsable[]) {
		const damage = this.Ability.GetDamage(target)
		if (damage > target.Health)
			return true
		if (args.length > 0)
			return target.GetImmobilityDuration > 0.4
		return true
	}
	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target)
		if ((this.Shackleshot?.Ability.TimeSinceCasted ?? 0) < 0.5)
			input.Delay -= this.Ability.ActivationDelay
		const output = this.Ability.GetPredictionOutput(input);
		if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
			return false
		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
