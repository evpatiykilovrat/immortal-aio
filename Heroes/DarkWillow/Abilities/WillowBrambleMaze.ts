import { ActiveAbility, OrbWalker } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIODisable, HERO_DATA, TargetManager } from "_ICore_/Index";

export class WillowBrambleMaze extends AIODisable {
	constructor (ability: ActiveAbility) {
		super(ability)
	}

	public UseAbility(aoe: boolean) {

		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target);
		const output = this.Ability.GetPredictionOutput(input);
		const targetPosition = output.TargetPosition;
		let castPosition = new Vector3().Invalidate()

		for (const vector3 of this.GetMazePositions(targetPosition)) {
			const tempCast = targetPosition.Extend2D(vector3, -vector3.Distance2D(targetPosition))
			if (this.Owner.Distance(tempCast) > this.Ability.CastRange)
				continue
			castPosition = tempCast
			break
		}

		if (!castPosition.IsValid)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: castPosition }))
			return false

		const hitTime = this.Ability.GetHitTime(TargetManager.Target!) + 0.5
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)

		TargetManager.Target!.SetExpectedUnitState(this.AIODisable.AppliesUnitState, hitTime)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(hitTime)
		return true
	}

	public GetMazePositions(center: Vector3) {
		const list: Vector3[] = []
		for (let i = 0; i < 2; i++) {
			const alpha = (Math.PI / 2) * i;
			const polar = new Vector3(Math.cos(alpha), Math.sin(alpha), 0);
			const range = polar.MultiplyScalar(200)
			list.push(center.Subtract(range))
			list.push(center.Add(range))
		}
		for (let i = 0; i < 2; i++) {
			const alpha = (Math.PI / 4) + ((Math.PI / 2) * i)
			const polar2 = new Vector3(Math.cos(alpha), Math.sin(alpha), 0)
			const range = polar2.MultiplyScalar(500)
			list.push(center.Subtract(range))
			list.push(center.Add(range))
		}
		return list
	}
}
