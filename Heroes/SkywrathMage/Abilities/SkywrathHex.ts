import { ActiveAbility, HeroX, isIBlink, isIDisable, isIShield, LotusOrb, MantaStyle, SpiritBearX } from "immortal-core/Imports";
import { AIODisable, TargetManager } from "_ICore_/Index";

export class HexSkywrath extends AIODisable {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldCast() {
		if (!super.ShouldCast())
			return false

		const target = TargetManager.Target!

		if (target.IsRooted) {
			const isAviableLotusOrb = TargetManager.AllEnemyUnits.some(x => (x instanceof HeroX || x instanceof SpiritBearX)
				&& x.Abilities.some(abil => abil instanceof LotusOrb && x.HasInventory(abil) && abil.CanBeCasted(false)))

			if (isAviableLotusOrb)
				return true

			return target.Abilities.some(x => (isIShield(x) || isIDisable(x) || isIBlink(x) || x instanceof MantaStyle) && x.CanBeCasted(false))
		}

		return true
	}
}
