import { ActiveAbility, HitChanceX, IShield, isIShield, isIToggleable, OrbWalker } from "immortal-core/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOUsable } from "./Usable";

export class AIOShield extends AIOUsable {

	public Shield: ActiveAbility & IShield

	constructor(ability: ActiveAbility) {
		super(ability)
		this.Shield = ability as ActiveAbility & IShield
	}

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Owner))
			return false
		if (isIShield(this.Ability) && this.Ability.AppliesUnitState !== undefined)
			TargetManager.Owner.SetExpectedUnitState(this.Ability.AppliesUnitState)
		const delay = this.Ability.GetCastDelay(TargetManager.Owner)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public ShouldCast() {

		const target = TargetManager.Owner

		if (target.IsInvulnerable)
			return false

		if (target.Equals(this.Shield.Owner)) {
			if (!this.Shield.ShieldsOwner)
				return false
		} else {
			if (!this.Shield.ShieldsAlly)
				return false
		}

		if (target.IsMagicImmune && !this.Shield.PiercesMagicImmunity(target))
			return false

		if (target.HasBuffByName(this.Shield.ShieldModifierName))
			return false

		if (isIToggleable(this.Shield) && this.Shield.Enabled)
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]) {
		if (typeof args[0] === "number")
			return this.UseAbilityCheckDistance(args[0])
		return this.UseAbility_(aoe)
	}

	private UseAbility_(aoe: boolean, ...args: any[]) {
		if (!this.Ability.UseAbility(TargetManager.Owner, TargetManager.AllyHeroes, HitChanceX.Low))
			return false
		if (isIShield(this.Ability) && this.Ability.AppliesUnitState !== undefined)
			TargetManager.Owner.SetExpectedUnitState(this.Ability.AppliesUnitState)
		const delay = this.Ability.GetCastDelay(TargetManager.Target!)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	private UseAbilityCheckDistance(distance: number) {
		if (this.Owner.Distance(TargetManager.Target!) > distance)
			return false
		return this.UseAbility_(true)
	}
}
