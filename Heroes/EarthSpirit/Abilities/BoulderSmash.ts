import { EarthSpiritStone, HeroX, HitChanceX, OrbWalker, PRectangle, SkillShotTypeX, UnitsX, UnitX } from "immortal-core/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class SpiritBoulderSmash extends AIONuke {

	public ForceUseAbility() {
		const target = TargetManager.Target!

		const input = this.GetPredictionInput(target)
		const output = this.Ability.GetPredictionOutput(input)

		if (output.HitChance < HitChanceX.Low)
			return false

		// TODO Check UnitsX.All
		for (const unit of UnitsX.All.filter(x => (!(x instanceof HeroX) || x.IsIllusion) && x.Distance(this.Owner) < this.Ability.CastRange && x.IsAlive)) {

			const rec = new PRectangle(
				this.Owner.Position.Extend2D(unit.Position, -100),
				this.Owner.Position.Extend2D(unit.Position, this.Ability.Range),
				this.Ability.Radius);

			if (!rec.IsInside(output.TargetPosition))
				continue

			if (!this.Owner.UseAbility(this.Ability, { intent: unit }))
				continue

			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}
		return false
	}

	public CanHit() {
		return !this.Owner.HasBuffByName("modifier_earth_spirit_rolling_boulder_caster")
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const input = this.GetPredictionInput(target)
		const output = this.Ability.GetPredictionOutput(input)

		if (output.HitChance < HitChanceX.Low)
			return false

		for (const stone of UnitsX.All.filter(x => x instanceof EarthSpiritStone && x.Distance(this.Owner) < this.Ability.CastRange && x.IsAlive)) {

			const rec = new PRectangle(
				this.Owner.Position.Extend2D(output.TargetPosition, -100),
				this.Owner.Position.Extend2D(output.TargetPosition, this.Ability.Range),
				this.Ability.Radius)

			if (!rec.IsInside(stone.Position))
				continue

			if (!this.Owner.UseAbility(this.Ability, { intent: output.CastPosition }))
				continue

			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}
		return false
	}

	private GetPredictionInput(target: UnitX) {
		const input = this.Ability.GetPredictionInput(target)
		input.SkillShotType = SkillShotTypeX.Line
		return input
	}
}
