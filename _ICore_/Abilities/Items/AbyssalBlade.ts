import { TargetManager } from "../../Manager/Target";
import { AIODisable } from "../Disable";

export class AIOAbyssalBlade extends AIODisable {

	public ShouldCast() {

		const target = TargetManager.Target!
		if (!target.IsInNormalState && !target.IsTeleporting && !target.IsChanneling)
			return false

		return this.ShouldForceCast()
	}

	public ShouldForceCast() {
		const target = TargetManager.Target!

		if (this.Ability.UnitTargetCast && !target.IsVisible)
			return false

		if (this.AIODisable.UnitTargetCast && target.IsBlockingAbilities)
			return false

		if (target.IsDarkPactProtected)
			return false

		if (target.IsInvulnerable)
			return false

		return true
	}
}
