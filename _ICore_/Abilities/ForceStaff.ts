import { OrbWalker } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { HERO_DATA } from "../data";
import { TargetManager } from "../Manager/Target";
import { AIOBlink } from "./Blink";

export class AIOForceStaff extends AIOBlink {

	public ForceUseAbility() {
		if (!this.Ability.UseAbility(TargetManager.Owner))
			return false
		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(this.Owner.InFront(this.Ability.Range)), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbility(aoe: boolean, ...args: any) {
		const IsValid = args.length !== 0 && args[0] !== undefined

		if (IsValid && args[0] instanceof Vector3)
			return this.UseAbilityPosition(args[0])

		if (IsValid && typeof args[0] === "number")
			return this.UseAbilityMinDistance(args[0], args[1])

		return false
	}

	public UseAbilityPosition(toPosition: Vector3) {
		if (this.Owner.GetAngle(toPosition) > 0.5)
			return false

		if (!this.Ability.UseAbility(this.Owner))
			return false

		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(this.Owner.InFront(this.Ability.Range)), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityMinDistance(minDistance: number, blinkDistance: number) {
		const target = TargetManager.Target!

		if (this.Owner.Distance(target) < minDistance)
			return false

		const distance = target.Distance(this.Owner)
		if (distance > this.Ability.Range + blinkDistance)
			return false

		if (this.Owner.GetAngle(target.Position) > 0.5)
			return this.Owner.Move(target.Position)

		if (!this.Ability.UseAbility(this.Owner))
			return false

		const delay = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(target.Position), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbilityOnTarget() {
		const target = TargetManager.Target!
		if (target.GetAngle(this.Owner.Position) > 0.3)
			return false
		if (target.Distance(this.Owner) > this.Ability.Range + 100)
			return false
		if (!this.Ability.UseAbility(target))
			return false
		const time = this.Ability.CastDelay
		HERO_DATA.ComboSleeper.Sleep(this.Ability.GetHitTime(target.Position), this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(time + 0.5)
		return true
	}
}
