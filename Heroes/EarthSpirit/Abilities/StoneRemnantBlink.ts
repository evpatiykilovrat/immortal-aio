import { OrbWalker } from "immortal-core/Imports";
import { AIOTargetable, HERO_DATA } from "_ICore_/Index";

export class SpiritStoneRemnantBlink extends AIOTargetable {
	public CanHit() {
		return true
	}
	public ShouldCast() {
		return true
	}
	public UseAbility(aoe: boolean) {
		if (!this.Owner.UseAbility(this.Ability, { intent: this.Owner.InFront(150) }))
			return false
		const delay = this.Ability.GetCastDelay(this.Owner.Position)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(2)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
