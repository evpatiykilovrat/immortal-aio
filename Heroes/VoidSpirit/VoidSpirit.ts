import { AbilityX, AetherRemnant, AstralStep, BlackKingBar, Bloodthorn, Dagon, Dissimilate, EulsScepterOfDivinity, EventsX, MinotaurHorn, Mjollnir, Nullifier, OrbWalker, OrchidMalevolence, ResonantPulse, ScytheOfVyse, ShivasGuard, SpiritVessel, TickSleeperX, UnitX, UrnOfShadows, VeilOfDiscord, WindWaker } from "immortal-core/Imports";
import { HERO_DATA } from "_ICore_/data";
import { AbilityHelper, AIOBloodthorn, AIODebuff, AIODisable, AIONuke, AIONullifier, AIOShield, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { VoidAetherRemnant } from "./Abilities/VoidAetherRemnant";
import { VoidDissimilate } from "./Abilities/VoidDissimilate";

const forceEul = new TickSleeperX()
const forceRemnant = new TickSleeperX()

const data = {
	dissimilate: undefined as Nullable<VoidDissimilate>,
	pulse: undefined as Nullable<AIONuke>,
	step: undefined as Nullable<AIONuke>,
	bkb: undefined as Nullable<AIOShield>,
	horn: undefined as Nullable<AIOShield>,
	mjollnir: undefined as Nullable<AIOShield>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	shiva: undefined as Nullable<AIODebuff>,
	veil: undefined as Nullable<AIODebuff>,
	eul: undefined as Nullable<AIODisable>,
	dagon: undefined as Nullable<AIONuke>,
	vessel: undefined as Nullable<AIODebuff>,
	urn: undefined as Nullable<AIODebuff>,
	nullifier: undefined as Nullable<AIONullifier>,
	hex: undefined as Nullable<AIODisable>,
	remnant: undefined as Nullable<VoidAetherRemnant>,
}

export const VAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Mjollnir)
		data.mjollnir = new AIOShield(abil)
	if (abil instanceof AstralStep)
		data.step = new AIONuke(abil)
	if (abil instanceof AetherRemnant)
		data.remnant = new VoidAetherRemnant(abil)
	if (abil instanceof Dissimilate)
		data.dissimilate = new VoidDissimilate(abil)
	if (abil instanceof ResonantPulse)
		data.pulse = new AIONuke(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof EulsScepterOfDivinity || abil instanceof WindWaker)
		data.eul = new AIODisable(abil)
	if (abil instanceof UrnOfShadows)
		data.urn = new AIODebuff(abil)
	if (abil instanceof SpiritVessel)
		data.vessel = new AIODebuff(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof MinotaurHorn)
		data.horn = new AIOShield(abil)
}

export const VoidSpiritMode = (owner: UnitX, menu: ModeCombo) => {

	const shield = ShieldBreakerMap.get(owner)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(owner.Handle) || !TargetManager.HasValidTarget)
		return false

	const target = TargetManager.Target!
	const helper = new AbilityHelper(owner, menu)

	if (owner.HasBuffByName("modifier_void_spirit_dissimilate_phase")) {
		HERO_DATA.ComboSleeper.Sleep(0.1, owner.Handle)
		OrbWalker.Sleeper.Sleep(0.1, owner.Handle)
		return owner.Move(target.Position)
	}

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.dagon))
		return true

	if (!owner.IsMagicImmune)
		if (helper.UseAbility(data.horn, 500))
			return true

	if (!owner.IsMagicImmune)
		if (helper.UseAbility(data.bkb, 500))
			return true

	if (helper.UseAbility(data.mjollnir, 500))
		return true

	if (helper.UseAbility(data.vessel))
		return true

	if (helper.UseAbility(data.urn))
		return true

	if (helper.CanBeCasted(data.remnant) && target.GetImmobilityDuration <= 0) {
		if (helper.CanBeCasted(data.eul, false, false)) {
			if (helper.UseAbility(data.eul) || (forceEul.Sleeping && helper.ForceUseAbility(data.eul))) {
				forceRemnant.Sleep(0.5)
				forceEul.ResetTimer()
				return true
			}
		}
	}

	if (forceRemnant.Sleeping && helper.CanBeCasted(data.remnant, true, false) && helper.ForceUseAbility(data.remnant, true)) {
		forceRemnant.ResetTimer()
		return true
	}

	if (owner.Distance(target) > 300) {
		if (helper.UseAbility(data.step)) {
			forceEul.Sleep(0.7)
			return true
		}
	}

	if (!helper.CanBeCasted(data.step, false) || owner.Distance(target) < 500 || !target.CanMove())
		if (helper.UseAbility(data.remnant))
			return true

	if (helper.UseAbility(data.dissimilate))
		return true

	if (helper.UseAbilityIfNone(data.shiva, data.dissimilate))
		return true

	if (!owner.HasAghanimsScepter || !target.IsSilenced) {
		if (helper.UseAbility(data.pulse)) {
			data.pulse?.Ability.ActionSleeper.ExtendSleep(0.2)
			return true
		}
	}

	return false
}

EventsX.on("GameEnded", () => {
	forceEul.ResetTimer()
	forceRemnant.ResetTimer()
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
