import { Attributes } from "wrapper/Imports";
import { BaseHeroMenu } from "../../Menu/Base";

export const PugnaMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_pugna",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_INTELLECT,
	Items: [
		"item_blink",
		"item_rod_of_atos",
		"item_sheepstick",
		"item_gungir",
		"item_orchid",
		"item_bloodthorn",
		"item_dagon_5",
		"item_veil_of_discord",
		"item_nullifier",
	],
	Abilities: [
		"pugna_nether_blast",
		"pugna_decrepify",
		"pugna_nether_ward",
		"pugna_life_drain",
	],
	LinkenBreak: ["pugna_decrepify", "pugna_life_drain"],
})
