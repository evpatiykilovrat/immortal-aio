import { ActiveAbility, OrbWalker } from "immortal-core/Imports";
import { AIOBlink, HERO_DATA, TargetManager } from "_ICore_/Index";

export class LegionBlink extends AIOBlink {

	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ForceUseAbility() {
		const target = TargetManager.Target!
		const position = target.PredictedPosition(0.4)
			.Extend2D(this.Owner.Position, 100)

		if (this.Owner.Distance(position) > this.Ability.CastRange)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: position }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}

	public UseAbility(aoe: boolean) {

		const target = TargetManager.Target!
		const position = target.PredictedPosition(0.4)
			.Extend2D(this.Owner.Position, 100)

		if (this.Owner.Distance(position) > this.Ability.CastRange)
			return false

		if (!this.Owner.UseAbility(this.Ability, { intent: position }))
			return false

		const delay = this.Ability.GetCastDelay(target)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(delay + 0.5)
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
