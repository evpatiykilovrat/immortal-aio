import { AbilityX, Laser, DefenseMatrix, BlinkDagger, LotusOrb, HeatSeekingMissile,ShivasGuard , EtherealBlade, EventsX, Gleipnir, MarchOfTheMachines, Rearm, RodOfAtos, ScytheOfVyse, Dagon, UnitX, VeilOfDiscord,BladeMail, } from "immortal-core/Imports";
import {AIONoTarget, AbilityHelper, AIODebuff, AIODisable, AIOEtherealBlade, AIOForceStaff, AIONuke, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
//import { TinkerRearm } from "./Abilities/TinkerRearm";
//import { TinkerBlink } from "./Abilities/TinkerBlink";
import { TinkerMarshs } from "./Abilities/TinkerMarsh";

export const data = {
	laser: undefined as Nullable<AIONuke>,
    matrix: undefined as Nullable<AIONuke>,
    rocket: undefined as Nullable<AIONuke>,
    marsh: undefined as Nullable<TinkerMarshs>,
    //rearm: undefined as Nullable<TinkerRearm>,

	dagon: undefined as Nullable<AIONuke>,
	atos: undefined as Nullable<AIODisable>,
    hex: undefined as Nullable<AIODisable>,
	veil: undefined as Nullable<AIODebuff>,
	force: undefined as Nullable<AIOForceStaff>,
	concussive: undefined as Nullable<AIODebuff>,
	ethereal: undefined as Nullable<AIOEtherealBlade>,
	//blink: undefined as Nullable<TinkerBlink>,

}

export const SAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof Laser)
		data.laser = new AIONuke(abil)
	if (abil instanceof DefenseMatrix)
		data.matrix = new AIONuke(abil)
	if (abil instanceof HeatSeekingMissile)
		data.rocket = new AIONuke(abil)
	if (abil instanceof MarchOfTheMachines)
		data.marsh = new TinkerMarshs(abil)
	

	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIOEtherealBlade(abil)
	//if (abil instanceof BlinkDagger)
		//data.blink = new TinkerBlink(abil)


}

export const TinkerNarkoman = (unit: UnitX, menu: ModeCombo): boolean => {

	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const abilityHelper = new AbilityHelper(unit, menu)

	//if (abilityHelper.UseAbility(data.blink, 850, 600))
		//return true

	if (abilityHelper.UseAbility(data.matrix))
		return true

	if (abilityHelper.UseAbility(data.hex))
		return true

	if (abilityHelper.UseAbility(data.ethereal))
		return true

	if (abilityHelper.UseAbility(data.laser))
		return true

	if (abilityHelper.UseAbility(data.rocket))
		return true

	if (abilityHelper.UseAbility(data.marsh))
		return true    

	//if (abilityHelper.UseAbility(data.rearm))
		//return true

	if (abilityHelper.UseAbility(data.atos))
		return true

	if (abilityHelper.UseAbility(data.dagon))
		return true

	if (abilityHelper.UseAbility(data.veil))
		return true

	if (abilityHelper.UseAbility(data.force, 850, 600))
		return true



	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
