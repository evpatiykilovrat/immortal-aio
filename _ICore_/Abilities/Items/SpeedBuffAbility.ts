import { TargetManager } from "../../Manager/Target";
import { AIOBuff } from "../Buff";

export class AIOSpeedBuff extends AIOBuff {
	public ShouldCast() {
		if (!super.ShouldCast())
			return false
		const target = TargetManager.Target!
		if ((!target.IsMoving || target.GetAngle(this.Owner.Position) < 1) && this.Owner.CanAttack(target))
			return false
		return true
	}
}
