import { AbilityX, BlinkDagger, EventsX, ForceStaff, OrbWalker, RefresherOrb, RefresherShard, ReversePolarity, Shockwave, Skewer, UnitX } from "immortal-core/Imports";
import { ArrayExtensions } from "wrapper/Imports";
import { BaseMenu, ModeCombo } from "../../Menu/Base";
import { HERO_DATA } from "../../_ICore_/data";
import { AbilityHelper, AIOForceStaff, AIOShivasGuard, AIOUntargetable, BaseOrbwalk, ShieldBreakerMap, TargetManager } from "../../_ICore_/Index";
import { BlinkDaggerMagnus } from "./Abilities/BlinkDaggerMagnus";
import { AIOReversePolarity } from "./Abilities/ReversePolarity";
import { AIOShockwave } from "./Abilities/Shockwave";
import { AIOSkewer } from "./Abilities/Skewer";
import { AllySkewerKey, AllySkewerSelector } from "./Menu/Index";

const data = {
	shiva: undefined as Nullable<AIOShivasGuard>,
	force: undefined as Nullable<AIOForceStaff>,
	blink: undefined as Nullable<BlinkDaggerMagnus>,
	polarity: undefined as Nullable<AIOReversePolarity>,
	shockwave: undefined as Nullable<AIOShockwave>,
	skewer: undefined as Nullable<AIOSkewer>,
	refresher: undefined as Nullable<AIOUntargetable>,
	refresherShard: undefined as Nullable<AIOUntargetable>,
}

export const MAbilitiesCreated = (abil: AbilityX, menu: BaseMenu) => {
	if (abil instanceof RefresherOrb)
		data.refresher = new AIOUntargetable(abil)
	if (abil instanceof RefresherShard)
		data.refresherShard = new AIOUntargetable(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerMagnus(abil)
	if (abil instanceof Shockwave)
		data.shockwave = new AIOShockwave(abil)
	if (abil instanceof Skewer)
		data.skewer = new AIOSkewer(abil, menu)
	if (abil instanceof ReversePolarity) {
		data.polarity = new AIOReversePolarity(abil, menu)
		data.polarity?.AddSkewer(data.skewer!)
	}
}

export const SkewerMode = (unit: UnitX) => {
	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	if (data.blink?.Ability.CanBeCasted() !== true || data.skewer?.Ability.CanBeCasted() !== true)
		return false

	const target = TargetManager.Target!
	const castPosition = target.PredictedPosition(data.skewer.Ability.CastPoint)
	const blinkPosition = unit.Position.Extend2D(castPosition, unit.Distance(castPosition) + 100)
	const distance = unit.Distance(blinkPosition)

	if (data.blink.Ability.CastRange < distance)
		return false

	const ally = ArrayExtensions.orderBy(TargetManager.AllyHeroes.filter(x => !x.Equals(unit)
		&& AllySkewerSelector.IsEnabled(x.Name) && x.Distance(target) < (data.skewer?.Ability.CastRange ?? 0) + 600,
	), x => x.Distance(target))[0]

	if (ally === undefined)
		return false

	OrbWalker.Sleeper.Sleep(data.skewer.Ability.CastPoint + 0.3, unit.Handle)
	unit.UseAbility(data.blink.Ability, { intent: blinkPosition })
	unit.UseAbility(data.skewer.Ability, { intent: ally.Position })
	HERO_DATA.ComboSleeper.Sleep(0.3, unit.Handle)
	return true
}

export const MagnusMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.polarity))
		return true

	if (!helper.CanBeCasted(data.polarity, false, false, false) && !helper.CanBeCasted(data.skewer))
		if (helper.UseAbility(data.shiva))
			return true

	if (helper.UseDoubleBlinkCombo(data.force, data.blink))
		return true

	if (helper.UseAbilityIfCondition(data.blink, data.polarity)) {
		data.polarity?.ForceUseAbility()
		return true
	}

	if (helper.UseAbility(data.force, 500, 100))
		return true

	if (helper.UseAbility(data.skewer))
		return true

	if (helper.CanBeCasted(data.skewer, false, false)) {
		const canCastBlink = helper.CanBeCasted(data.blink, false, false)
		const canCastForce = helper.CanBeCasted(data.force, false, false)
		if (data.skewer?.UseAbilityIfCondition(data.polarity, canCastBlink, canCastForce))
			return true
	}

	if (helper.UseAbilityIfCondition(data.skewer, data.blink, data.force))
		return true

	if (helper.CanBeCasted(data.skewer, false, false) && helper.CanBeCasted(data.shockwave, false)
		&& !helper.CanBeCasted(data.polarity, false, false)) {
		if (data.skewer?.UseAbilityOnTarget(TargetManager.Target!))
			return true
	}

	if (helper.UseAbilityIfCondition(data.shockwave, data.blink, data.force, data.skewer, data.polarity))
		return true

	if (helper.CanBeCasted(data.refresher) || helper.CanBeCasted(data.refresherShard)) {
		if (helper.CanBeCasted(data.polarity, true, true, true, false) && !data.polarity?.Ability.IsReady) {
			const useRefresher = helper.CanBeCasted(data.refresherShard) ? data.refresherShard : data.refresher
			if (helper.HasMana(data.polarity, useRefresher))
				if (helper.UseAbility(useRefresher))
					return true
		}
	}
	return false
}

export class OrbWalkMagnus extends BaseOrbwalk {
	protected Attack(target: UnitX) {
		if (AllySkewerKey.is_pressed)
			return false
		return super.Attack(target)
	}
}

EventsX.on("AbilityDestroyed", abil => {
	if (abil instanceof RefresherShard)
		data.refresherShard = undefined
})

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
