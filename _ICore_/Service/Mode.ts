import { UnitX } from "immortal-core/Imports"
import { Menu } from "wrapper/Imports"
import { BaseMenu, ModeCombo } from "../../Menu/Base"
import { HERO_DATA } from "../data"
import { FailSafeManager } from "../Manager/FailSafe"
import { OrbWallkerManager } from "../Manager/Orbwalk"

export function ComboMode(
	unit: UnitX,
	call: CallableFunction,
	mode: ModeCombo,
	menuBase: BaseMenu,
	key: Menu.KeyBind,
) {
	if (!key.is_pressed)
		return

	if (menuBase.SetFailsafe.value)
		FailSafeManager.Update(unit)

	if (call(unit, mode))
		return

	OrbWallkerManager.Update(unit.Handle)
}

/** TODO add check press hero/unit combo/harass */
export const OnReleasePressedMode = (menu: BaseMenu, oldState: number) => {
	menu.ComboKey.OnPressed(() => {
		if (HERO_DATA.ModeAttack !== 0)
			HERO_DATA.ModeAttack = 0
	})
	menu.HarassKey.OnPressed(() => {
		if (HERO_DATA.ModeAttack !== 0)
			HERO_DATA.ModeAttack = 0
	})
	menu.ComboKey.OnRelease(() => {
		FailSafeManager.Dispose()
		if (HERO_DATA.ModeAttack !== oldState)
			HERO_DATA.ModeAttack = oldState
	})
	menu.HarassKey.OnRelease(() => {
		FailSafeManager.Dispose()
		if (HERO_DATA.ModeAttack !== oldState)
			HERO_DATA.ModeAttack = oldState
	})
}
