import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { SkywrathMage } from "./menu";
import { SAbilitiesCreated, SkywrathMode } from "./Skywrath";

HeroService.RegisterHeroModule(SkywrathMage.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(SkywrathMage, oldState)
TargetManager.DestroyParticleDrawTarget(SkywrathMage)

function onTick(unit: UnitX) {
	if (!SkywrathMage.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = SkywrathMage.TargetMenuTree
	TargetManager.TargetLocked = SkywrathMage.ComboKey.is_pressed || SkywrathMage.HarassKey.is_pressed

	ComboMode(
		unit,
		SkywrathMode,
		SkywrathMage.ComboMode,
		SkywrathMage,
		SkywrathMage.ComboKey,
	)

	ComboMode(
		unit,
		SkywrathMode,
		SkywrathMage.HarrasMode,
		SkywrathMage,
		SkywrathMage.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!SkywrathMage.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		SkywrathMage.TargetMenuTree.DrawTargetParticleColor,
		SkywrathMage.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(SkywrathMage.ComboMode, owner)
	SwitchPanel.Draw(SkywrathMage.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!SkywrathMage.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (SkywrathMage.ComboKey.is_pressed || SkywrathMage.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onMouseDown(key: VMouseKeys) {
	if (!SkywrathMage.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(SkywrathMage.ComboMode)
		SwitchPanel.MouseLeftDown(SkywrathMage.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && ((SkywrathMage.ComboKey.assigned_key as VMouseKeys) === key
		|| (SkywrathMage.HarassKey.assigned_key as VMouseKeys) === key))
	return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!SkywrathMage.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((SkywrathMage.ComboKey.assigned_key as VKeys) === key || (SkywrathMage.HarassKey.assigned_key as VKeys) === key)
}

function onCreated(owner: UnitX) {
	if (SkywrathMage.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, SkywrathMage.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, SkywrathMage))
}

function onDestoyed(owner: UnitX) {
	if (SkywrathMage.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== SkywrathMage.npc_name)
		return
	SAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (SkywrathMage.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
