import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { AAbilitiesCreated, AxeMode } from "./Axe";
import { AxeMenu } from "./menu";

HeroService.RegisterHeroModule(AxeMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(AxeMenu, oldState)
TargetManager.DestroyParticleDrawTarget(AxeMenu)

function onTick(unit: UnitX) {
	if (!AxeMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = AxeMenu.TargetMenuTree
	TargetManager.TargetLocked = AxeMenu.ComboKey.is_pressed || AxeMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		AxeMode,
		AxeMenu.ComboMode,
		AxeMenu,
		AxeMenu.ComboKey,
	)

	ComboMode(
		unit,
		AxeMode,
		AxeMenu.HarrasMode,
		AxeMenu,
		AxeMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!AxeMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(owner,
		AxeMenu.TargetMenuTree.DrawTargetParticleColor,
		AxeMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(AxeMenu.ComboMode, owner)
	SwitchPanel.Draw(AxeMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!AxeMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true
	if (TargetManager.HasValidTarget && (AxeMenu.ComboKey.is_pressed || AxeMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!AxeMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((AxeMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!AxeMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(AxeMenu.ComboMode)
		SwitchPanel.MouseLeftDown(AxeMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (AxeMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (AxeMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, AxeMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, AxeMenu))
}

function onDestoyed(owner: UnitX) {
	if (AxeMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== AxeMenu.npc_name)
		return
	AAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (AxeMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
