import { ActiveAbility, Chakram, OrbWalker } from "immortal-core/Imports";
import { Vector3 } from "wrapper/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class TimberChakram extends AIONuke {

	public readonly СastPosition = new Vector3()

	private readonly chakram: Chakram

	constructor(ability: ActiveAbility) {
		super(ability)
		this.chakram = ability as Chakram
	}

	public get ReturnChakram() {
		return this.chakram.ReturnChakram
	}

	public IsDamaging() {

		if (this.Ability.ActionSleeper.Sleeping)
			return false

		if (TargetManager.Target!.Distance(this.СastPosition) > this.Ability.Radius)
			return false

		return true
	}

	public Return() {
		if (this.Owner.UseAbility(this.ReturnChakram)) {
			this.ReturnChakram.ActionSleeper.Sleep(0.35 + this.Ability.InputLag)
			return true
		}
		return true
	}

	public ShouldReturnChakram(damagingChakrams: number) {

		if (this.Ability.ActionSleeper.Sleeping || !this.chakram.ReturnChakram.CanBeCasted())
			return false

		const target = TargetManager.Target!

		if (this.IsDamaging()) {

			const damage = this.Ability.GetDamage(target)
			if (target.Health < (damage / 2) * damagingChakrams)
				return true

			if (target.GetAngle(this.СastPosition) < 0.75)
				return false

			if (target.IsMoving && target.Distance(this.СastPosition) > this.Ability.Radius - 50)
				return true

			return false

		} else {
			if (target.GetAngle(this.СastPosition) > 0.5)
				return true
		}

		return false
	}

	public UseAbility(aoe: boolean) {

		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target, TargetManager.EnemyHeroes)

		if (target.GetAngle(this.Owner.Position) > 2) {
			input.Delay += 0.5
		}

		const output = this.Ability.GetPredictionOutput(input)
		this.СastPosition.CopyFrom(output.CastPosition)

		if (!this.Owner.UseAbility(this.Ability, { intent: this.СastPosition }))
			return false

		const delay = this.Ability.GetCastDelay(this.СastPosition)
		HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
		this.Ability.ActionSleeper.Sleep(this.Ability.GetHitTime(this.СastPosition))
		OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
		return true
	}
}
