import { EarthSpiritStone, HeroX, HitChanceX, OrbWalker, PRectangle, UnitsX } from "immortal-core/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";

export class SpiritGeomagneticGrip extends AIONuke {

	public CanHit() {
		return !this.Owner.HasBuffByName("modifier_earth_spirit_rolling_boulder_caster")
	}

	public ForceUseAbility() {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target)
		let output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false

		// TODO Check UnitsX.All
		for (const unit of UnitsX.All.filter(x => (!(x instanceof HeroX) || x.IsIllusion)
			&& !x.IsInvulnerable
			&& x.Distance(this.Owner) < this.Ability.CastRange
			&& x.IsAlive)
		) {
			input.Caster = unit
			output = this.Ability.GetPredictionOutput(input)
			const rec = new PRectangle(this.Owner.Position, unit.Position, this.Ability.Radius)
			if (!rec.IsInside(output.TargetPosition))
				continue
			if (!this.Owner.UseAbility(this.Ability, { intent: unit }))
				continue
			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}
		return false
	}

	public UseAbility(aoe: boolean) {
		const target = TargetManager.Target!
		const input = this.Ability.GetPredictionInput(target)
		let output = this.Ability.GetPredictionOutput(input)
		if (output.HitChance < HitChanceX.Low)
			return false

		for (const stone of UnitsX.All.filter(x => x instanceof EarthSpiritStone && x.Distance(this.Owner) < this.Ability.CastRange && x.IsAlive)) {
			input.Caster = stone
			output = this.Ability.GetPredictionOutput(input)
			const rec = new PRectangle(this.Owner.Position, stone.Position, this.Ability.Radius)

			if (!rec.IsInside(output.TargetPosition))
				continue

			if (!this.Owner.UseAbility(this.Ability, { intent: stone.Position }))
				continue

			const delay = this.Ability.GetCastDelay(TargetManager.Target!)
			HERO_DATA.ComboSleeper.Sleep(delay, this.Owner.Handle)
			this.Ability.ActionSleeper.Sleep(delay + 0.5)
			OrbWalker.Sleeper.Sleep(delay, this.Owner.Handle)
			return true
		}

		return false
	}
}
