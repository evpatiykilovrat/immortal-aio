import { AbilityX, BlinkDagger, Bloodthorn, Dagon, Decrepify, EventsX, Gleipnir, LifeDrain, NetherBlast, NetherWard, Nullifier, OrchidMalevolence, RodOfAtos, ScytheOfVyse, UnitX, VeilOfDiscord } from "immortal-core/Imports"
import { AbilityHelper, AIOAoe, AIOBlink, AIOBloodthorn, AIODebuff, AIODisable, AIONuke, AIONullifier, AIOTargetable, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index"
import { ModeCombo } from "../../Menu/Base"

const data = {
	drain: undefined as Nullable<AIOTargetable>,
	ward: undefined as Nullable<AIOAoe>,
	decrepify: undefined as Nullable<AIODebuff>,
	blast: undefined as Nullable<AIONuke>,
	blink: undefined as Nullable<AIOBlink>,
	dagon: undefined as Nullable<AIONuke>,
	hex: undefined as Nullable<AIODisable>,
	orchid: undefined as Nullable<AIODisable>,
	veil: undefined as Nullable<AIODebuff>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	nullifier: undefined as Nullable<AIONullifier>,
	atos: undefined as Nullable<AIODisable>,
}

export const PAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof LifeDrain)
		data.drain = new AIOTargetable(abil)
	if (abil instanceof Decrepify)
		data.decrepify = new AIODebuff(abil)
	if (abil instanceof NetherWard)
		data.ward = new AIOAoe(abil)
	if (abil instanceof NetherBlast)
		data.blast = new AIONuke(abil)
	if (abil instanceof BlinkDagger)
		data.blink = new AIOBlink(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof VeilOfDiscord)
		data.veil = new AIODebuff(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
}

export const PugnaMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.blink, 700, 350))
		return true

	if (helper.UseAbility(data.veil))
		return true

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.nullifier))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.UseAbility(data.decrepify)) {
		HERO_DATA.ComboSleeper.ExtendSleep(0.15, unit.Handle)
		return true
	}

	if (helper.UseAbility(data.dagon))
		return true

	if (helper.UseAbility(data.blast))
		return true

	if (helper.UseAbility(data.ward))
		return true

	if (helper.UseAbility(data.drain)) {
		HERO_DATA.ComboSleeper.ExtendSleep(0.1, unit.Handle)
		return true
	}

	return false
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
