import { GameX } from "immortal-core/Imports"
import { dotaunitorder_t, EventsSDK, Unit } from "wrapper/Imports"
import { HERO_DATA } from "../data"

EventsSDK.on("PrepareUnitOrders", order => {
	if (!GameX.IsInGame || !(order.Issuers[0] instanceof Unit)
		|| order.Issuers[0].IsEnemy() || order.OrderType === dotaunitorder_t.DOTA_UNIT_ORDER_TRAIN_ABILITY)
		return true
	const hero_module = HERO_DATA.HeroModules.get(order.Issuers[0].Name)
	if (hero_module === undefined)
		return true
	return hero_module.onOrders(order)
})
