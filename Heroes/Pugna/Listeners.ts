import { AbilityX, EventsX, UnitX } from "immortal-core/Imports";
import { dotaunitorder_t, ExecuteOrder, Input, VKeys, VMouseKeys } from "wrapper/Imports";
import { BaseOrbwalk, ComboMode, HeroService, HERO_DATA, OnReleasePressedMode, OrbWallker, ShieldBreaker, ShieldBreakerMap, SwitchPanel, TargetManager } from "_ICore_/Index";
import { PugnaMenu } from "./menu";
import { PAbilitiesCreated, PugnaMode } from "./Pugna";

HeroService.RegisterHeroModule(PugnaMenu.npc_name, {
	onTick,
	onDraw,
	onOrders,
	onKeyDown,
	onMouseDown,
	onCreated,
	onDestoyed,
	onAbilityCreated,
})

const oldState = HERO_DATA.ModeAttack
OnReleasePressedMode(PugnaMenu, oldState)
TargetManager.DestroyParticleDrawTarget(PugnaMenu)

function onTick(unit: UnitX) {
	if (!PugnaMenu.BaseState.value)
		return

	if (!unit.Owner.IsIllusion)
		TargetManager.Owner = unit

	TargetManager.Update()
	TargetManager.MenuSettings = PugnaMenu.TargetMenuTree
	TargetManager.TargetLocked = PugnaMenu.ComboKey.is_pressed || PugnaMenu.HarassKey.is_pressed

	ComboMode(
		unit,
		PugnaMode,
		PugnaMenu.ComboMode,
		PugnaMenu,
		PugnaMenu.ComboKey,
	)

	ComboMode(
		unit,
		PugnaMode,
		PugnaMenu.HarrasMode,
		PugnaMenu,
		PugnaMenu.HarassKey,
	)
}

function onDraw(owner: UnitX) {
	if (!PugnaMenu.BaseState.value || owner.BaseOwner.IsIllusion)
		return
	TargetManager.Draw(
		owner,
		PugnaMenu.TargetMenuTree.DrawTargetParticleColor,
		PugnaMenu.TargetMenuTree.DrawTargetParticleColor2,
	)
	SwitchPanel.Draw(PugnaMenu.ComboMode, owner)
	SwitchPanel.Draw(PugnaMenu.HarrasMode, owner)
}

function onOrders(order: ExecuteOrder) {
	if (!PugnaMenu.BaseState.value || order.Issuers[0].IsEnemy())
		return true

	if (TargetManager.HasValidTarget && (PugnaMenu.ComboKey.is_pressed || PugnaMenu.HarassKey.is_pressed)
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_POSITION
		&& order.OrderType !== dotaunitorder_t.DOTA_UNIT_ORDER_CAST_NO_TARGET
	) return false

	return true
}

function onKeyDown(key: VKeys) {
	if (!PugnaMenu.BaseState.value || Input.IsKeyDown(VKeys.CONTROL))
		return true
	return !((PugnaMenu.ComboKey.assigned_key as VKeys) === key)
}

function onMouseDown(key: VMouseKeys) {
	if (!PugnaMenu.BaseState.value)
		return true
	if (key === VMouseKeys.MK_LBUTTON) {
		SwitchPanel.MouseLeftDown(PugnaMenu.ComboMode)
		SwitchPanel.MouseLeftDown(PugnaMenu.HarrasMode)
		return true
	}
	if (!Input.IsKeyDown(VKeys.CONTROL) && (PugnaMenu.ComboKey.assigned_key as VMouseKeys) === key)
		return false

	return true
}

function onCreated(owner: UnitX) {
	if (PugnaMenu.npc_name !== owner.Name)
		return
	if (!ShieldBreakerMap.has(owner))
		ShieldBreakerMap.set(owner, new ShieldBreaker(owner, PugnaMenu.Shields))
	if (!OrbWallker.has(owner.Handle))
		OrbWallker.set(owner.Handle, new BaseOrbwalk(owner, PugnaMenu))
}

function onDestoyed(owner: UnitX) {
	if (PugnaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
	ShieldBreakerMap.delete(owner)
}

function onAbilityCreated(owner: UnitX, abil: AbilityX) {
	if (owner.Name !== PugnaMenu.npc_name)
		return
	PAbilitiesCreated(abil)
}

EventsX.on("removeControllable", owner => {
	if (PugnaMenu.npc_name !== owner.Name)
		return
	OrbWallker.delete(owner.Handle)
})
